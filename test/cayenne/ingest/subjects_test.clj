(ns cayenne.ingest.subjects-test
  (:require [cayenne.api-fixture :refer [api-get setup-api]]
            [cayenne.tasks.subject :as subject]
            [cayenne.tasks.journal :as journal]
            [clojure.java.io :refer [resource reader]]
            [clojure.test :refer [deftest testing is]]
            [user :refer [flush-elastic index-work-files]]))

(deftest ^:integration ingesting-subjects
  (testing "subjects are indexed in ES"

    (setup-api)
    (is (nil? (subject/es-subject-name "1202")))
    (is (nil? (subject/es-subject-name "3098")))

    (subject/es-index-subjects
      [{:code 1202 :name "Applied Psychoceramics"}
       {:code 3098 :name "Gallifreyan Philosophy"}])
    (flush-elastic)

    (is (= "Applied Psychoceramics" (subject/es-subject-name "1202")))
    (is (= "Gallifreyan Philosophy" (subject/es-subject-name "3098")))))

(deftest ^:integration updating-journal-subjects
  (testing "journal subjects are updated in ES"
    
    (setup-api)
    (doseq [issns [["2141-6494"] ["0044-586X"] ["2107-7207"] ["0044-586X" "2107-7207"]]]
      (is (empty? (subject/es-journal-subjects issns))))
    
    (with-open [rdr (-> "ingest/journals/journals.csv" resource reader)]
      (journal/index-journals rdr))
    (flush-elastic)

    (subject/es-update-journal-subjects
      "157865"
      [{:ASJC "1202" :name "Applied Psychoceramics"}
       {:ASJC "3098" :name "Gallifreyan Philosophy"}
       {:ASJC "1270" :name "Theoretical Theory"}])
    (subject/es-update-journal-subjects 
      "99950"
      [{:ASJC "3098" :name "Gallifreyan Philosophy"}])
    (flush-elastic)

    (is (= #{"Applied Psychoceramics" "Gallifreyan Philosophy" "Theoretical Theory"}
           (set (subject/es-journal-subjects ["2141-6494"]))))
    (doseq [issns [["0044-586X"] ["2107-7207"] ["0044-586X" "2107-7207"]]]
      (is (= ["Gallifreyan Philosophy"] (subject/es-journal-subjects issns)))))

  (testing "journal subjects are kept after journal update"
    
    (setup-api)

    (with-open [rdr (-> "ingest/journals/journals.csv" resource reader)]
      (journal/index-journals rdr))
    (flush-elastic)
    
    (subject/es-update-journal-subjects
      "157865"
      [{:ASJC "1202" :name "Applied Psychoceramics"}
       {:ASJC "3098" :name "Gallifreyan Philosophy"}
       {:ASJC "1270" :name "Theoretical Theory"}])
    (subject/es-update-journal-subjects 
      "99950"
      [{:ASJC "3098" :name "Gallifreyan Philosophy"}])
    (flush-elastic)
    
    (with-open [rdr (-> "ingest/journals/journals.csv" resource reader)]
      (journal/index-journals rdr))
    (flush-elastic)
    
    (is (= #{"Applied Psychoceramics" "Gallifreyan Philosophy" "Theoretical Theory"}
           (set (subject/es-journal-subjects ["2141-6494"]))))
    (doseq [issns [["0044-586X"] ["2107-7207"] ["0044-586X" "2107-7207"]]]
      (is (= ["Gallifreyan Philosophy"] (subject/es-journal-subjects issns))))))

(deftest ^:integration updating-work-subjects
  (testing "work subjects are updated in ES"
    
    (setup-api)
    
    (with-open [rdr (-> "ingest/journals/journals.csv" resource reader)]
      (journal/index-journals rdr))
    (flush-elastic)
    
    (subject/es-update-journal-subjects
      "99950"
      [{:ASJC "1202" :name "Applied Psychoceramics"}
       {:ASJC "3098" :name "Gallifreyan Philosophy"}
       {:ASJC "1270" :name "Theoretical Theory"}])
    (flush-elastic)
    
    (index-work-files
      ["dev-resources/ingest/works/data/crossref-unixsd-example.body"
       "dev-resources/ingest/works/data/crossref-unixsd-379a5ac7-5b52-443d-9655-0fe3871d0baa.body"])
    (flush-elastic)
    
    (is (nil? (-> "/v1/works/10.1155/2016/6402942" api-get :subject)))
    (is (= #{"Applied Psychoceramics" "Gallifreyan Philosophy" "Theoretical Theory"}
           (-> "/v1/works/10.1051/acarologia/20132085" api-get :subject set)))))

