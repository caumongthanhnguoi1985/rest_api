(ns cayenne.api-fixture
  (:require [cayenne.api.route :as route]
            [cayenne.conf :as conf]
            [cayenne.tasks.coverage :refer [check-journals check-members]]
            [clj-http.client :as http]
            [clj-time.core :as clj-time]
            [clojure.data.json :as json]
            [clojure.java.io :refer [resource]]
            [ring.mock.request :as mock]
            [taoensso.timbre :as timbre]
            [clojure.core.async :refer [chan put! <!!]]
            [user]))

(defonce api-root "http://localhost:3000")

; Narnia – where it's always winter but never Christmas.
(def frozen-time (clj-time/date-time 2018 12 1))


;;; These functions are more self-contained versions of those found in the user namespace.
;;; We can use them to build more precise test fixtures, leading to higher specificity, more maintainable, faster tests.


(defn setup-api []
  (conf/set-param! [:dir :data] (.getPath (resource "feeds")))
  (user/start))

(defn setup-test-works []
  (clj-time/do-at
    frozen-time
    (user/index-work-files-dir (.getPath (resource "feeds/corpus"))))
  (user/flush-elastic))

(defn setup-test-journals []
  (user/index-journal-list (.getPath (resource "titles.csv")))
  (user/flush-elastic))

(defn setup-journals-coverage []
  (clj-time/do-at
    frozen-time
    (check-journals))
  (user/flush-elastic))

(defn setup-test-members []
  (user/index-member-list
    (.getPath (resource "get-member-list.edn"))
    (.getPath (resource "get-prefix-info.edn")))
  (user/flush-elastic))

(defn setup-members-coverage []
  (clj-time/do-at
    frozen-time
    (check-members))
  (user/flush-elastic))

(defn setup-test-funders []
  (user/index-funder-list (.getPath (resource "registry.rdf")))
  (user/flush-elastic))

(defn setup-test-subjects []
  (user/index-subjects "categories.edn" "journal-categories.edn")
  (user/flush-elastic))


(defn api-fixture [& setup-functions]
  (fn [f]
    (doseq [sf setup-functions]
      (sf))
    (f)))

;;;

(defn clean-api-response
  "Remove fields which make simple comparisons non-deterministic."
  [message {:keys [sorter] :or {sorter :DOI}}]
  (cond-> message
    (:last-status-check-time message) (dissoc :last-status-check-time)
    (:indexed message) (dissoc :indexed)
    (:items message) (-> (update :items (partial map #(dissoc % :indexed :last-status-check-time)))
                         (update :items (partial sort-by sorter)))
    (:descendants message) (update :descendants sort)))

(defn api-get-network
  "Make an API request via the HTTP stack."
  [route & options]
  (with-redefs [timbre/str-println (constantly nil)]
    (-> (http/get (str api-root route) {:as :json})
        :body
        :message
        (clean-api-response options))))

(defn- api-request
  "Make an API request via directly via the Ring routes."
  ([route type]
   (api-request route type {}))
  ([route type request]
   (with-redefs [timbre/str-println (constantly nil)]
     (let [api-handler (route/create-handler)
           request (merge (mock/request type route) request)
           ch (chan)
           respond (partial put! ch)]
       (api-handler request respond (constantly nil))
       (<!! ch)))))

(defn api-get
  "Make an API request via directly via the Ring routes."
  ([route]
   (api-get route {}))
  ([route request]
   (-> (api-request route :get request)
       :body
       (json/read-str :key-fn keyword)
       :message)))

(defn api-get-status
  "Make an API request via directly via the Ring routes."
  ([route]
   (api-get-status route {}))
  ([route request]
   (-> (api-request route :get request)
       :status)))

(defn api-get-raw
  "Make an API request via directly via the Ring routes."
  ([route]
   (api-get-raw route {}))
  ([route request]
   (-> (api-request route :get request)
       :body)))

(defn api-post
  "Make an API request via directly via the Ring routes."
  [route request]
  (api-request route :post request))

(defn api-get-cleaned
  [route & options]
  (-> route api-get (clean-api-response options)))

(defn no-scores
  "Update an API result, removing all scores."
  [m]
  (cond-> m
      (:score m) (dissoc :score)
      (:items m) (-> (update :items (partial map #(dissoc % :score))))))
