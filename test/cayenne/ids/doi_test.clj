(ns cayenne.ids.doi-test
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.ids.doi :refer [to-long-doi-uri]]))

(deftest ^:unit to-long-doi-uri-test
  (testing "to-long-doi-uri returns nil for nil or malformed DOI"
    (is (nil? (to-long-doi-uri nil)))
    (is (nil? (to-long-doi-uri "nil"))))

  (testing "to-long-doi-uri transforms to uri"
    (let [uri "http://dx.doi.org/10.1007/s11302-016-9551-2"]
      (is (= uri (to-long-doi-uri "https://dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "http://dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "https://doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "http://doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-long-doi-uri "10.1007/s11302-016-9551-2")))))
  
  (testing "to-long-doi-uri does not remove characters"
    (let [uri "http://dx.doi.org/10.2741/ortéga"]
      (is (= uri (to-long-doi-uri "https://dx.doi.org/10.2741/Ortéga")))
      (is (= uri (to-long-doi-uri "10.2741/Ortéga"))))))
