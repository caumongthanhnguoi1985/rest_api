(ns cayenne.licenses.licenses-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration list-licenses
  
  (testing "number of licenses matches declared total"
    (doseq [query ["/v1/licenses?" "/v1/licenses?query=memory&"]]
      (let [total (-> query (str "rows=0") api-get :total-results)]
        (is (= total (->> total (+ 10) (str query "rows=") api-get :items count))))))

  (testing "expected number of licenses is retrieved"
    (doseq [query ["/v1/licenses?" "/v1/licenses?query=memory&"]]
      (is (= 4 (-> query (str "rows=4") api-get :items count))))))

(deftest ^:integration offset-licenses

  (testing "default offset is 0"
    (is (= (api-get "/v1/licenses?rows=11")
           (api-get "/v1/licenses?rows=11&offset=0"))))

  (testing "empty set is returned for offset outside of existing license range"
    (let [total (-> "/v1/licenses?rows=0" api-get :total-results)]
      (is (->> total (+ 10) (str "/v1/licenses?offset=") api-get :items empty?))))
  
  (testing "offset retrieves consecutive ranges of licenses"
    (doseq [query ["/v1/licenses?" "/v1/licenses?query=the&"]]
      (let [license-range (fn [offset rows]
                            (-> (str query "rows=" rows "&offset=" offset) api-get :items))
            licenses-all (license-range 2 10)
            licenses-1 (license-range 2 7)
            licenses-2 (license-range 9 3)]
        (is (= licenses-all (concat licenses-1 licenses-2)))))))

(deftest ^:integration cursor-licenses
  
  (testing "cursor retrieves the full license set"
    (doseq [query ["/v1/licenses?" "/v1/licenses?query=the&"]]
      (let [total (-> query (str "rows=0") api-get :total-results)
            licenses-all (-> (str query "rows=" total) api-get :items set)
            iterate-licenses (fn il [cursor]
                               (let [response (api-get (str query "rows=5&cursor=" cursor))
                                     items (:items response)]
                                 (if (empty? items)
                                   items
                                   (lazy-cat items (il (:next-cursor response))))))
            licenses-cursor (->> "*" iterate-licenses (take total) set)]
        (is (= licenses-all licenses-cursor))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-works))
