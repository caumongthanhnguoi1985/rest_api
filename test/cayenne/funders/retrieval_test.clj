(ns cayenne.funders.retrieval-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status setup-api]]
            [clojure.test :refer [deftest testing is]]
            [clojure.java.io :refer [resource]]
            [user :refer [index-funder-list flush-elastic]]))

(defn index-test-funders [fname]
  (setup-api)
  (->> fname
       (str "funder-cases/")
       resource
       .getPath
       index-funder-list)
  (flush-elastic))

(deftest ^:integration retrieving-funders
  (testing "funder can be retrieved by an id in a variety of formats"
    (index-test-funders "funder.rdf")
    (let [fid "100010662"]
      (doseq [prefix [""
                      "10.13039/"
                      "doi.org/10.13039/"
                      "dx.doi.org/10.13039/"
                      "http://doi.org/10.13039/"
                      "http://dx.doi.org/10.13039/"
                      "https://doi.org/10.13039/"
                      "https://dx.doi.org/10.13039/"]]
        (is (= fid (->> fid (str prefix) (str "/v1/funders/") api-get :id))))))

  (testing "retrieving a non-existent funder returns 404"
    (setup-api)
    (is (= 404 (api-get-status "/v1/funders/nonexistent")))
    (is (= 404 (api-get-status "/v1/funders/nonexistent/works")))
    (is (= 404 (api-get-status "/v1/funders/10.13039/nonexistent")))
    (is (= 404 (api-get-status "/v1/funders/10.13039/nonexistent/works")))))

(deftest ^:integration ingesting-retrieving-funders

  (testing "more field is never present in hierarchy-names"
    (index-test-funders "hierarchy-names-more.rdf")
    (let [hierarchy-names (:hierarchy-names (api-get "/v1/funders/100014591"))]
      (is (not (contains? hierarchy-names :more)))))

  (testing "correct hierarchy is retrieved"
    (index-test-funders "hierarchy-more.rdf")
    (is (= {:100000001 {:100005447 {:more true} :100014591 {}}}
           (:hierarchy (api-get "/v1/funders/100000001"))))
    (is (= {:100000001 {:100005447 {:100005450 {}} :100014591 {}}}
           (:hierarchy (api-get "/v1/funders/100005447"))))
    (is (= {:100000001 {:100014591 {} :100005447 {:more true}}}
           (:hierarchy (api-get "/v1/funders/100014591"))))
    (is (= {:100000001 {:100005447 {:100005450 {}} :100014591 {}}}
           (:hierarchy (api-get "/v1/funders/100005450")))))

  (testing "relevant descendants are only added for the first parent of a funder"
    (index-test-funders "descendants.rdf")
    (is (= ["100010663"] (:descendants (api-get "/v1/funders/100010662"))))
    (is (= [] (:descendants (api-get "/v1/funders/501100000781")))))

  (testing "funder with an empty label is indexed and retrieved"
    (index-test-funders "empty-label.rdf")
    (is (= "501100009109" (:id (api-get "/v1/funders/501100009109"))))))

