(ns cayenne.funders.funders-test
  (:require [cayenne.api-fixture :refer [api-get api-fixture setup-api setup-test-funders setup-test-works]]
            [clojure.set :refer [subset?]]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [clojure.string :as str]
            [user]))

(deftest ^:integration listing-funders
  (testing "funders endpoint returns results"
    (is (seq (-> "/v1/funders" api-get :items)))))

(deftest ^:integration querying-funders
  (testing "funders endpoint returns result for query"
    (doseq [query ["foundation"
                   "science+foundation"
                   "major+baseball"
                   "baseball+major"
                   "majo+base"
                   "baseb+maj"
                   "american+cancer+society+acs"
                   "Universit%C3%A0%20degli+Studi+G.+d%27Annunzio+Chieti-Pescara"
                   "Universit%C3%A0%20degli%20Studi%20di%20Milano-Bicocca"]]
      (let [funders (->> query (str "/v1/funders?query=") api-get :items)]
        (is (seq funders) (str "failed for query " query))))))

(deftest ^:integration offset-funders
  (testing "offset retrieves consecutive ranges of funders"
    (doseq [[start range-1 range-2] [[0 27 48] [11 45 29] [3 59 0] [49 0 49] [21 0 0]]]
      (let [get-item-range (fn [start size] (-> (str "/v1/funders?rows=" size "&offset=" start) api-get :items))
            items-all (get-item-range start (+ range-1 range-2))
            items-1 (get-item-range start range-1)
            items-2 (get-item-range (+ start range-1) range-2)]
        (is (= (count items-all) (count (concat items-1 items-2))))
        (is (= items-all (concat items-1 items-2)))))))

(deftest ^:integration cursor-funders
  (testing "iteration with cursors retrieves the entire dataset"
    (let [iterate-funders (fn it-f [cursor]
                            (let [funders-page (->> cursor (str "/v1/funders?rows=500&cursor=") api-get)
                                  ids (map :id (:items funders-page))]
                              (if (empty? ids)
                                []
                                (lazy-cat ids (it-f (:next-cursor funders-page))))))
          cursor-funders (iterate-funders "*")
          total (-> "/v1/funders" api-get :total-results)]
      (is (= total (count cursor-funders)))
      (is (= total (count (set cursor-funders)))))))

(deftest ^:integration filtering-funders
  (testing "location filter correctly filters funders"
    (let [locations (->> "/v1/funders?rows=500&filter=location:United+States"
                         api-get
                         :items
                         (map :location)
                         set)]
      (is (= #{"United States"} locations))))

  (testing "doi filter on a specified funder's works query correctly limits result to doi in question"
    (doseq [[filter-doi exp-result-num] [["doi:10.1073/pnas.1609811114" 1]
                                         ["doi:10.1073/pnas.1609811114,doi:10.1016/j.psyneuen.2016.11.018" 2]]]
      (let [funder "100000001"
            queried-dois (set (str/split (str/replace filter-doi #"doi:" "") #","))
            response (api-get (str "/v1/funders/" funder "/works?filter=" filter-doi "&select=DOI"))
            total-results (:total-results response)
            rsp-doi (set (map :DOI (:items response)))]
        (is (= total-results exp-result-num))
        (is (= rsp-doi queried-dois))))))

(deftest ^:integration filter-and-query
  (testing "queries and filters are AND-ed"
    (let [id-set (fn [query] (->> query (str "/v1/funders?rows=500&") api-get :items (map :id) set))]
      (doseq [[wide-q narrow-q] [["query=medical" "query=medical&filter=location:Spain"]
                                 ["filter=location:Spain" "query=medical&filter=location:Spain"]]]
        (is (subset? (id-set narrow-q) (id-set wide-q)))))))

(deftest ^:intgration filtering-works
  (testing "works are returned through /funders/<id>/works route"
    (let [route-total (-> "/v1/funders/100000002/works" api-get :total-results)
          route-total-0-rows (-> "/v1/funders/100000002/works?rows=0" api-get :total-results)]
      (is (> route-total 0))
      (is (= route-total route-total-0-rows)))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-funders setup-test-works))
