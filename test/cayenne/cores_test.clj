(ns cayenne.cores-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration listing-cores
  (testing "cores endpoint returns cores"
    (let [cores (-> "/v1/cores" api-get :items)]
      (is (seq cores)))))

(deftest ^:integration retrieving-cores
  (testing "core is retrieved"
    (is (api-get "/v1/cores/default"))))

(use-fixtures
  :once
  (api-fixture setup-api))
