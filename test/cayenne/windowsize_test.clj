(ns cayenne.windowsize-test
  (:require [cayenne.elastic.mappings :refer [index-settings]]
            [cayenne.api-fixture :refer [api-get api-fixture setup-api setup-test-journals setup-test-members
                                         setup-test-funders setup-test-works]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(defn limit [route]
  (->> route
       (get {"journals" "journal" "members" "member" "funders" "funder" "works" "work"})
       index-settings
       :max_result_window))

(deftest ^:integration offset-windowsize
  (testing "offset upto ES result window limit returns a non-error message"
    (doseq [route ["journals" "members" "funders"]
            rows [20 1000]]
      (let [offset (- (limit route) rows)
            response (api-get (str "/v1/" route "?offset=" offset "&rows=" rows))]
        (is (= (-> response :query :start-index) offset)))))

  (testing "offset over ES result window limit errors out gracefully"
    (doseq [route ["journals" "members" "funders"]]
      (let [offset (- (limit route) 20)
            response (api-get (str "/v1/" route "?offset=" offset "&rows=30"))]
        (is (= [{:type "integer-not-valid"
                 :value offset
                 :message (str "Offset specified as " offset " but for this route and for rows = 30, offset must be a positive integer less than or equal to " (- (limit route) 30) ".")}]
               response)))

      (let [offset (limit route)
            response (api-get (str "/v1/" route "?offset=" offset "&rows=20"))]
        (is (= [{:type "integer-not-valid"
                 :value offset
                 :message (str "Offset specified as " offset " but for this route and for rows = 20, offset must be a positive integer less than or equal to " (- (limit route) 20) ".")}]
               response)))))
  
  (testing "offset for works upto works result window limit returns a non-error message"
    (doseq [rows [20 1000]
            route ["works"
                   "funders/100000002/works"
                   "prefixes/10.1016/works"
                   "members/311/works"
                   "journals/0306-4530/works"
                   "types/journal-article/works"]]
      (let [offset (- (limit "works") rows)
            response (api-get (str "/v1/" route "?offset=" offset "&rows=" rows))]
        (is (= (-> response :query :start-index) offset)))))
  
  (testing "offset for works over works result window limit errors out gracefully"
    (doseq [[offset rows] [[(- (limit "works") 20) 30] [(- (limit "works") 950) 980]]
            route ["works"
                   "funders/100000002/works"
                   "prefixes/10.1016/works"
                   "members/311/works"
                   "journals/0306-4530/works"
                   "types/journal-article/works"]]
      (let [response (api-get (str "/v1/" route "?offset=" offset "&rows=" rows))]
        (is (= 1 (count response)))
        (is (= "integer-not-valid" (-> response first :type)))
        (is (= offset (-> response first :value)))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-funders setup-test-journals setup-test-members setup-test-works))
