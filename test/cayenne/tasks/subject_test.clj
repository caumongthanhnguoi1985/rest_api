(ns cayenne.tasks.subject-test
  (:require [cayenne.tasks.subject :as subject]
            [clojure.test :refer [deftest is testing]])
  (:import [com.monitorjbl.xlsx.impl StreamingRow StreamingCell]
           [java.util TreeMap]))

(defn make-cell [col-index row-index value]
  (proxy [StreamingCell] [col-index row-index false]
    (getStringCellValue [] value)))

(defn make-row [row-index & cells]
  (let [row (StreamingRow. row-index false)
        cell-map (TreeMap.)]
    (doseq [[cell-index cell-value] cells]
      (.put cell-map (int cell-index) (make-cell cell-index row-index cell-value)))
    (.setCellMap row cell-map)
    row))

(deftest ^:unit extracting-cell-value

  (let [row (make-row 21 [0 "2612"] [4 "Numerical Analysis"])]
    
    (testing "nil cell value is returned when row contains no value for a given index"
      (is (nil? (subject/cell-value nil 0)))
      (is (nil? (subject/cell-value (make-row 21) 10)))
      (is (nil? (subject/cell-value row 10))))

    (testing "cell value is extracted"
      (is (= "2612" (subject/cell-value row 0)))
      (is (= "Numerical Analysis" (subject/cell-value row 4))))))

(deftest ^:unit detecting-cell-numbers
  
  (testing "cell numbers are detected"
    (is (= [1 2 4]
           (subject/col-numbers (make-row 21 [0 "2612"] [1 "Print-ISSN"] [2 "E-ISSN"] [3 ""] [4 "Codes (ASJC)"])))))

  (testing "default numbers are returned if cell numbers are not detected"
    (is (= [2 1 3]
           (subject/col-numbers (make-row 21 [0 "Prnt"] [1 "E-ISSN"] [2 ""] [3 "Codes (ASJC)"]))))
    (is (= [1 3 4]
           (subject/col-numbers (make-row 21 [0 "2612"] [1 "Print-ISSN"] [2 "E-SN"] [3 ""] [4 "Codes (ASJC)"]))))
    (is (= [1 2 24]
           (subject/col-numbers (make-row 21 [0 "2612"] [1 "Print-ISSN"] [2 "E-ISSN"] [3 ""] [4 "Codes"]))))
    (is (= [2 3 24]
           (subject/col-numbers (make-row 21))))))

(deftest ^:component reading-subjects
  
  (testing "subject is correctly read from row"
    (is (nil? (subject/->subject nil)))
    (is (nil? (subject/->subject (make-row 23))))
    (is (nil? (subject/->subject (make-row 23 [0 "Comments"]))))
    (is (nil? (subject/->subject (make-row 23 [1 "Mathematics"]))))
    (is (= {:code 2612 :name "Numerical Analysis"}
           (subject/->subject (make-row 23 [0 "2612"] [1 "Numerical Analysis"])))))

  (testing "subjects are correctly read from row sequence"
    (let [rows [(make-row 0 [0 "Comments"])
                (make-row 1 [0 "Comments"])
                (make-row 2 [0 "Code"] [1 "Description"])
                (make-row 3 [1 "Mathematics"])
                (make-row 4 [0 "2612"] [1 "Numerical Analysis"])
                nil
                (make-row 6 [0 ""] [1 "Neuroscience"])
                (make-row 7 [0 "2803"] [1 "Biological Psychiatry"])
                (make-row 8 [0 "Comments"])
                (make-row 9 [0 "2808"] [1 "Neurology"])]]
      (is (= [{:code 2612 :name "Numerical Analysis"}
              {:code 2803 :name "Biological Psychiatry"}
              {:code 2808 :name "Neurology"}]
             (subject/->subjects rows))))))

(deftest ^:component reading-issns
  (testing "ISSNs are correctly read from row"
     (is (= [] (subject/->issns (make-row 6) 2 3)))
     (is (= [] (subject/->issns (make-row 6 [2 ""]) 2 3)))
     (is (= [] (subject/->issns (make-row 6 [2 " "] [3 ""]) 2 3)))
     (is (= [] (subject/->issns (make-row 6 [2 " "] [3 "malformed"]) 2 3)))
     (is (= ["2366-004X"] (subject/->issns (make-row 6 [2 "2366004X"]) 2 3)))
     (is (= ["1876-2867"] (subject/->issns (make-row 6 [3 "18762867"]) 2 3)))
     (is (= ["1744-9480" "1744-9499"]
            (subject/->issns (make-row 6 [2 "17449480"] [3 "17449499"]) 2 3)))
     (is (= ["1744-9499"] (subject/->issns (make-row 6 [2 nil] [3 "17449499"]) 2 3)))
     (is (= ["1744-9499"] (subject/->issns (make-row 6 [2 "nil"] [3 "17449499"]) 2 3)))))

(deftest ^:component reading-journal-subjects
  (testing "journal subjects are correctly read from row"
    (with-redefs [subject/es-subject-name (partial str "name-")]
      (is (= [] (subject/->journal-subjects (make-row 90 [23 ""]) 23)))
      (is (= [{:ASJC 2700 :name "name-2700"}]
             (subject/->journal-subjects (make-row 90 [23 "2700;"]) 23)))
      (is (= [{:ASJC 2802 :name "name-2802"}
              {:ASJC 1311 :name "name-1311"}]
             (subject/->journal-subjects (make-row 90 [23 "2802; 1311;"]) 23)))
      (is (= [{:ASJC 1704 :name "name-1704"}
              {:ASJC 2608 :name "name-2608"}
              {:ASJC 1707 :name "name-1707"}
              {:ASJC 2611 :name "name-2611"}]
             (subject/->journal-subjects (make-row 90 [23 "1704  2608; 1707 2611;"]) 23)))))
  
  (testing "journal subjects with null name are not included in the response"
    (with-redefs [subject/es-subject-name #(when (= "2802" %) "name")]
      (is (= [{:ASJC 2802 :name "name"}]
             (subject/->journal-subjects (make-row 90 [23 "2802; 1311;"]) 23))))))

