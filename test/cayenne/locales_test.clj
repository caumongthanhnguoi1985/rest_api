(ns cayenne.locales-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration listing-locales
  (testing "locales endpoint returns locales"
    (let [locales (-> "/v1/locales" api-get :items)]
      (is (seq locales))
      (is (some #{"en-GB"} locales)))))

(use-fixtures
  :once
  (api-fixture setup-api))
