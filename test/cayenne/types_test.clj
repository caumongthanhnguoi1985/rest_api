(ns cayenne.types-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get api-get-status setup-test-works]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration listing-types
  (testing "types endpoint returns types"
    (let [types (-> "/v1/types" api-get :items)]
      (is (seq types)))))

(deftest ^:integration retrieving-types
  (testing "type is retrieved"
    (is (= {:id "book-chapter" :label "Book Chapter"}
           (api-get "/v1/types/book-chapter"))))

  (testing "retrieving a non-existent type returns 404"
    (is (= 404 (api-get-status "/v1/types/nonexistent")))
    (is (= 404 (api-get-status "/v1/types/nonexistent/works")))))

(deftest ^:intgration filtering-works
  (testing "the same number of works is returned through /types/<id>/works route and the filter"
    (let [filter-total (-> "/v1/works?filter=type:journal-article" api-get :total-results)
          route-total (-> "/v1/types/journal-article/works" api-get :total-results)
          route-total-0-rows (-> "/v1/types/journal-article/works?rows=0" api-get :total-results)]
      (is (> filter-total 0))
      (is (= filter-total route-total))
      (is (= filter-total route-total-0-rows)))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-works))
