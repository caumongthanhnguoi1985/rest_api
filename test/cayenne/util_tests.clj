(ns cayenne.util-tests
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.util :refer [->sha1]]))

(deftest ^:unit convert-to-sha1
  (testing "converting to sha1 returns expected results"
    (is (= (->sha1 "The quick brown fox jumps over the lazy dog") "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"))
    (is (= (->sha1 "The quick brown fox jumps over the lazy cog") "de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3"))
    (is (= (->sha1 "") "da39a3ee5e6b4b0d3255bfef95601890afd80709"))))
