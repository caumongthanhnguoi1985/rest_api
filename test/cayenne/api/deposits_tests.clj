(ns cayenne.api.deposits-tests
  (:require [cayenne.api-fixture :refer [api-post api-get api-fixture setup-api]]
            [cayenne.api.auth.crossref]
            [cayenne.conf :as conf]
            [clojure.java.io :as io]
            [clojure.data.codec.base64 :refer [encode]]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [somnium.congomongo :as mongo]))

(deftest ^:integration pushing-deposits
  (testing "creating deposit redirects to new deposit"
    (with-redefs [cayenne.api.auth.crossref/authenticated? (fn [user pass] [user pass])]
      (doseq [file ["cayenne/deposits/journal_with_journal_article_deposit"
                    "cayenne/deposits/journal_fundref_test"
                    "cayenne/deposits/journal_with_component_deposit"]]
        (let [response (api-post 
                         "/v1/deposits?test=true" 
                         {:body (slurp (io/resource (str file ".xml")))
                          :headers {"authorization" (str "Basic " (String. (encode (.getBytes "user:pass"))))
                                    "content-type" "application/vnd.crossref.deposit+xml"}})
              deposit-id (get-in response [:headers "Location"])
              expected (read-string (slurp (io/resource (str file ".edn"))))
              actual (-> (str "/v1" deposit-id)
                         (api-get {:headers {"authorization" (str "Basic " (String. (encode (.getBytes "user:pass"))))}})
                         (dissoc :handoff)
                         (dissoc :submitted-at)
                         (dissoc :batch-id))]

          (is (= actual expected)))))))

(deftest ^:integration deposit-retrieval
  (testing "deposits created are returned"
    (with-redefs [cayenne.api.auth.crossref/authenticated? (fn [user pass] [user pass])]
      (doseq [file ["cayenne/deposits/journal_with_journal_article_deposit"
                    "cayenne/deposits/journal_fundref_test"
                    "cayenne/deposits/journal_with_component_deposit"]]
        (api-post 
          "/v1/deposits?test=true" 
          {:body (slurp (io/resource (str file ".xml")))
           :headers {"authorization" (str "Basic " (String. (encode (.getBytes "user:pass"))))
                     "content-type" "application/vnd.crossref.deposit+xml"}}))
      (let [actual (->> (api-get "/v1/deposits" {:headers {"authorization" (str "Basic " (String. (encode (.getBytes "user:pass"))))}})
                        :items
                        (map (fn [i]
                               (-> i
                                   (dissoc :handoff)
                                   (dissoc :submitted-at)
                                   (dissoc :batch-id))))
                        (sort-by :length <))
            expected (read-string (slurp (io/resource (str "cayenne/deposits/deposits.edn"))))]

        (is (= actual expected))))))

(use-fixtures :once (api-fixture setup-api))
(use-fixtures
  :each
  (fn [f]
    (f)
    (mongo/with-mongo (conf/get-service :mongo)
      (mongo/destroy! :deposits {}))))
