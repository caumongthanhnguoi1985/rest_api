(ns cayenne.api.v1.token-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.ingest.local-storage :as storage]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration token-test

  (conf/set-param! [:s3 :token-storage-service] (storage/->LocalS3))
  (conf/set-param! [:s3 :token-bucket] "dev-resources")
  (conf/set-param! [:s3 :token-file] "tokens.dat")

  (testing "api returns expected response with no token"
    (is (= (->> (str "/v1/works/")
                api-get)
           "Access Denied"))
    (is (= (->> (str "/v1/works/")
                api-get-status)
           401)))

  (testing "api returns expected response with invalid token"
    (is (= (-> (str "/v1/works/")
               (api-get {:headers {"authorization" "Bearer invalidToken1"}}))
           "Access Denied"))
    (is (= (-> (str "/v1/works/")
               (api-get-status {:headers {"authorization" "Bearer invalidToken1"}}))
           401)))

  (testing "api returns expected response with valid token"
    (is (= (api-get-status "/v1/works/" {:headers {"authorization" "Bearer testToken1"}})
           200))
    (is (= (api-get-status "/v1/works/" {:headers {"Crossref-Plus-API-Token" "Bearer testToken1"}})
           200)))
  
  (testing "api returns 404 for bad route and valid token"
    (is (= (api-get-status "/v1/aaaaa/" {:headers {"authorization" "Bearer testToken1"}})
           404)))

  (testing "api returns 404 for bad route and no token"
    (is (= (api-get-status "/v1/aaaaa/")
           404))))

(use-fixtures :once (api-fixture setup-api))
(use-fixtures
  :each
  (fn [f]
    (conf/set-param! [:service :token-auth] true)
    (f)
    (conf/set-param! [:service :token-auth] false)))
