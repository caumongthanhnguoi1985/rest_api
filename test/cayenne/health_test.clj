(ns cayenne.health-test
  (:require [cayenne.api-fixture :refer [api-get]]
            [clojure.test :refer [deftest testing is]]
            [qbits.spandex]
            [cayenne.elastic.util :refer [parse-open-contexts]]))

(deftest ^:unit elastic-search-stats-to-api-response
  (testing "For an Elastic Search /_nodes/stats/indices response, produce a JSON document structure with the same number."
    (let [data {:nodes {:_KSNIeT1SDukDTnsMJJNOQ {:indices {:search {:open_contexts 4}}}}}]
      (is (= (parse-open-contexts data) [4])
          "Number of contexts should be correctly retrieved.")))

  (testing "Name of node is not significant."
    (let [data {:nodes {:WHO_CARES {:indices {:search {:open_contexts 4}}}}}]
      (is (= (parse-open-contexts data) [4])
          "Number of contexts should be correctly retrieved.")))

  (testing "Multiple nodes' values are returned."
    (let [data {:nodes {:WHO_CARES {:indices {:search {:open_contexts 4}}}
                        :NODE_2 {:indices {:search {:open_contexts 5}}}
                        :NODE_20 {:indices {:search {:open_contexts 6}}}}}]
      (is (= (sort (parse-open-contexts data))
             (sort [4 5 6]))
          "Number of contexts should be correctly retrieved.")))

  (testing "Order of results is not dependent on order of inputs."
    ; Same input as before but re-ordered.
    (let [data {:nodes {:NODE_2 {:indices {:search {:open_contexts 5}}}
                        :WHO_CARES {:indices {:search {:open_contexts 4}}}
                        :NODE_20 {:indices {:search {:open_contexts 6}}}}}]
      (is (= (sort (parse-open-contexts data))
             (sort [4 5 6]))
          "Number of contexts should be correctly retrieved."))))

(deftest ^:unit retrieve-health-check
  (testing "Health check can be retrieved from the API with the appropriate values."
     (with-redefs [qbits.spandex/request
                   (constantly
                     {:body
                       {:nodes
                         {:NODE_2 {:indices {:search {:open_contexts 5}}}
                          :WHO_CARES {:indices {:search {:open_contexts 4}}}
                          :NODE_20 {:indices {:search {:open_contexts 6}}}}}})]
      (let [result (api-get "/health/cursors")]
        (is (= (sort (:cursor-sessions result))
               (sort [4 5 6])))))))
