(ns cayenne.ref-visibility-test
  (:require [cayenne.api-fixture :refer [api-get api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.data.work :as work]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [qbits.spandex :as elastic]
            [user :refer [flush-elastic]]))

(deftest ^:integration filtering-ref-visibility
  (let [member-data [{:index {:_id "1"}}
                     {:id "1"
                      :primary-name "name"
                      :location "loc"
                      :prefix [{:member-id "1" :name "" :value "10.5555" :reference-visibility "open"}
                               {:member-id "1" :name "" :value "10.5556" :reference-visibility "closed"}]}]
        member-request {:method :post
                        :url (str (elastic-util/index-url-prefix :member) "_bulk")
                        :body (elastic-util/raw-jsons member-data)}
        work-data [{:index {:_id "10.5555/test1"}} {:doi "10.5555/test1" :owner-prefix "10.5555" :prefix "10.5555"}
                   {:index {:_id "10.5555/test2"}} {:doi "10.5555/test2" :owner-prefix "10.5556" :prefix "10.5555"}]
        work-request {:method :post
                      :url (str (elastic-util/index-url-prefix :work) "_bulk")
                      :body (elastic-util/raw-jsons work-data)}]
    (elastic/request (conf/get-service :elastic) member-request)
    (elastic/request (conf/get-service :elastic) work-request)
    (flush-elastic)
    
    (testing "reference-visibility filters by owner-prefix"
      (doseq [[visibility doi] [["open" "10.5555/test1"]
                                ["closed" "10.5555/test2"]]]
        (is (= #{doi} (->> visibility
                           (str "/v1/works?filter=reference-visibility:")
                           api-get
                           :items
                           (map :DOI)
                           set)))))))

(deftest ^:integration reference-visibility-respects-config
  (let [member-data [{:index {:_id "1"}}
                     {:id "1"
                      :primary-name "name"
                      :location "loc"
                      :prefix [{:member-id "1" :name "" :value "10.1001" :reference-visibility "open"}]}
                     {:index {:_id "2"}}
                     {:id "2"
                      :primary-name "name"
                      :location "loc"
                      :prefix [{:member-id "2" :name "" :value "10.1002" :reference-visibility "limited"}]}
                     {:index {:_id "3"}}
                     {:id "3"
                      :primary-name "name"
                      :location "loc"
                      :prefix [{:member-id "3" :name "" :value "10.1003" :reference-visibility "closed"}]}]
        member-request {:method :post
                        :url (str (elastic-util/index-url-prefix :member) "_bulk")
                        :body (elastic-util/raw-jsons member-data)}
        work-data [{:index {:_id "10.1001/test1"}} {:doi "10.1001/test1" :owner-prefix "10.1001" :member-id 1 :prefix "10.1001"}
                   {:index {:_id "10.1001/test2"}} {:doi "10.1001/test2" :owner-prefix "10.1001" :member-id 1 :prefix "10.1001" :reference [{:doi "10.1001/test1"}]}

                   {:index {:_id "10.1002/test1"}} {:doi "10.1002/test1" :owner-prefix "10.1002" :member-id 2 :prefix "10.1002"}
                   {:index {:_id "10.1002/test2"}} {:doi "10.1002/test2" :owner-prefix "10.1002" :member-id 2 :prefix "10.1002" :reference [{:doi "10.1002/test1"}]}

                   {:index {:_id "10.1003/test1"}} {:doi "10.1003/test1" :owner-prefix "10.1003" :member-id 3 :prefix "10.1003"}
                   {:index {:_id "10.1003/test2"}} {:doi "10.1003/test2" :owner-prefix "10.1003" :member-id 3 :prefix "10.1003" :reference [{:doi "10.1003/test1"}]}

                   {:index {:_id "10.1001/test3"}} {:doi "10.1001/test3" :owner-prefix "10.1003" :member-id 3 :prefix "10.1001"}
                   {:index {:_id "10.1001/test4"}} {:doi "10.1001/test4" :owner-prefix "10.1003" :member-id 3 :prefix "10.1001" :reference [{:doi "10.1001/test3"}]}]
        work-request {:method :post
                      :url (str (elastic-util/index-url-prefix :work) "_bulk")
                      :body (elastic-util/raw-jsons work-data)}]
    (elastic/request (conf/get-service :elastic) member-request)
    (elastic/request (conf/get-service :elastic) work-request)
    (flush-elastic)
    
    (testing "references are shown based on configuration"
      (doseq [{:keys [config dois]} 
              [{:config "open" 
                :dois {"10.1001/test2" #{"10.1001/test1"}
                       "10.1002/test2" #{}
                       "10.1003/test2" #{}}}
               {:config "limited" 
                :dois {"10.1001/test2" #{"10.1001/test1"}
                       "10.1002/test2" #{"10.1002/test1"}
                       "10.1003/test2" #{}}}
               {:config "closed" 
                :dois {"10.1001/test2" #{"10.1001/test1"}
                       "10.1002/test2" #{"10.1002/test1"}
                       "10.1003/test2" #{"10.1003/test1"}}}
               ;; DOI 10.1001/test4 has a prefix that would make it "open" but an owner-prefix that makes it closed
               {:config "open" 
                :dois {"10.1001/test4" #{}}}
               {:config "closed" 
                :dois {"10.1001/test4" #{"10.1001/test3"}}}]]
        (conf/set-param! [:service :api :references] config)
        (work/display-citations-memo-clear!)
        (doseq [[doi expected] dois]
          (is (= expected
                 (->> doi
                      (str "/v1/works/")
                      api-get
                      :reference
                      (map :DOI)
                      set))
              (str "test failed for config: " config " and " "DOI: " doi)))))))

(use-fixtures
  :once
  (api-fixture setup-api))
