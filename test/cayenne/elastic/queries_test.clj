(ns cayenne.elastic.queries-test
  (:require [cayenne.api.v1.query :refer [->es-request cursor-expiration]]
            [cayenne.api.v1.filter :refer [std-filters]]
            [cayenne.data.member :as member]
            [cayenne.data.journal :as journal]
            [cayenne.data.funder :as funder]
            [cayenne.elastic.util :as elastic-util]
            [clojure.test :refer [deftest testing is]]))

(deftest ^:component es-query-members
  (let [base {:prefix-field :primary-name :offset 0 :filters {} :cursor nil :debug nil
              :rows 20, :rels [] :sample 0 :id nil :order :desc
              :facets [] :prefix-terms nil :sort nil :select nil :field-terms []}]

    (testing "ES query is properly constructed"
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :member) "_search")
              :body {:from 88 :size 40}}
             (-> base
                 (assoc :rows 40 :offset 88)
                 (->es-request :url-prefix (elastic-util/index-url-prefix :member))))))))

(deftest ^:component es-query-funders
  (let [base {:prefix-field :primary-name :offset 0 :filters {} :cursor nil :debug nil
              :rows 20 :rels [] :sample 0 :id nil :order :asc
              :facets [] :prefix-terms nil :sort [:level :_id] :select nil :field-terms []}]
    (testing "ES query is properly constructed"
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :funder) "_search")
              :body {:track_scores true :sort [{:level {:order :asc}} {:_id {:order :asc}}] :from 88 :size 40}}
             (-> base
                 (assoc :rows 40 :offset 88)
                 (->es-request :url-prefix (elastic-util/index-url-prefix :funder))))))))

(deftest ^:component es-query-bibliographic
  (let [base {:terms nil :offset 0 :filters {} :cursor nil :debug nil
                  :rows 20 :rels [] :sample 0 :id nil :order :desc
                  :facets [] :sort nil :select nil}
        query-type "bibliographic"
        query-term "quantum computing"]
       (testing "ES query is properly constructed"
         (is (= {:method :get
                 :url (str (elastic-util/index-url-prefix :work) "_search")
                 :body {:from 0
                        :size 20
                        :query {:bool {:must [{:match {:bibliographic-content-text {:query query-term
                                                                                    :minimum_should_match "20%"}}}]}}}}

                (-> base
                    (assoc :field-terms (vector [query-type query-term]))
                    (->es-request :url-prefix (elastic-util/index-url-prefix :work))))))))

(deftest ^:component es-query-works-with-query-value
  (let [base {:terms "quantum computing" :offset 0 :filters {} :cursor nil :debug nil
              :rows 20 :rels [] :sample 0 :id nil :order :desc
              :facets [] :sort nil :select nil :field-terms []}
        query-term "quantum computing"]
    (testing "ES query is properly constructed"
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :work) "_search")
              :body {:from 0
                     :size 20
                     :query {:bool {:must [{:match {:metadata-content-text {:query query-term
                                                                            :minimum_should_match "20%"}}}]}}}}
             (->es-request base :url-prefix (elastic-util/index-url-prefix :work)))))))

(deftest ^:component es-query-queries-filters
  (testing "queries and filters are AND-ed"
    (let [base {:offset 0 :cursor nil :debug nil :rows 20 :rels [] :sample 0 :id nil
                :order :desc :facets [] :sort nil :select nil :terms "quantum computing"
                :filters {"member" ["78"] "type" ["journal-article"]}
                :field-terms [["author" "richard"] ["bibliographic" "first"]]}
          query {:method :get
                 :url (str (elastic-util/index-url-prefix :work) "_search")
                 :body {:from 0 :size 20
                        :query {:bool {:must [{:match {:metadata-content-text {:query "quantum computing"
                                                                               :minimum_should_match "20%"}}}
                                              {:match {:author-text "richard"}}
                                              {:match {:bibliographic-content-text {:query "first"
                                                                                    :minimum_should_match "20%"}}}]
                                       :filter [{:terms {:member-id ["78"]}}
                                                {:terms {:type ["journal-article"]}}]
                                       :must_not []}}}}]
      (is (= query (->es-request base :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters))))))

(deftest ^:component es-query-works
  (let [base {:terms nil :offset 0 :filters {} :cursor nil :debug nil
              :rows 20 :rels [] :sample 0 :id nil :order :desc
              :facets [] :sort nil :select nil :field-terms '()}
        clean-es-query (fn [r] (-> r
                                   (dissoc :method)
                                   (update-in [:body] dissoc :query :aggs)))
        test-query (fn [& fields] (-> (apply assoc base fields)
                                      ->es-request
                                      clean-es-query))]

    (testing "correct ES query is constructed"
      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:from 88 :size 40}}
             (test-query :rows 40 :offset 88)))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:from 88 :size 40}}
             (test-query :rows 40 :offset 88 :terms "some words")))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:from 88 :size 40}}
             (-> base
                 (assoc :rows 40 :offset 88 :filters {:member [45]})
                 (->es-request :filters std-filters)
                 clean-es-query)))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:from 88 :size 40}}
             (test-query :rows 40 :offset 88 :facets [{:field "type-name" :count "*"}])))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search?scroll=" cursor-expiration) :body {:from 0 :size 40}}
             (test-query :rows 40 :cursor "*")))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search?scroll=" cursor-expiration) :body {:from 0 :size 40}}
             (test-query :rows 40 :cursor "*" :terms "some words")))

      (is (= {:url "/_search/scroll" :body {:scroll cursor-expiration, :scroll_id "cid"}}
             (test-query :cursor "cid" :rows 40)))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:track_scores true :sort [{:is-referenced-by-count {:order :desc}}], :from 0, :size 20}}
             (test-query :sort [:is-referenced-by-count])))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:track_scores true :sort [{:references-count {:order :asc}}], :from 0, :size 20}}
             (test-query :sort [:references-count] :order :asc)))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search") :body {:track_scores true :sort [{:references-count {:order :desc}}], :from 88, :size 40}}
             (test-query :rows 40 :offset 88 :sort [:references-count])))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search?scroll=" cursor-expiration) :body {:track_scores true :sort [{:references-count {:order :desc}}] :from 0 :size 20}}
             (test-query :sort [:references-count] :cursor "*")))

      (is (= {:url (str (elastic-util/index-url-prefix :work) "_search?scroll=" cursor-expiration) :body {:track_scores true :sort [{:references-count {:order :asc}}] :from 0 :size 20}}
             (test-query :sort [:references-count] :order :asc :cursor "*"))))))

(deftest ^:component es-query-journals
  (let [base {:prefix-field :title :offset 0 :filters {} :cursor nil :debug nil
              :rows 20 :rels [] :sample 0 :id nil :order :desc
              :facets [] :prefix-terms nil :sort nil :select nil :field-terms []}]

    (testing "ES query is properly constructed"
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :journal) "_search")
              :body {:from 88 :size 40}}
             (-> base
                 (assoc :rows 40 :offset 88)
                 (->es-request :url-prefix (elastic-util/index-url-prefix :journal))))))))

(deftest ^:component es-query-with-id
  (testing "ES query with id is properly constructed"
    (doseq [[id-field value] [[:type "journal-article"]
                              [:member-id "78"]
                              [:journal-id "0306-4530"]
                              [:owner-prefix "10.1016"]]]
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :work) "_search")
              :body {:query {:bool {:filter [{:term {id-field value}}]}} :from 0 :size 20}}
             (->es-request
               {:terms nil :offset 0 :filters {} :cursor nil :debug nil :rows 20 :rels []
                :sample 0 :id value :order :desc :facets [] :sort nil :select nil :field-terms []}
               :url-prefix (elastic-util/index-url-prefix :work)
               :id-field id-field)))))
  (testing "ES singleton query with id is properly constructed"
    (doseq [[index value] [[:member "78"]
                           [:journal "0306-4530"]]]
      (is (= {:method :get :url (str (elastic-util/index-url-prefix index) "_search")
              :body {:query {:bool {:filter [{:term {:id value}}]}} :from 0 :size 20}}
             (->es-request
               {:id value :rows 20 :debug nil}
               :url-prefix (elastic-util/index-url-prefix index)
               :id-field :id))))))

(deftest ^:unit members-default-sort
  (testing "ES request for retrieving members specifies correct sorting order"
    (let [base-context {:terms nil :offset 0 :filters {} :cursor nil :debug nil :rows 20 :rels []
                        :sample 0 :id nil :order :desc :facets [] :sort nil :select nil :field-terms []}
          expected-sort-params [{:_score {:order :desc}} {:id {:order :asc}}]]
      (doseq [context [base-context
                       (assoc base-context :terms "association")
                       (assoc base-context :filters {"reference-visibility" ["open"]})
                       (assoc base-context :rows 34 :offset 102)
                       (assoc base-context :sort [:primary-name] :order :asc)]]
        (is (= expected-sort-params (-> context member/es-request-with-member-sort :body :sort)))))))

(deftest ^:unit journals-default-sort
  (testing "ES request for retrieving journals specifies correct sorting order"
    (let [base-context {:terms nil :offset 0 :filters {} :cursor nil :debug nil :rows 20 :rels []
                        :sample 0 :id nil :order :desc :facets [] :sort nil :select nil :field-terms []}
          expected-sort-params [{:_score {:order :desc}} {:id {:order :asc}}]]
      (doseq [context [base-context
                       (assoc base-context :terms "journal")
                       (assoc base-context :rows 34 :offset 102)
                       (assoc base-context :sort [:title] :order :asc)]]
        (is (= expected-sort-params (-> context journal/es-request-with-journal-sort :body :sort)))))))

(deftest ^:unit funders-default-sort
  (testing "ES request for retrieving funders specifies correct sorting order"
    (let [base-context {:terms nil :offset 0 :filters {} :cursor nil :debug nil :rows 20 :rels []
                        :sample 0 :id nil :order :desc :facets [] :sort nil :select nil :field-terms []}
          expected-sort-params [{:_score {:order :desc}} {:level {:order :asc}} {:id {:order :asc}}]]
      (doseq [context [base-context
                       (assoc base-context :terms "foundation")
                       (assoc base-context :filters {"location" ["Spain"]})
                       (assoc base-context :rows 34 :offset 102)
                       (assoc base-context :sort [:title] :order :asc)]]
        (is (= expected-sort-params (-> context funder/es-request-with-funder-sort :body :sort)))))))

(deftest ^:component es-sampling
  (testing "ES query for sampling is properly constructed"
    (let [base {:terms nil :offset 0 :filters {} :cursor nil :debug nil
                :rows 20 :rels [] :sample 10 :id nil :order :desc
                :facets [] :sort nil :select nil :field-terms []}
          clean-query (fn [q] (-> q
                                  :body
                                  :query
                                  (assoc-in [:function_score :functions 0 :random_score :seed] 0)))]
      (is (= {:function_score {:query {:bool {:must {:match_all {}}}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work))
                 clean-query)))
      (is (= {:function_score {:query {:bool {:must [{:match {:metadata-content-text {:query "first"
                                                                                      :minimum_should_match "20%"}}}]}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (assoc :terms "first")
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work))
                 clean-query)))
      (is (= {:function_score {:query {:bool {:must [{:match {:author-text "richard"}}]}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (assoc :field-terms [["author" "richard"]])
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work))
                 clean-query)))
      (is (= {:function_score {:query {:bool {:filter [{:terms {:member-id ["78"]}}]
                                              :must_not []
                                              :must {:match_all {}}}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (assoc :filters {"member" ["78"]})
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters)
                 clean-query)))
      (is (= {:function_score {:query {:bool {:filter [{:terms {:member-id ["78"]}}]
                                              :must_not []
                                              :must [{:match {:metadata-content-text {:query "first"
                                                                                      :minimum_should_match "20%"}}}]}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (assoc :terms "first")
                 (assoc :filters {"member" ["78"]})
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters)
                 clean-query)))
      (is (= {:function_score {:query {:bool {:filter [{:terms {:member-id ["78"]}}]
                                              :must_not []
                                              :must [{:match {:author-text "richard"}}]}}
                               :functions [{:random_score {:seed 0 :field :_seq_no}}]}}
             (-> base
                 (assoc :field-terms [["author" "richard"]])
                 (assoc :filters {"member" ["78"]})
                 (->es-request :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters)
                 clean-query))))))

(deftest ^:component works-nested-filter
  (testing "ES query is properly constructed"
    (let [request {:terms nil :offset 0 :filters {"award" {"funder" ["10.13039/100004440"]}}
                   :cursor nil :debug nil :rows 20 :rels [] :sample 0 :id nil :order :desc
                   :facets [] :sort nil :select nil :field-terms []}]
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :work) "_search")
              :body {:query {:bool {:filter [{:terms {:funder-doi ["10.13039/100004440"]}}]
                                    :must_not []}}
                     :from 0
                     :size 20}}
             (->es-request request :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters))))

    (let [request {:terms nil :offset 0 :filters {"award" {"number" ["dW10"]}}
                   :cursor nil :debug nil :rows 20 :rels [] :sample 0 :id nil :order :desc
                   :facets [] :sort nil :select nil :field-terms []}]
      (is (= {:method :get
              :url (str (elastic-util/index-url-prefix :work) "_search")
              :body {:query {:bool {:filter [{:bool {:should [{:terms {:award-keyword ["dw10"]}}
                                                              {:nested {:path :funder
                                                                        :query {:terms {:funder.award-keyword ["dw10"]}}}}]
                                                     :minimum_should_match 1}}]
                                    :must_not []}}
                     :from 0
                     :size 20}}
             (->es-request request :url-prefix (elastic-util/index-url-prefix :work) :filters std-filters))))))

