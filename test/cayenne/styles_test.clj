(ns cayenne.styles-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration listing-styles
  (testing "styles endpoint returns styles"
    (let [styles (-> "/v1/styles" api-get :items)]
      (is (seq styles))
      (is (some #{"apa"} styles)))))

(use-fixtures
  :once
  (api-fixture setup-api))
