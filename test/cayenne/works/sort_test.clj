(ns cayenne.works.sort-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration sorting-works

  (testing "query results are sorted by score by default"
    (let [items (:items (api-get "/v1/works?query=Psychoneuroendocrinology&rows=150"))
          values (map :score items)]
      (is (apply >= values))))

  (testing "the results are sorted accordingly to sort and order parameters"
    (let [date-parts-to-int (fn [{[parts] :date-parts}]
                              (if (or (nil? parts) (nil? (first parts)))
                                nil
                                (let [[y m d] (concat parts [1 1])]
                                  (+ (* 10000 y) (* 100 m) d))))]
      (doseq [[field extr-fun] [["score" :score]
                                ["relevance" :score]
                                ["created" (comp :timestamp :created)]
                                ["indexed" (comp :timestamp :indexed)]
                                ["is-referenced-by-count" :is-referenced-by-count]
                                ["references-count" :references-count]
                                ["published-print" (comp date-parts-to-int :published-print)]
                                ["published" (comp date-parts-to-int :published)]
                                ["issued" (comp date-parts-to-int :issued)]
                                ["updated" (comp :timestamp :deposited)]
                                ["deposited" (comp :timestamp :deposited)]]]

        (let [items (api-get (str "/v1/works?query=Psychoneuroendocrinology&rows=150&sort=" field))
              values (->> items :items (map extr-fun) (remove nil?))]
          (is (apply >= values) (format "order of %s should be descending with default order" field)))
        (let [items (api-get (str "/v1/works?query=Psychoneuroendocrinology&rows=150&sort=" field "&order=asc"))
              values (->> items :items (map extr-fun) (remove nil?))]
          (is (apply <= values) (format "order of %s should be ascending" field)))
        (let [items (api-get (str "/v1/works?query=Psychoneuroendocrinology&rows=150&sort=" field "&order=desc&"))
              values (->> items :items (map extr-fun) (remove nil?))]
          (is (apply >= values) (format "order of %s should be descending" field))))))
  
  (testing "grants are sorted accordingly to sort and order parameters"
    (let [date-parts-to-int (fn [{[parts] :date-parts}]
                              (if (or (nil? parts) (nil? (first parts)))
                                nil
                                (let [[y m d] (concat parts [1 1])]
                                  (+ (* 10000 y) (* 100 m) d))))]
      (doseq [[field extr-fun] [["score" :score]
                                ["relevance" :score]
                                ["created" (comp :timestamp :created)]
                                ["indexed" (comp :timestamp :indexed)]
                                ["issued" (comp date-parts-to-int :issued)]
                                ["updated" (comp :timestamp :deposited)]
                                ["deposited" (comp :timestamp :deposited)]]]

        (let [items (api-get (str "/v1/works?filter=type:grant&sort=" field "&order=asc&"))
              values (->> items :items (map extr-fun) (remove nil?))]
          (is (apply <= values) (format "order of %s should be descending" field)))
        (let [items (api-get (str "/v1/works?filter=type:grant&sort=" field "&order=desc&"))
              values (->> items :items (map extr-fun) (remove nil?))]
          (is (apply >= values) (format "order of %s should be descending" field))))))
  
  (testing "offset can be used with custom sorting"
    (doseq [[field extr-fun] [["is-referenced-by-count" :is-referenced-by-count]
                              ["deposited" (comp :timestamp :deposited)]]]
      (let [offsets (range 0 100 10)
            queries (map (partial str "/v1/works?sort=" field "&rows=10&offset=") offsets)
            items (mapcat (comp :items api-get) queries)
            values (->> items (map extr-fun) (remove nil?))]
        (is (apply >= values) (format "order of %s should be descending" field)))))
  
  (testing "cursor can be used with custom sorting"
    (doseq [[field extr-fun] [["is-referenced-by-count" :is-referenced-by-count]
                              ["deposited" (comp :timestamp :deposited)]]]
      (let [get-items (fn lazy-cursor
                        ([] (lazy-cursor "*"))
                        ([cursor] (let [response (->> cursor (str "/v1/works?sort=" field "&rows=10&cursor=") api-get)
                                        items (:items response)]
                                    (if (empty? items)
                                      items
                                      (lazy-cat items (lazy-cursor (:next-cursor response)))))))
            items (take 100 (get-items))
            values (->> items (map extr-fun) (remove nil?))]
        (is (apply >= values) (format "order of %s should be descending" field))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-works))
