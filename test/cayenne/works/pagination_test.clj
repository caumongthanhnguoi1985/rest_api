(ns cayenne.works.pagination-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works api-get]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration offset-works
  (testing "offset retrieves consecutive ranges of items"
    (doseq [query ["/v1/works?"
                   "/v1/works?query=the&"
                   "/v1/works?filter=member:78&"
                   "/v1/works?facet=type-name:*&"]
            [start range-1 range-2] [[0 27 48] [11 45 29] [3 59 0] [49 0 49] [21 0 0]]]
      (let [get-item-range (fn [start size] (-> (str query "rows=" size "&offset=" start) api-get :items))
            items-all (get-item-range start (+ range-1 range-2))
            items-1 (get-item-range start range-1)
            items-2 (get-item-range (+ start range-1) range-2)]
        (is (= items-all (concat items-1 items-2)))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-works))
