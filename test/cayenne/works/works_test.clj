(ns cayenne.works.works-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works
                                         setup-test-journals setup-test-subjects
                                         setup-test-members api-get api-root]]
            [cayenne.data.work :as work]
            [cayenne.api.v1.types :as types]
            [clj-http.client :as http]
            [org.httpkit.client :as httpkit-client]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [clojure.java.io :refer [resource]]))

(deftest ^:integration listing-works
  (testing "works endpoint returns results"
    (is (seq (-> "/v1/works" api-get :items)))))

(deftest ^:integration sampling-works
  (testing "works endpoint returns results for sample"
    (let [response (api-get "/v1/works?sample=100")
          expected-count 100]
      (is (= (count (:items response)) expected-count)))))

(deftest ^:integration relevance-scores
  (testing "scores appear in the API response"
    (doseq [query ["/v1/works"
                   "/v1/works?rows=100&offset=20"
                   "/v1/works?rows=500&query=the"
                   "/v1/works?rows=500&filter=member:78"
                   "/v1/works?rows=500&select=title,score"
                   "/v1/works?rows=500&cursor=*"
                   "/v1/works?rows=500&sort=is-referenced-by-count"]]
      (let [scores (->> query api-get :items (map :score))]
        (is (every? (complement nil?) scores))))))

(deftest ^:integration illegal-parameter-combinations
  (testing "illegal combination of parameters returns error"
    (is (= "cursor-with-offset-or-sample"
           (-> "/v1/works?cursor=*&offset=10" api-get first :type)))
    (is (= "cursor-with-offset-or-sample"
           (-> "/v1/works?cursor=*&sample=10" api-get first :type)))
    (is (= "sample-with-rows-or-offset"
           (-> "/v1/works?sample=10&offset=10" api-get first :type)))))

(deftest ^:integration selecting-works
  (testing "selected fields are included in the results"
    (doseq [[field-name fields] [["abstract" [:abstract]]
                                 ["accepted" [:accepted]]
                                 ["alternative-id" [:alternative-id]]
                                 ["approved" [:approved]]
                                 ["archive" [:archive]]
                                 ["article-number" [:article-number]]
                                 ["assertion" [:assertion]]
                                 ["author" [:author]]
                                 ["chair" [:chair]]
                                 ["clinical-trial-number" [:clinical-trial-number]]
                                 ["container-title" [:container-title]]
                                 ["content-created" [:content-created]]
                                 ["content-domain" [:content-domain]]
                                 ["created" [:created]]
                                 ["degree" [:degree]]
                                 ["deposited" [:deposited]]
                                 ["DOI" [:DOI]]
                                 ["editor" [:editor]]
                                 ["event" [:event]]
                                 ["funder" [:funder]]
                                 ["group-title" [:group-title]]
                                 ["indexed" [:indexed]]
                                 ["is-referenced-by-count" [:is-referenced-by-count]]
                                 ["ISBN" [:ISBN]]
                                 ["ISSN" [:ISSN]]
                                 ["issn-type" [:issn-type]]
                                 ["issue" [:issue]]
                                 ["issued" [:issued]]
                                 ["license" [:license]]
                                 ["link" [:link]]
                                 ["member" [:member]]
                                 ["original-title" [:original-title]]
                                 ["page" [:page]]
                                 ["posted" [:posted]]
                                 ["prefix" [:prefix]]
                                 ["published" [:published]]
                                 ["published,issued" [:published :issued]]
                                 ["published-online" [:published-online]]
                                 ["published-print" [:published-print]]
                                 ["publisher" [:publisher]]
                                 ["publisher-location" [:publisher-location]]
                                 ["short-container-title" [:short-container-title]]
                                 ["short-title" [:short-title]]
                                 ["score" [:score]]
                                 ["reference" [:reference]]
                                 ["references-count" [:references-count]]
                                 ["relation" [:relation]]
                                 ["standards-body" [:standards-body]]
                                 ["subject" [:subject]]
                                 ["subtitle" [:subtitle]]
                                 ["title" [:title]]
                                 ["translator" [:translator]]
                                 ["type" [:type]]
                                 ["update-policy" [:update-policy]]
                                 ["update-to" [:update-to]]
                                 ["URL" [:URL]]
                                 ["volume" [:volume]]]]
      (let [items (->> field-name (str "/v1/works?rows=500&select=") api-get :items)]
        (doseq [field fields]
          (is (some field items) (str "failed for " field-name))))))
  
  (testing "some fields are never included in grants"
    (doseq [[field-name field] [["container-title" :container-title]
                                ["content-domain" :content-domain]
                                ["is-referenced-by-count" :is-referenced-by-count]
                                ["original-title" :original-title]
                                ["references-count" :references-count]
                                ["title" :title]]]
      (let [items (->> field-name (str "/v1/works?filter=type:grant&select=DOI,") api-get :items)]
        (is (not-any? field items) (str "failed for " field-name))))))

(deftest ^:integration transform-works

  (testing "works endpoint returns expected result for transform"
    (doseq [doi ["10.1016/j.psyneuen.2016.10.018"
                 "10.7287/peerj.2196v0.1/reviews/2"
                 "10.7717/peerj.1698"]]
      (doseq [content-type (->> types/work-transform
                                (remove #{"application/x-bibtex"
                                          "application/json"
                                          "application/citeproc+json"
                                          "application/vnd.citationstyles.csl+json"}))]
        (with-redefs
          [httpkit-client/get
           (fn [_ _]
             ;; trasforming to unixref and unixsd xml results in a call to an upstream http
             ;; service, here we mock it out to slurp from a file instead
             (let [file (if (str/ends-with? content-type "unixref+xml")
                          "application/vnd.crossref.unixref+xml"
                          "application/vnd.crossref.unixsd+xml")]
               (atom {:body (slurp (resource (str "works/" doi "/" file)))})))]
          (let [response (->> {:accept content-type}
                              (http/get (str api-root "/v1/works/" doi "/transform"))
                              :body)
                expected-response (slurp (resource (str "works/" doi "/transform/" content-type)))]
            (is (= expected-response response) (str "unexpected result for transform of " doi " to " content-type)))))

      (doseq [content-type ["application/json"
                            "application/citeproc+json"
                            "application/vnd.citationstyles.csl+json"]]
        (let [response (->> {:accept content-type}
                            (http/get (str api-root "/v1/works/" doi "/transform"))
                            :body
                            (#(json/read-str % :key-fn keyword)))]
          (is (= doi (:DOI response)))))

      (let [response (->> {:accept "application/x-bibtex"}
                          (http/get (str api-root "/v1/works/" doi "/transform"))
                          :body)]
        (is (str/includes? response doi))))))

(deftest ^:integration works-agency

  (testing "works endpoint returns expected result for DOI agency"
    (with-redefs
      [work/get-agency
       (fn [d]
         (case d
           "10.1016/j.psyneuen.2016.10.018" {:body (json/write-str [{:RA "Crossref"}])}
           "10.5167/UZH-30455" {:body (json/write-str [{:RA "DataCite"}])}
           {:body (json/write-str [{:RA "Unknown Agency"}])}))]
      (doseq [doi ["10.1016/j.psyneuen.2016.10.018" "10.5167/UZH-30455"]]
        (let [response (api-get (str "/v1/works/" doi "/agency"))
              expected-response (read-string (slurp (resource (str "works/" doi "-agency.edn"))))]
          (is (= expected-response response)))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-members setup-test-journals setup-test-subjects setup-test-works))
