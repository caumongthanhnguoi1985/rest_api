(ns cayenne.works.nil-test
  (:require [cayenne.api-fixture :refer [api-get setup-api]]
            [clojure.test :refer [deftest testing is]]
            [user :refer [flush-elastic index-work-files]]))

(def index-files
  ["dev-resources/metadata-cases/event-nil.body"
   "dev-resources/metadata-cases/clinical-trial-nil.body"
   "dev-resources/metadata-cases/reference-nil.body"
   "dev-resources/metadata-cases/institution-nil.body"
   "dev-resources/metadata-cases/institution-nil-2.body"
   "dev-resources/metadata-cases/event-nil-2.body"])

(defn chk-nils
  [h]
  (map
    #(cond
       ;if it's not a vector
       (and (coll? %) (not (sequential? %)))
      ;if it is a hashmap, check values of the hashmap
       (cond
           ; if data structure has values and can form a collection, recurse to check its values
        (coll? (vals %)) (chk-nils (vals %))
          ; if datastructure is empty
        (empty? %) "empty")
      ; if it's an empty vector
      (and (coll? %) (empty? (filter some? %))) "empty"
      ; just a nil value
      (nil? %) "empty"
      :else nil) h))

(deftest ^:integration check-nil-empty-containers
  (testing "works returns non-nil values and non-empty containers for certain fields")
  (setup-api)
  (index-work-files index-files)
  (flush-elastic)
  (doseq [[doi field] [["10.1093/jat/bkw129" :reference]
                       ["10.1136/bmjopen-2019-030788" :clinical-trial-number]
                       ["10.1115/imece2008-69048" :event]
                       ["10.5753/eniac.2019" :event]
                       ["10.1601/nm.14265" :institution]
                       ["10.1101/026963" :institution]]]
    (let [response (field (api-get (str "/v1/works/" doi)))
          checked-response (if (= field :event)
                             (filter some? (flatten (chk-nils (vals response))))
                             (filter some? (flatten (map #(-> (vals %) (chk-nils)) response))))]
      (is (empty? checked-response)))))
