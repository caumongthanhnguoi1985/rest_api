(ns cayenne.journals.journals-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status api-fixture 
                                         setup-api setup-test-journals setup-test-works]]
            [clojure.java.io :as io]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration all-titles-indexed
  (testing "When all titles are ingested there should be the same number in the index."

    (let [num-lines (with-open [r (-> "titles.csv"
                                      io/resource
                                      io/reader)]
                      (-> r line-seq count))]

      (is (= (-> "/v1/journals" api-get :total-results)
             ; Ignore header line.
             (dec num-lines))
        "Number of results should match number of entries in the input file."))))

(deftest ^:integration offset-journals
  (testing "offset retrieves consecutive ranges of journals"
    (doseq  [[start range-1 range-2] [[0 27 48] [11 45 29] [3 59 0] [49 0 49] [21 0 0]]]
      (let [get-item-range (fn [start size] (-> (str "/v1/journals?rows=" size "&offset=" start) api-get :items))
            items-all (get-item-range start (+ range-1 range-2))
            items-1 (get-item-range start range-1)
            items-2 (get-item-range (+ start range-1) range-2)]
        (is (= items-all (concat items-1 items-2)))))))

(deftest ^:integration cursor-journals
  (testing "iteration with cursors retrieves the entire dataset"
    (let [iterate-journals (fn it-j [cursor]
                             (let [journals-page (->> cursor (str "/v1/journals?rows=50&cursor=") api-get)
                                   items (:items journals-page)]
                               (if (empty? items)
                                 []
                                 (lazy-cat items (it-j (:next-cursor journals-page))))))
          cursor-journals (set (iterate-journals "*"))
          all-journals (-> "/v1/journals?rows=500" api-get :items set)]
      (is (= all-journals cursor-journals)))))

(def present-issns
  "These are known to be present in the test fixture titles.csv file"
  #{"2542-1298" "2303-5595"})

(deftest ^:integration all-journals
  (testing "Journals endpoint includes all known journals."
    (let [response (:items (api-get "/v1/journals?rows=500"))]
      (is (seq response))
      (is (every? (->> response (mapcat :ISSN) set) present-issns)
          "Every known ISSN should be present in the response."))))

(deftest ^:integration journal-by-issn
  (testing "Journal endpoint returns journals with the given ISSN."
    (doseq [issn present-issns]
      (let [response (api-get (str "/v1/journals/" issn))]

        (is (-> response :ISSN set (contains? issn))
            "ISSN should be returned")
        (is (contains? (->> response :issn-type (map :value) set)
                       issn)
            "ISSN should be present in one of the issn types"))))
  
  (testing "retrieving a non-existent journal returns 404"
    (is (= 404 (api-get-status "/v1/journals/nonexistent")))
    (is (= 404 (api-get-status "/v1/journals/9999-9999")))
    (is (= 404 (api-get-status "/v1/journals/nonexistent/works")))
    (is (= 404 (api-get-status "/v1/journals/9999-9999/works")))))

(deftest ^:integration works-by-journal
  (testing "All works retrieved by ISSN have the correct ISSN."
           ; NB Weirdly, this one isn't present in the title file.
           (let [issn "0306-4530"
                 response (api-get (str "/v1/journals/" issn "/works"))]
            (is (-> response :items not-empty) "Some results returned.")
            (is (->> response :items (mapcat :ISSN) set (every? #{issn}))
                "Every item should have the given ISSN"))))

(deftest ^:integration querying-journals

  (testing "journals endpoint returns result for query"
    (doseq [query ["journal"
                   "journal+of"
                   "advanced+care"
                   "care+advanced"
                   "advanc+car"
                   "car+adva"
                   "Christian+bioethics+Non-Ecumenical"]]
      (let [journals (->> query (str "/v1/journals?query=") api-get :items)]
        (is (seq journals))))))

(deftest ^:intgration filtering-works
  (testing "the same number of works is returned through /journals/<id>/works route and the filter"
    (let [filter-total (-> "/v1/works?filter=issn:0306-4530" api-get :total-results)
          route-total (-> "/v1/journals/0306-4530/works" api-get :total-results)
          route-total-0-rows (-> "/v1/journals/0306-4530/works?rows=0" api-get :total-results)]
      (is (> filter-total 0))
      (is (= filter-total route-total))
      (is (= filter-total route-total-0-rows)))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-journals setup-test-works))
