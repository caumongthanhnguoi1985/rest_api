<?xml version="1.0" encoding="UTF-8"?>
<crossref_result xmlns="http://www.crossref.org/qrschema/3.0" version="3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/qrschema/3.0 http://www.crossref.org/schemas/crossref_query_output3.0.xsd">
  <query_result>
    <head>
      <doi_batch_id>none</doi_batch_id>
    </head>
    <body>
      <query status="resolved">
        <doi type="journal_article">10.1039/D0TC00148A</doi>
        <crm-item name="publisher-name" type="string">Royal Society of Chemistry (RSC)</crm-item>
        <crm-item name="prefix-name" type="string">The Royal Society of Chemistry</crm-item>
        <crm-item name="member-id" type="number">292</crm-item>
        <crm-item name="citation-id" type="number">114636385</crm-item>
        <crm-item name="journal-id" type="number">185186</crm-item>
        <crm-item name="deposit-timestamp" type="number">20200316030327</crm-item>
        <crm-item name="owner-prefix" type="string">10.1039</crm-item>
        <crm-item name="last-update" type="date">2020-03-16T03:03:26Z</crm-item>
        <crm-item name="created" type="date">2020-03-16T03:03:25Z</crm-item>
        <crm-item name="citedby-count" type="number">0</crm-item>
        <doi_record>
          <crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
            <journal>
              <journal_metadata language="en">
                <full_title>Journal of Materials Chemistry C</full_title>
                <abbrev_title>J. Mater. Chem. C</abbrev_title>
                <issn media_type="print">2050-7526</issn>
                <issn media_type="electronic">2050-7534</issn>
                <coden>JMCCCX</coden>
              </journal_metadata>
              <journal_issue>
                <publication_date media_type="print">
                  <year>2020</year>
                </publication_date>
              </journal_issue>
              <journal_article publication_type="full_text">
                <titles>
                  <title>Enhanced dielectric performance in flexible MWCNTs/poly(vinylidene fluoride-co-hexafluoropropene)-based nanocomposites by the design of tri-layered structure</title>
                </titles>
                <contributors>
                  <person_name sequence="first" contributor_role="author">
                    <given_name>Jie</given_name>
                    <surname>Chen</surname>
                  </person_name>
                  <person_name sequence="additional" contributor_role="author">
                    <given_name>Yifei</given_name>
                    <surname>Wang</surname>
                  </person_name>
                  <person_name sequence="additional" contributor_role="author">
                    <given_name>Jiufeng</given_name>
                    <surname>Dong</surname>
                  </person_name>
                  <person_name sequence="additional" contributor_role="author">
                    <given_name>Yujuan</given_name>
                    <surname>Niu</surname>
                  </person_name>
                  <person_name sequence="additional" contributor_role="author">
                    <given_name>Weixing</given_name>
                    <surname>Chen</surname>
                  </person_name>
                  <person_name sequence="additional" contributor_role="author">
                    <given_name>Hong</given_name>
                    <surname>Wang</surname>
                  </person_name>
                </contributors>
                <abstract xmlns="http://www.ncbi.nlm.nih.gov/JATS1" abstract-type="toc">
                  <p>Polymer composites with high permittivity are promising applications in advanced electronics. However, it remains a challenge to achieve high permittivity, low dielectric loss, and high flexibility in the single layer...</p>
                </abstract>
                <abstract xmlns="http://www.ncbi.nlm.nih.gov/JATS1">
                  <p>Polymer composites with high permittivity are promising applications in advanced electronics. However, it remains a challenge to achieve high permittivity, low dielectric loss, and high flexibility in the single layer nanocomposites, simultaneously. In this work, a series of ternary tri-layered structure films is presented via a facile solution-casting process. Two outer layers of poly(vinylidene fluoride-co-hexafluoropropene) (P(VDF-HFP)) ferroelectric copolymer matrix are filled with acid-treated multi-walled carbon nanotubes (MWCNT) to enhance permittivity, while the inter layer of P(VDF-HFP) blended with poly(methyl methacrylate) (PMMA) can effectively suppress the dielectric losses. The acid-treated MWCNT filler, organic-organic blend, and tri-layered structure contribute to the increase of permittivity, decrease of dielectric loss, and favorable mechanical reliability. As a result, the maximized permittivity of 21 and low dielectric loss of 0.05 have been endowed in the tri-layered composites with an optimized MWCNT content of 9 wt.% at a test frequency of 1kHz. Specifically, excellent capacitive stability is demonstrated in this trilayered film over straight bending cycles (i.e. 20,000 cycles) and winding (i.e. 120 hours) tests. These attractive features of the designed tri-layered structure composites manifest that the facile approach proposed herein is scalable that can be extended to develop flexible composites with high permittivity and low dielectric loss for dielectric applications.</p>
                </abstract>
                <publication_date media_type="online">
                  <year>2020</year>
                </publication_date>
                <publisher_item>
                  <item_number item_number_type="sequence-number">10.1039.D0TC00148A</item_number>
                </publisher_item>
                <crossmark>
                  <crossmark_version>1</crossmark_version>
                  <crossmark_policy>10.1039/rsc_crossmark_policy</crossmark_policy>
                  <crossmark_domains>
                    <crossmark_domain>
                      <domain>rsc.org</domain>
                    </crossmark_domain>
                  </crossmark_domains>
                  <crossmark_domain_exclusive>true</crossmark_domain_exclusive>
                  <custom_metadata>
                    <assertion name="similarity_check" group_label="SIMILARITY CHECK" group_name="similarity_check" explanation="https://www.crossref.org/services/similarity-check/">This document is Similarity Check deposited</assertion>
                    <assertion name="related_data" group_name="related_data" group_label="RELATED DATA" href="https://doi.org/10.1039/D0TC00148A">Supplementary Information</assertion>
                    <assertion name="peer_review_method" group_label="PEER REVIEW METHOD" group_name="peer_review_method" order="2">Single-blind</assertion>
                    <assertion name="history" group_name="publication_history" group_label="PUBLICATION HISTORY" order="1">Received 10 January 2020; Accepted 13 March 2020;  Accepted Manuscript published 16 March 2020</assertion>
                    <program xmlns="http://www.crossref.org/AccessIndicators.xsd" name="AccessIndicators">
                      <!--standardLtP-->
                      <license_ref applies_to="am" start_date="2021-03-16">http://rsc.li/journals-terms-of-use</license_ref>
                      <!--online-date: 16 March 2020, OALicenceDate from Resource: calculated-embargo-start-date:16 March 2020-->
                    </program>
                  </custom_metadata>
                </crossmark>
                <doi_data>
                  <doi>10.1039/D0TC00148A</doi>
                  <timestamp>20200316030327</timestamp>
                  <resource>http://pubs.rsc.org/en/Content/ArticleLanding/2020/TC/D0TC00148A</resource>
                  <collection property="crawler-based">
                    <item crawler="iParadigms">
                      <resource>http://pubs.rsc.org/en/content/articlepdf/2020/TC/D0TC00148A</resource>
                    </item>
                  </collection>
                </doi_data>
              </journal_article>
            </journal>
          </crossref>
        </doi_record>
      </query>
    </body>
  </query_result>
</crossref_result>
