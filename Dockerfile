FROM clojure

COPY . /usr/src/app
RUN mkdir /usr/src/app/data
WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y git curl unzip
RUN git submodule update --init
RUN lein deps
RUN curl -o install-clj-kondo https://raw.githubusercontent.com/borkdude/clj-kondo/master/script/install-clj-kondo
RUN bash install-clj-kondo --version 2021.02.13
CMD ["bash","docker-entrypoint.sh"]
