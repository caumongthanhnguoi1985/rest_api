(ns cayenne.data.coverage
  (:require [clj-time.coerce :as dc]))

(defn- remove-zero-count-nodes [m]
  (reduce-kv
   (fn [m k v]
     (if (pos? (:_count v))
       (assoc m k (dissoc  v :_count))
       m))
   {}
   m))

(defn build-type-counts [m]
  (reduce-kv
   (fn [m k v]
     (if (pos? (:_count v))
       (assoc m k (:_count v))
       m))
   {}
   m))

(defn coverage [coverage-doc coverage-type]
  (reduce-kv
   (fn [m k v]
     (-> m
         (assoc-in [:coverage (keyword (str (name k) "-" (name coverage-type)))] v)
         (assoc-in [:flags (keyword (str "deposits-" (name k) "-" (name coverage-type)))] (pos? v))))
   {:coverage {}
    :flags {:deposits (> (apply + (map :_count (vals (get-in coverage-doc [:coverage :all])))) 0)
            :deposits-articles (or (> (get-in coverage-doc [:coverage :all :journal-article :_count]) 0) false)}}
   (dissoc (get-in coverage-doc [:coverage coverage-type :all]) :_count)))

(defn coverage-type [coverage-doc]
  (let [t (-> coverage-doc :finished dc/to-long)
        update-cov (fn [coverage type]
                     (-> coverage
                         (update-in [type] dissoc :all)
                         (update-in [type] remove-zero-count-nodes)
                         (update-in [type] (partial reduce-kv (fn [m k v] (assoc m k (assoc v :last-status-check-time t))) {}))))]
    (-> (:coverage coverage-doc)
        (update-cov :all)
        (update-cov :backfile)
        (update-cov :current))))

(defn coverage-type-journal [coverage-doc]
  (let [t (-> coverage-doc :finished dc/to-long)
        update-journal-cov (fn [coverage type]
                             (-> coverage
                                 (update-in [type] :all)
                                 (update-in [type] dissoc :_count)
                                 (update-in [type] assoc :last-status-check-time t)))]
    (-> (:coverage coverage-doc)
        (update-journal-cov :all)
        (update-journal-cov :backfile)
        (update-journal-cov :current))))

(defn type-counts [coverage-doc]
  (-> (:coverage coverage-doc)
      (update-in [:all] dissoc :all)
      (update-in [:backfile] dissoc :all)
      (update-in [:current] dissoc :all)
      (update-in [:all] build-type-counts)
      (update-in [:current] build-type-counts)
      (update-in [:backfile] build-type-counts)))

