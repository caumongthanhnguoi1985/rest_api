(ns cayenne.data.license
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.query :as query]
            [cayenne.elastic.util :as elastic-util]
            [clojure.string :as string]
            [clojure.data.codec.base64 :as b64]
            [qbits.spandex :as elastic]))

(defn licenses-count [query-context]
  (let [request (-> {:method :get
                     :url (str (elastic-util/index-url-prefix :work) "_search")}
                    (assoc :body (query/with-query {} query-context))
                    (assoc-in [:body :size] 0)
                    (assoc-in [:body :aggs] {:licenses-count {:cardinality {:field "license-url"}}}))
        response (elastic/request (conf/get-service :elastic) request)]
    (get-in response [:body :aggregations :licenses-count :value])))

; Composite aggregation in ES accepts "after" parameter. In our case case, to get
; the next page of the results, we pass the last license url on the current page
; as the "after" parameter. This last license url is therefore used as the cursor.
; To make it look similarly to the cursors in /works, this license url
; is base64-encoded before attaching to the API response as cursor.
(defn ->cursor [license-url]
  (if (nil? license-url)
    nil
    (-> license-url .getBytes b64/encode String.)))

(defn ->license-url [cursor]
  (if (or (string/blank? cursor) (= "*" cursor))
    nil
    (-> cursor .getBytes b64/decode String.)))

(defn ->es-request [query-context & {:keys [rows after-license]}]
  (let [request (-> {:method :get
                     :url (str (elastic-util/index-url-prefix :work) "_search")}
                    (assoc-in [:body] (query/with-query {} query-context))
                    (assoc-in [:body :size] 0)
                    (assoc-in [:body :aggs] {:licenses
                                             {:composite
                                              {:size rows
                                               :sources [{:license {:terms {:field "license-url"}}}]}}}))]
    (if (nil? after-license)
      request
      (assoc-in request [:body :aggs :licenses :composite :after] {:license after-license}))))

(defn license-items [response]
  (let [buckets (get-in response [:body :aggregations :licenses :buckets])
        ->item (fn [bucket] (let [url (get-in bucket [:key :license])
                                  work-count (:doc_count bucket)]
                              {:URL url :work-count work-count}))]
    (map ->item buckets)))

(defn licenses-seq
  ([query-context]
   (licenses-seq query-context "*"))
  ([query-context cursor]
   (let [after-license (->license-url cursor)
         request (->es-request query-context :rows 100 :after-license after-license)
         response (elastic/request (conf/get-service :elastic) request)
         licenses (license-items response)
         next-cursor (-> licenses last :URL ->cursor)]
     (if (empty? licenses)
       licenses
       (lazy-cat licenses (licenses-seq query-context next-cursor))))))

(defn fetch [{:keys [rows offset cursor] :or {cursor nil} :as query-context}]
  (let [total (licenses-count query-context)
        licenses (licenses-seq query-context cursor)
        licenses (->> licenses (drop offset) (take rows))
        next-cursor (when cursor (-> licenses last :URL ->cursor))]
    (-> (r/api-response :license-list)
        (r/with-result-items total licenses :next-cursor next-cursor))))

