(ns cayenne.ingest.sqs
  (:require [amazonica.aws.sqs :as sqs]
            [cayenne.api.v1.feed :as feed]
            [cayenne.api.v1.update :as update]
            [cayenne.conf :as conf]
            [cayenne.elastic.index :as index]
            [cayenne.formats.unixsd :as unixsd]
            [cayenne.item-tree :as itree]
            [cayenne.ingest.prefix-storage :as prefix-storage]
            [cayenne.tasks.subject :as subject]
            [cayenne.sentry :refer [with-sentry-reporting send-sentry-event]]
            [cayenne.xml :as xml]
            [clojure.core.async :refer [go-loop chan timeout alts!! >! <!!]]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [com.climate.claypoole :as cp]
            [com.climate.claypoole.lazy :as lazy]
            [taoensso.timbre :as log]))

(defn sqs-message [{:keys [bucket object-key index-all]}]
  (let [record (-> {}
                   (assoc-in [:s3 :bucket :name] bucket)
                   (assoc-in [:s3 :object :key] object-key)
                   (assoc-in [:index-all] index-all))]
    (json/write-str {:Message (json/write-str {:Records [record]})})))

(defn record-s3-objects [sqs-record]
  (let [bucket (get-in sqs-record [:s3 :bucket :name])
        key (get-in sqs-record [:s3 :object :key])
        basic-object {:bucket bucket :object-key key}]
    (if (:index-all sqs-record)
      [basic-object
       {:bucket bucket
        :object-key (str/replace key #"unixsd.xml$" "citation-update.json")
        :ignore-errors true}]
      [basic-object])))

(defn s3-objects [message]
  (-> message
      :body
      (json/read-str :key-fn keyword)
      :Message
      (json/read-str :key-fn keyword)
      :Records
      ((partial mapcat record-s3-objects))))

(defn s3-object-content [{:keys [bucket object-key ignore-errors]
                          :or {ignore-errors false}
                          :as record}]
  (if ignore-errors
    (try
      (log/info "Retrieving record" record)
      (-> (conf/get-param [:s3 :metadata-storage-service])
          (prefix-storage/file-by-key bucket object-key)
          char-array
          (vector record))
      (catch Exception _))
    (try
      (with-sentry-reporting
        {:record record}
        (log/info "Retrieving record" record)
        (-> (conf/get-param [:s3 :metadata-storage-service])
            (prefix-storage/file-by-key bucket object-key)
            char-array
            (vector record)))
      (catch Exception ex 
        (.printStackTrace ex)
        (log/error "Exception retrieving record" record (.getMessage ex))))))

(defn index-command-xml [rdr object-key]
  (let [index-commands (atom [])
        f #(let [parsed (->> %
                             unixsd/unixsd-record-parser
                             (apply subject/apply-to)
                             (apply itree/centre-on))
                 doi (first (itree/get-item-ids parsed :long-doi))
                 index-command (index/bulk-index-command parsed)]
              (if (nil? doi)
                (log/error "Null DOI in parsed XML" object-key)
                (do
                  (doseq [crm-item (unixsd/missing-crm-items %)]
                    (log/warn "Missing crm-item" crm-item "for DOI" doi))
                    ;(send-sentry-event
                    ;  {:doi doi :crm-item crm-item}
                    ;  (Exception. (str "Missing crm-item " crm-item " for DOI " doi))))
                  (swap! index-commands conj index-command)
                  (log/info "Parsed file for DOI" doi))))]
    (xml/process-xml rdr "crossref_result" f)
    @index-commands))

(defn index-command-update [rdr]
  (let [index-commands (->> rdr
                            update/read-updates-message
                            (map update/update-as-elastic-command))]
    (doseq [command index-commands]
      (log/info "Parsed update for DOI" (-> command first :update :_id)))
    index-commands))

(defn index-command [file {:keys [object-key] :as record}]
  (try
    (with-sentry-reporting
      {:record record}
      (log/info "Handling record" record)
      (with-open [rdr (io/reader file)]
        (cond
          (str/ends-with? object-key "unixsd.xml")
          (index-command-xml rdr object-key)
          (str/ends-with? object-key "citation-update.json")
          (index-command-update rdr))))
    (catch Exception ex 
      (.printStackTrace ex)
      (log/error "Exception processing record" record (.getMessage ex)))))

(defn extract-errors [ex-message es-prefix bulk-commands]
  (if (str/starts-with? ex-message es-prefix)
    (-> ex-message
        (subs (count es-prefix))
        str/trim
        (json/read-str :key-fn keyword)
        ((partial map #(let [op-type (if (:update %) :update :index)
                             doi (:_id (op-type %))
                             error (op-type %)]
                         (hash-map :doi doi :op-type op-type :error error)))))
    (map
      #(let [op-type (if (:update (first %)) :update :index)
             doi (:_id (op-type (first %)))]
         (hash-map :doi doi :op-type op-type :error (str (name op-type) " failed")))
      bulk-commands)))

(defn index-data [bulk-commands]
  (log/info "Indexing" (count bulk-commands) "items")
  (let [error-msg "Elasticsearch work index failed"]
    (try
      (index/bulk-index-items (flatten bulk-commands) :error-msg error-msg)
      (catch Exception ex 
        (.printStackTrace ex)
        (log/error "Exception indexing data" (.getMessage ex))
        (doseq [error-info (extract-errors (.getMessage ex) error-msg bulk-commands)]
          (when-not (and (= :update (:op-type error-info))
                         (= "document_missing_exception" (get-in error-info [:error :error :type])))
            (send-sentry-event {} (Exception. (str "Indexing item failed " (json/write-str error-info)))))
          (log/error "Indexing item failed" (json/write-str error-info)))))))

(defn consume-notifications []
  (let [index-ch (chan)]
    (go-loop []
      (log/debug "Polling SQS")
      (let [messages (:messages (sqs/receive-message
                                  :queue-url (conf/get-param [:service :sqs :url])
                                  :wait-time-seconds 10
                                  :max-number-of-messages 10
                                  :delete false))]
        (log/info "Got" (count messages) "messages")

        (let [records (mapcat s3-objects messages)
              s3-files (filter identity (lazy/pmap 10 s3-object-content records))]
          (doseq [commands (cp/pmap 10 (partial apply index-command) s3-files)]
            (doseq [command commands]
              (>! index-ch command))))

        (doseq [message messages]
          (sqs/delete-message {:endpoint (conf/get-param [:aws :endpoint])}
                              (assoc message :queue-url (conf/get-param [:service :sqs :url])))))
      (recur))
    
    (loop [bulk-commands []
           command (<!! index-ch)]
      (when (or (>= (count bulk-commands) 200)
                (and (nil? command) (seq bulk-commands)))
        (index-data bulk-commands))
      (let [next-bulk-commands (cond
                                 ; timeout
                                 (nil? command) []
                                 
                                 ; the item list is full
                                 (>= (count bulk-commands) 200) (vector command)

                                 :else (conj bulk-commands command))
            [next-command _] (alts!! [index-ch (timeout (* (conf/get-param [:val :indexing-timeout]) 1000))])]
        (recur next-bulk-commands next-command)))))

(defn all-keys [suffix]
  (let [bucket (conf/get-param [:s3 :metadata-bucket])
        service (conf/get-param [:s3 :metadata-storage-service])]
    (cond
      (seq (conf/get-param [:s3 :dois]))
      (apply concat (map
                      #(prefix-storage/files-by-sha1-prefix service bucket % suffix nil)
                      (conf/get-param [:s3 :dois])))
      
      (seq (conf/get-param [:s3 :prefixes]))
      (apply concat (map
                      #(prefix-storage/files-by-prefix service bucket % suffix nil)
                      (conf/get-param [:s3 :prefixes])))
      
      :else
      (prefix-storage/files-by-sha1-prefix service bucket nil suffix nil))))

(defn produce-all-keys [suffix]
  (let [bucket (conf/get-param [:s3 :metadata-bucket])
        produce (fn [data msg]
                  (log/info "Producing" msg)
                  (sqs/send-message
                    {:endpoint (conf/get-param [:aws :endpoint])}
                    (conf/get-param [:service :sqs :url])
                    (sqs-message data)))]
        
    (cp/pdoseq 10 [[first {:keys [key]}]
                   (->> (all-keys suffix)
                        (cons {:key "unixsd.xml"})
                        (partition 2 1))]
      (try
        (while (> (-> (sqs/get-queue-attributes
                        :queue-url (conf/get-param [:service :sqs :url])
                        :attribute-names ["ApproximateNumberOfMessages"])
                      :ApproximateNumberOfMessages
                      (Integer/parseInt))
                  50000)
          (Thread/sleep 60000))
        (cond
          (and (str/ends-with? (:key first) "unixsd.xml")
               (str/ends-with? key "unixsd.xml"))
          (produce {:object-key key :bucket bucket} key)
          
          (and (str/ends-with? (:key first) "citation-update.json")
               (str/ends-with? key "unixsd.xml")
               (not= (str/replace key #"unixsd.xml$" "citation-update.json") (:key first)))
          (produce {:object-key key :bucket bucket} key)
          
          (and (str/ends-with? (:key first) "citation-update.json")
               (str/ends-with? key "unixsd.xml")
               (= (str/replace key #"unixsd.xml$" "citation-update.json") (:key first)))
          (produce {:object-key key :bucket bucket :index-all true} (str key " and " (:key first)))
          
          (= "citation-update.json" suffix)
          (produce {:object-key key :bucket bucket} key))
        (catch Exception ex 
          (.printStackTrace ex)
          (log/error "Exception sending key" (:key key) (.getMessage ex)))))
          
    (log/info "Scanning finished.")
    (System/exit 0)))

(conf/with-core :default
  (conf/add-startup-task
    :sqs-ingest
    (fn [_]
      (future
        (conf/set-service!
          :sqs-ingest
          (consume-notifications))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-xml
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-xml
          (produce-all-keys "unixsd.xml"))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-update
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-update
          (produce-all-keys "citation-update.json"))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-all
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-all
          (produce-all-keys nil))))))

