(ns cayenne.user
  (:require [clojure.string :as str]
            [cayenne.conf :as conf]
            [cayenne.schedule :as schedule]
            [cayenne.startup-tasks]
            [taoensso.timbre :as timbre]
            [cayenne.tasks.subject :as subject]
            [cayenne.tasks.journal :as journal]
            [cayenne.tasks.member :as member]))

(defn begin [& profiles]
  (timbre/set-level! (conf/get-param [:log :level]))
  (timbre/set-config! [:appenders :standard-out :enabled?] true)

  (schedule/start)
  
  (conf/create-core-from! :user :default)
  (conf/set-core! :user)
  (apply conf/start-core! :user profiles))

(defn status []
  (println
   (str "Status = " (conf/get-param [:status])))
  (println
   (str "Running services = "
        (str/join ", " (-> @conf/cores
                           (get-in [conf/*core-name* :services])
                           keys)))))

(defn index-ancillary []
  (subject/index-subjects)
  (member/index-members)
  (journal/index-journals)
  (subject/update-journal-subjects))

