(ns cayenne.sentry
  (:require [sentry-clj.core :as sentry]
  									 [cayenne.version :refer [version]]
   			      [clojure.data.json :as json]
  							   [cayenne.conf :as conf]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sentry reporting

(defn send-sentry-event
  [breadcrumbs e]
  (sentry/send-event
    {:throwable e
     :server-name (conf/get-param [:sentry :server])
     :breadcrumbs [{:data (->> breadcrumbs
                               (#(assoc % "cayenne-version" version
                                          "task-arn" (conf/get-param [:sentry :task])))
                               (map #(-> % first name (vector (json/write-str (second %)))))
                               (into {}))}]}))

(defmacro with-sentry-reporting
  [breadcrumbs & code]
  (let [e (gensym)]
    `(try
       ~@code
       (catch Exception ~e
         (send-sentry-event ~breadcrumbs ~e)
         (throw ~e)))))