(ns cayenne.extlib.core
  "Expose Cayenne functionality as a Java library."
  (:require [cayenne.formats.unixsd :as unixsd]
            [cayenne.xml :as xml]
            [cayenne.item-tree :as itree]
            [clj-xpath.core :refer [$x $x:text+ xml->doc with-namespace-context xmlnsmap-from-root-node]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [cayenne.ids.doi :as doi-id]
            [cayenne.ids :as ids]

            ; identifier-defaults ensures that DOIs and other identifiers are formatted correctly.
            [cayenne.identifier-defaults])

  ; For export in extlib
  (:gen-class
    :name org.crossref.cayenne
    :methods [#^{:static true} [parseXml [java.io.Reader java.lang.String] java.lang.Iterable]
              #^{:static true} [parseXmlCentred [java.io.Reader java.lang.String] java.lang.Iterable]
              #^{:static true} [getDepositInfo [java.lang.String] java.lang.Iterable]]))

(defn -parseXml
  "Parse XML, returning a list of [primary-identifier, item-tree] pairs. Note that the primary-identifier 
   may be null depending on whether the XML came from UNIXSD or OAI-PMH. The getDepositedDois function can find all
   relevant DOIs in the whole deposit.
   Root node is the same as it would be for the XML, e.g. the Journal.
   elementName is the root element to look for, i.e. 'query_result' for OAI-PMH or 'record' for Unixsd.
   Don't close the reader (which allows it to be called on e.g. a non-file reader)."
  [rdr elementName]
  (let [xml-reader (io/reader rdr)
        elements (xml/get-elements xml-reader elementName)
        parsed (map unixsd/unixsd-record-parser elements)]
    parsed))

(defn -parseXmlCentred
  "Parse XML, returning a list of item-trees centred on the primary identifier item to make it
   the root node of the tree.
   elementName is the root element to look for, i.e. 'query_result' for OAI-PMH or 'record' for Unixsd.
   Don't close the reader (which allows it to be called on e.g. a non-file reader).
   This may not work if the identifier isn't found due to this being OAI-PMH format."
  [rdr elementName]
  (let [xml-reader (io/reader rdr)
        elements (xml/get-elements xml-reader elementName)]
    (map #(->> % unixsd/unixsd-record-parser (apply itree/centre-on)) elements)))


; This is borrowed from 'cayenne.api.deposits but without extraneous dependencies.

(defn -getDepositInfo
  "Get the list of DOIs that are deposited (not simply referenced) as part of this XML.
   Also memebr-ids mentioned.
   Note that this will return the info for the whole XML document, which may correspond to several
   item trees.
   TODO Bit hacky that this takes a String and the other functions take a reader.

   Return tuple of: [member-id list-of-deposited-dois]"
  [xml-string]
  (let [root-node (xml->doc xml-string)
        namespaces (xmlnsmap-from-root-node root-node)
        dois (with-namespace-context namespaces
              (->> root-node
                 ($x:text+ "//doi_data/doi")
                 (map (comp string/trim doi-id/normalize-long-doi))
                 (map (partial ids/get-id-uri "long-doi"))))
        member-id (with-namespace-context namespaces
                    (->> root-node
                        ($x "//crm-item[@name='member-id']")
                        (map :text)
                        (map (partial ids/get-id-uri "member-id"))))]
       [member-id dois]))

(defn -main []
  (println "This is meant to be used as a library!"))

