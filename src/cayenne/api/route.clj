(ns cayenne.api.route
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.routes :as v1]
            [cayenne.api.v1.doc :as v1-doc]
            [cayenne.api.conneg :as conneg]
            [cayenne.api.auth.crossref :as cr-auth]
            [cayenne.api.auth.token :as token-auth]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.api.snapshots :as snapshots]
            [cayenne.version :refer [version]]
            [ring.logger :as logger]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.adapter.jetty :refer [run-jetty]]
            [heartbeat.core :refer [def-version]]
            [ring.middleware.basic-authentication :refer [wrap-basic-authentication]]
            [compojure.handler :as handler]
            [ring.util.response :refer [redirect] :as response]
            [clojure.data.json :as json]
            [taoensso.timbre :as timbre]
            [compojure.core :refer [wrap-routes defroutes routes context ANY GET]])
  (:import org.eclipse.jetty.io.EofException))

(def-version version)

(defroutes health-routes
  (GET "/health/cursors" []
    ; This implementation is very simple, just exposing the counts per node.
    ; In future it can be expanded to meet monitoring needs.
    (let [response (if-let [result (elastic-util/build-open-contexts-response)]
                    (-> (response/response
                         (json/write-str
                          {:status :ok
                           :message-type :health-check
                           :message-version "1.0.0"
                           :message {:cursor-sessions result}}))
                        (response/status 200))

                    (-> (response/response
                         (json/write-str
                          {:status :error
                           :message-type :health-check
                           :message-version "1.0.0"
                           :message "No response from Elastic."}))
                        (response/status 500)))]
     (-> response
         (response/header "Content-Type" "application/json")))))

(defroutes snapshot-routes
  (GET "/snapshots" []
       (-> "<h1>/</h1><ol><li><a href='/snapshots/monthly'>monthly</a></li></ol>"
           (response/response)
           (response/status 200)
           (response/header "Content-Type" "text/html")))
  (GET "/snapshots/monthly" []
       (-> (snapshots/snapshot-years)
           ((partial str "<h1>/monthly</h1>"))
           (response/response)
           (response/status 200)
           (response/header "Content-Type" "text/html")))
  (GET "/snapshots/monthly/latest" []
       (-> (snapshots/latest-snapshot-files)
           ((partial str "<h1>/monthly/latest</h1>"))
           (response/response)
           (response/status 200)
           (response/header "Content-Type" "text/html")))
  (GET "/snapshots/monthly/:year" [year]
       (-> (snapshots/snapshot-months year)
           ((partial str "<h1>/monthly/" year "</h1>"))
           (response/response)
           (response/status 200)
           (response/header "Content-Type" "text/html")))
  (GET "/snapshots/monthly/:year/:month" [year month]
       (-> (snapshots/snapshot-files year month)
           ((partial str "<h1>/monthly/" year "/" month "</h1>"))
           (response/response)
           (response/status 200)
           (response/header "Content-Type" "text/html"))))

(defroutes protected-snapshot-routes
  (GET "/snapshots/monthly/latest/:file" [file]
    (-> (snapshots/latest-snapshot-url file)
        (.toString)
        (response/redirect)))
  (GET "/snapshots/monthly/:year/:month/:file" [year month file]
    (-> (snapshots/snapshot-url year month file)
        (.toString)
        (response/redirect))))

(defn create-basic-auth-api-routes []
  (wrap-routes
   (routes
    v1/restricted-api-routes
    (context "/v1" [] v1/restricted-api-routes)
    (context "/v1.0" [] v1/restricted-api-routes))
   wrap-basic-authentication
   cr-auth/authenticated?))

(defn wrap-serve-and-auth-when-enabled
  ([handler enabler authenticator]
   (if (enabler)
     (authenticator handler)
     (constantly
       (-> (response/response (json/write-str
                                {:status :error
                                 :message-type :access-denied
                                 :message-version "1.0.0"
                                 :message "Access Denied"}))
           (response/status 401)
           (response/header "Content-Type" "application/json"))))))

(defn create-plus-only-routes []
  (wrap-routes
   (routes
    protected-snapshot-routes)
   wrap-serve-and-auth-when-enabled
   (constantly (conf/get-param [:service :snapshots-download]))
   token-auth/wrap-auth-when-enabled))

(defn create-unprotected-api-routes []
  (wrap-routes
    (routes
      health-routes
      v1/api-routes
      v1-doc/api-doc-routes
      snapshot-routes
      (context "/v1" [] v1/api-routes)
      (context "/v1" [] v1-doc/api-doc-routes)
      (context "/v1.0" [] v1/api-routes)
      (context "/v1.0" [] v1-doc/api-doc-routes))
    token-auth/wrap-auth-when-enabled))

(defn create-docs-routes []
  (routes
   (ANY "/help" []
        (redirect "/swagger-ui"))
   (ANY "/" [] 
        (redirect "/help"))))

(defn create-unknown-route []
  (routes
   (ANY "*" []
        (-> (response/response
             (json/write-str
              {:status :error
               :message-type :route-not-found
               :message-version "1.0.0"
               :message "Route not found"}))
            (response/status 404)
            (response/header "Content-Type" "application/json")))))

(defn create-all-routes []
  (apply routes
         (-> [(create-docs-routes)]
             (conj (create-basic-auth-api-routes))
             (conj (create-plus-only-routes))
             (conj (create-unprotected-api-routes))
             (conj (create-unknown-route)))))

(defn wrap-cors
  [handler]
  (fn [request respond raise]
    (handler
      request
      #(-> %           
           (assoc-in [:headers "Access-Control-Expose-Headers"] "Link")
           (assoc-in [:headers "Access-Control-Allow-Headers"] "*")
           (assoc-in [:headers "Access-Control-Allow-Origin"] "*")
           (assoc-in [:headers "Access-Control-Allow-Headers"]
                     (str "X-Requested-With, Accept, Accept-Encoding, "
                          "Accept-Charset, Accept-Language, Accept-Ranges, Cache-Control"))
           respond)
      raise)))

(defn body-response [body]
  (-> body
      json/write-str
      response/response
      (response/status 200)
      (response/header "Content-Type" "application/json")))

(defn not-found-response [_]
  (-> "Resource not found."
      response/response
      (response/status 404)
      (response/header "Content-Type" "application/json")))

(defn ex-response [e]
  (-> {:status :error
       :message-type :exception
       :message-version "1.0.0"
       :message
       {:name (type e)
        :description (.toString e)
        :message (.getMessage e)
        :stack (map #(.toString %) (.getStackTrace e))
        :cause
        (when-let [cause (.getCause e)]
          {:name (type cause)
           :description (.toString cause)
           :stack (map #(.toString %) (.getStackTrace e))
           :message (.getMessage cause)})}}
      json/write-str
      response/response
      (response/status 500)
      (response/header "Content-Type" "application/json")
      (response/header "Exception-Name" (type e))))

(defn wrap-exception-handler
  [handler]
  (fn [request respond raise]
    (try
      (handler request respond raise)
      (catch Exception e
        (raise e)))))

(defn wrap-ignore-trailing-slash
  [handler]
  (fn [request respond raise]
    (let [uri (:uri request)]
      (handler (assoc request :uri (if (and (not (= "/" uri))
                                            (.endsWith uri "/"))
                                     (subs uri 0 (dec (count uri)))
                                     uri))
        respond raise))))

(defn log-msg-transform [message]
  (update-in
    message
    [:message :headers]
    (partial reduce-kv (fn [m k v] (assoc m k (if (#{"authorization" "crossref-plus-api-token"} k) "[REMOVED]" v))) {})))

(defn timbre-json-log [{:keys [throwable message]}]
  (let [m (if throwable
            (assoc message
              :ex-message (.getMessage throwable)
              :ex-stack (map #(.toString %) (.getStackTrace throwable))
              :ex-cause (when-let [cause (.getCause throwable)]
                          {:name (type cause)
                           :description (.toString cause)
                           :stack (map #(.toString %) (.getStackTrace cause))
                           :message (.getMessage cause)})
              :ex-data (ex-data throwable)
              :status 500)
            message)]
    (timbre/str-println (json/write-str (assoc m :worker (.getName (Thread/currentThread)))))))

(defn wrap-async [handler]
  (fn [request respond raise]
    ; passing respond function in request because liberator does not support async handlers
    ; see https://github.com/clojure-liberator/liberator/issues/280
    (let [response (-> request
                       (assoc :ring-respond #(cond
                                               (and (instance? Exception %) (= 404 (-> % ex-data :status)))
                                               (respond (not-found-response %))
                                               
                                               (instance? Exception %)
                                               (raise %)
                                               
                                               :else
                                               (respond (body-response %))))
                       handler)]
      ; we respond here only if we didn't respond while calling handler
      ; responding while calling handler is signalled by returning "ASYNC" in body
      (when (not= "ASYNC" (:body response))
        (respond response)))))

(defn wrap-heartbeat
  [handler]
  (fn [request respond raise]
    (if (.startsWith (:uri request) "/heartbeat")
      (respond (-> {:overall-status "ok"}
                   (json/write-str)
                   (response/response)
                   (response/status 200)
                   (response/header "Content-Type" "application/json")))
      (handler request respond raise))))

(defn wrap-raise
  [handler]
  (fn [request respond _]
    (handler
      request
      #(try
         (respond %)
         (catch EofException _))
      #(try
         (respond (ex-response %))
         (catch EofException _)))))

(defn create-handler []
  (-> (create-all-routes)
      (wrap-async)
      (handler/api)
      (wrap-cors)
      (wrap-heartbeat)
      (conneg/wrap-accept)
      (wrap-exception-handler)
      (wrap-ignore-trailing-slash)
      (wrap-gzip)
      (logger/wrap-log-request-start {:transform-fn log-msg-transform :log-fn timbre-json-log :request-keys [:request-method :uri :server-name :query-string :headers]})
      (logger/wrap-log-response {:transform-fn log-msg-transform :log-fn timbre-json-log :request-keys [:request-method :uri :server-name :query-string :headers]})
      (wrap-raise)))

; Register a startup task in the default core.
; This task will register the a service called 'api', which is a running server.
(conf/with-core :default
  (conf/add-startup-task
   :api
   (fn [_]
     (conf/set-service! 
      :api
      (run-jetty
        (create-handler)
        {:join? false
         :async? true
         :port (conf/get-param [:service :api :port])})))))

