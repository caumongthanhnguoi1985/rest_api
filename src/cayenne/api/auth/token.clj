(ns cayenne.api.auth.token
  (:require [ring.util.response :as ring]
            [clojure.core.memoize :as mem]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [cayenne.conf :as conf]
            [cayenne.ingest.prefix-storage :refer [file-by-key]]))

(defn- tokens []
  (let [storage (conf/get-param [:s3 :token-storage-service])
        bucket (conf/get-param [:s3 :token-bucket])
        file (conf/get-param [:s3 :token-file])]
    (set (string/split-lines (file-by-key storage bucket file)))))

(def tokens-memo (mem/ttl tokens :ttl/threshold (* 1000 60 5)))

(defn- authenticated? [r]
  (let [token (or (get-in r [:headers "Authorization"])
                  (get-in r [:headers "authorization"])
                  (get-in r [:headers "Crossref-Plus-API-Token"])
                  (get-in r [:headers "crossref-plus-api-token"]))]
    (and token (some #{token} (tokens-memo)))))

(defn wrap-auth [handler]
  (fn [request]
    (if (authenticated? request)
      (handler request)
      (-> (ring/response (json/write-str
                           {:status :error
                            :message-type :access-denied
                            :message-version "1.0.0"
                            :message "Access Denied"}))
          (ring/status 401)))))

(defn wrap-auth-when-enabled
  ([handler]
   (wrap-auth-when-enabled handler (constantly (conf/get-param [:service :token-auth]))))
  ([handler enabler]
   (if (enabler)
     (wrap-auth handler)
     (fn
       ([request] (handler request))
       ([request _ _] (handler request))))))

