(ns cayenne.api.v1.feed
  "Feed ingests XML files that are pushed into Cayenne.
   A directory structure of input and output files is expected: 'in', 'processed' and 'failed'."
  (:require [cayenne.conf :as conf]
            [cayenne.xml :as xml]
            [cayenne.formats.unixsd :as unixsd]
            [cayenne.item-tree :as itree]
            [cayenne.tasks.subject :as subject]
            [cayenne.api.v1.types :as types]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.update :refer [read-updates-message update-as-elastic-command]]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [liberator.core :refer [defresource]]
            [clojure.string :as string]
            [clojure.java.io :refer [reader] :as io]
            [taoensso.timbre :refer [info error]]
            [digest :as digest]
            [nio2.dir-seq :refer [dir-seq-glob dir-seq-filter]]
            [nio2.io :refer [path]]
            [nio2.files :refer [last-modified-time]]
            [clojure.core.async :refer [chan buffer go-loop <! >!!]]
            [cayenne.elastic.index :as es-index]
            [cayenne.elastic.update :as es-update])
  (:import [java.util UUID]
           [java.io File]
           [java.util.concurrent TimeUnit]))

; Supplementary curernt filename for feed logging.
; Feed may not always come from a file in the filesystem.
(def ^:dynamic *current-feed-file* "unknown")

(def feed-content-types #{"application/vnd.crossref.unixsd+xml"
                          "application/vnd.crossref.update+json"})

(def content-type-mnemonics
  {"application/vnd.crossref.unixsd+xml" "unixsd"
   "application/vnd.crossref.update+json" "update"})

(def content-type-mnemonics-reverse
  {"unixsd" "application/vnd.crossref.unixsd+xml"
   "update" "application/vnd.crossref.update+json"})

(def feed-providers #{"crossref"})

(def provider-names {"crossref" "Crossref"})

(defn new-id [] (UUID/randomUUID))

(defn feed-in-dir []
  (str (conf/get-param [:dir :data]) "/feed-in"))

(defn feed-filename
  "Filenames are expected to take a prescribed pattern in an expected directory structure."
  [stage-name content-type provider id]
  {:pre [(#{"in" "processed" "failed"} stage-name)]}
  (str (conf/get-param [:dir :data])
       "/feed-" stage-name
       "/" provider
       "-" (content-type-mnemonics content-type)
       "-" id
       ".body"))

(defn parse-feed-filename [filename]
  (let [[provider content-type & rest] (-> filename
                                           (string/split #"/")
                                           last
                                           (string/split #"-"))
        id (-> (string/join "-" rest)
               (string/split #"\.")
               first)]
    {:content-type (content-type-mnemonics-reverse content-type)
     :provider provider
     :id id}))

(defn incoming-file [feed-context]
  (feed-filename "in"
                 (:content-type feed-context)
                 (:provider feed-context)
                 (:id feed-context)))

(defn processed-file [feed-context]
  (feed-filename "processed"
                 (:content-type feed-context)
                 (:provider feed-context)
                 (:id feed-context)))

(defn failed-file [feed-context]
  (feed-filename "failed"
                 (:content-type feed-context)
                 (:provider feed-context)
                 (:id feed-context)))

(defn move-file! [from to]
  (let [from-file (File. from)
        to-file (File. to)]
    (.mkdirs (.getParentFile to-file))
    (.renameTo from-file to-file)))

(defn make-feed-context
  "A feed context describes a single input file's attributes."
  ([content-type provider doi]
   (let [base {:content-type content-type
               :provider provider
               :doi doi
               :id (new-id)}]
     (-> base
         (assoc :incoming-file (incoming-file base))
         (assoc :processed-file (processed-file base))
         (assoc :failed-file (failed-file base)))))
  ([filename]
   (let [base (parse-feed-filename filename)]
     (-> base
         (assoc :incoming-file (incoming-file base))
         (assoc :processed-file (processed-file base))
         (assoc :failed-file (failed-file base))))))
  
(defmulti process! (fn [content-type _] content-type))

(defmethod process! "application/vnd.crossref.unixsd+xml" [_ rdr]
  ; xml/process-xml returns the document but we use the callback to do the work.
  (let [f #(let [parsed (->> %
                         unixsd/unixsd-record-parser
                         (apply subject/apply-to)
                         (apply itree/centre-on))
                 doi (first (itree/get-item-ids parsed :long-doi))]

             (es-index/index-item parsed)
             
             (info *current-feed-file* "-" "Parsed file for DOI" doi))]
    (xml/process-xml rdr "crossref_result" f)))
   

(defmethod process! "application/vnd.crossref.update+json" [_ rdr]
  (->> rdr
       read-updates-message
       (map update-as-elastic-command)
       es-update/index-updates))

(defn process-with
  "Processes an input file according to supplied function.
   Builds a reader over the input file, passes to the function, then moves to failed or processed dir."
  [feed-context]
  (with-open [rdr (reader (:incoming-file feed-context))]
    (try
      (with-sentry-reporting
        {"feed-file" (:incoming-file feed-context)}
        (process! (:content-type feed-context) rdr)
        (move-file! (:incoming-file feed-context)
          (:processed-file feed-context)))
      (catch Exception e
        (error (:incoming-file feed-context) "-" "Exception while processing file:" (.getMessage e))
        (error e "Failed to process feed file" (:incoming-file feed-context))
        (move-file! (:incoming-file feed-context)
          (:failed-file feed-context))))))

(defn process-feed-file! [f]
  (let [filename (.getName f)]
   (binding [*current-feed-file* f]
    (try
      (let [context (-> f .getAbsolutePath make-feed-context)]

        (info filename "-" "Preparing to process")
        (process-with context)
        (info filename "-" "Processed"))

      (catch Exception e
        (error filename "-" "Failed")
        (error e "Failed to process feed file" f))))))

(defn record! [feed-context body]
  (let [incoming-file (-> feed-context :incoming-file io/file)]
    (info (.getName incoming-file) "-" "Receiving")
    (.mkdirs (.getParentFile incoming-file))
    (io/copy body incoming-file)
    (info (.getName incoming-file) "-" "Received and stored for doi:" (:doi feed-context))
    (assoc feed-context :digest (digest/md5 incoming-file))))

(defn strip-content-type-params [ct]
  (-> ct
      (string/split #";")
      first
      string/trim))

(defresource feed-resource [provider]
  :allowed-methods [:post :options]
  :available-media-types types/json
  :known-content-type? #(some #{(-> %
                                    (get-in [:request :headers "content-type"])
                                    strip-content-type-params)}
                              feed-content-types)
  :exists? (fn [_] (some #{provider} feed-providers))
  :new? true
  :post! #(let [result (-> %
                           (get-in [:request :headers "content-type"])
                           strip-content-type-params
                           (make-feed-context provider (get-in % [:request :headers "doi"]))
                           (record! (get-in % [:request :body])))]
            (assoc % :digest (:digest result)))
  :handle-created #(r/api-response :feed-file-creation
                                   :content {:digest (:digest %)}))

(def feed-file-chan (chan (buffer 1000)))

(defn start-feed-processing []
  (info "Start with concurrency" (conf/get-param [:val :feed-concurrency]))
  (dotimes [n (conf/get-param [:val :feed-concurrency])]
    (go-loop []
      (try
        (info "Go loop #" n "iteration getting a job")
        (let [f (<! feed-file-chan)]
          (info "Go loop #" n "iteration got a job -" (.getName f))
          (when (.exists f)
            (process-feed-file! f)))
        (catch Exception e
          (error e "Failed to check file existence")))
      (recur)))
  (.mkdirs (File. (feed-in-dir)))
  (doto (conf/get-service :executor)
    (.scheduleWithFixedDelay
     (fn []
       (try
         (doseq [p (dir-seq-glob (path (feed-in-dir)) "*.body")]
           (>!! feed-file-chan (.toFile p)))
         (catch Exception e
           (error e "Could not iterate feed-in files"))))
     0
     5000
     TimeUnit/MILLISECONDS)))

(defn feed-once!
  "Feed in everything in feed-in directory. Blocking."
  []
  (let [cnt (atom 0)
        entries (dir-seq-glob (path (feed-in-dir)) "*.body")
        num-entries (count entries)]
    (doseq [p entries]
      (let [f (.toFile p)]
        (swap! cnt inc)

        (when (zero? (rem @cnt 100))
          (println "Load" f "," @cnt "/" num-entries))

        (when (.exists f)
          (process-feed-file! f))))))

(defn delete-processed-feed-files [delay-minutes]
  (let [delay-millis (* delay-minutes 60 1000)
        to-delete? (fn [p] (< (-> p last-modified-time .toMillis)
                              (-> (java.util.Date.) .getTime (- delay-millis))))
        processed-dir (path (str (conf/get-param [:dir :data]) "/feed-processed"))
        files (dir-seq-filter processed-dir to-delete?)]
    (doseq [file files]
      (-> file .toFile .delete))))

(conf/with-core :default
  (conf/add-startup-task
   :process-feed-files
   (fn [_]
     (start-feed-processing))))
