(ns cayenne.api.v1.facet
  (:require [cayenne.ids.doi :as doi]
            [cayenne.ids.issn :as issn]
            [cayenne.ids.type :as type]))

(def std-facets
  {"type"                    {:external-field "type-name"
                              :allow-unlimited-values true
                              :transformer #(get-in type/type-dictionary [(keyword %) :index-id])}
   "issued-year"             {:external-field "published"
                              :allow-unlimited-values true}
   "container-title"         {:external-field "container-title"}
   "funder-name"             {:external-field "funder-name"
                              :allow-unlimited-values true}
   "funder-doi"              {:external-field "funder-doi"
                              :allow-unlimited-values true
                              :transformer doi/to-long-doi-uri}
   "contributor-orcid"       {:external-field "orcid"
                              :allow-unlimited-values true}
   "issn.value"              {:external-field "issn"
                              :allow-unlimited-values true
                              :transformer issn/to-issn-uri}
   "publisher"               {:external-field "publisher-name"
                              :allow-unlimited-values true}
   "license-url"             {:external-field "license"
                              :allow-unlimited-values true}
   "archive"                 {:external-field "archive"
                              :allow-unlimited-values true}
   "update-type"             {:external-field "update-type"
                              :allow-unlimited-values true}
   "relation-type"           {:external-field "relation-type"
                              :allow-unlimited-values true}
   "contributor-affiliation" {:external-field "affiliation"
                              :allow-unlimited-values true}
   "assertion-name"          {:external-field "assertion"
                              :allow-unlimited-values true}
   "assertion-group-name"    {:external-field "assertion-group"
                              :allow-unlimited-values true}
   "link-application"        {:external-field "link-application"}
   "volume"                  {:external-field "journal-volume"
                              :allow-unlimited-values true}
   "issue"                   {:external-field "journal-issue"
                              :allow-unlimited-values true}
   "subject"                 {:external-field "category-name"
                              :allow-unlimited-values true}
   "source"                  {:external-field "source"
                              :allow-unlimited-values true}
   "institution-ror-id"      {:external-field "ror-id"
                              :allow-unlimited-values true}})

(def external->internal-name
  (into {}
        (map (fn [[key val]] [(get val :external-field) key]) std-facets)))

(defn facet-value-limit [field specified-limit]
  (cond (and (= specified-limit "*")
             (get-in std-facets [field :allow-unlimited-values]))
        100000
        (= specified-limit "*")
        1000
        :else
        specified-limit))

(defn with-aggregations [es-body {:keys [facets]}]
  (reduce
   (fn [es-body {:keys [field count]}]
     (let [internal-field-name (external->internal-name field)
           limited-count (facet-value-limit internal-field-name count)]
       (assoc-in
        es-body
        [:aggs internal-field-name]
        {:terms {:field internal-field-name
                 :size limited-count}})))
   es-body
   facets))

(defn ->response-facet [aggregation]
  (let [internal-field-name (first aggregation)
        buckets (-> aggregation second :buckets)
        facet-config (std-facets (name internal-field-name))
        transformer (or (:transformer facet-config) identity)
        values (into {} (map
                         #(vector (transformer (:key %)) (:doc_count %)) buckets))
        sorted-values (into
                        (sorted-map-by
                          (fn [key1 key2]
                            (compare [(get values key2) key2][(get values key1) key1]))) values)]
    [(:external-field facet-config)
     {:value-count (count buckets)
      :values sorted-values}]))

(defn ->response-facets [aggregations]
  (into {} (map ->response-facet aggregations)))
