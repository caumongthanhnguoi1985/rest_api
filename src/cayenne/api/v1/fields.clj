(ns cayenne.api.v1.fields
  (:require [clojure.string :as string]))

(def work-fields
  {"bibliographic"          :bibliographic-content-text
   "title"                  :title
   "container-title"        :container-title-text
   "event-name"             :event.name
   "event-theme"            :event.theme
   "event-location"         :event.location
   "event-sponsor"          :event.sponsor
   "event-acronym"          :event.acronym
   "standards-body-name"    :standards-body.name
   "standards-body-acronym" :standards-body.acronym
   "degree"                 :degree-text
   "affiliation"            :affiliation-text
   "publisher-name"         :publisher-text
   "publisher-location"     :publisher-location-text
   "funder-name"            :funder-name-text
   "author"                 :author-text
   "editor"                 :editor-text
   "chair"                  :chair-text
   "translator"             :translator-text
   "contributor"            :contributor-text
   "description"            :description-text})

(defn clean-query [terms]
  (->> terms
       string/lower-case
       (#(string/split % #"[\s\+<>\-/\.,]+"))
       (filter #(not (contains?
                       #{"a" "an" "and" "are" "as" "at" "be" "but" "by" "for" "if"
                         "in" "into" "is" "it" "no" "not" "of" "on" "or" "s" "such"
                         "t" "that" "the" "their" "then" "there" "these" "they"
                         "this" "to" "was" "will" "with" "journal" "proceedings"
                         "vol" "p" "pp"}
                       (.replaceAll % "[^\\p{L}0-9]" ""))))
       (filter #(> (count (.replaceAll % "[^\\p{L}0-9]" "")) 1))
       (string/join " ")))

(defn with-field-queries [es-body {:keys [field-terms]}]
  (if (not-empty field-terms)
    (update-in
      es-body
      [:query :bool :must]
      (fn [matches]
        (concat
          matches
          (map (fn [t]
                 (if (= "bibliographic" (first t))
                   {:match {(-> t first work-fields)
                            {:query (-> t second clean-query)
                             :minimum_should_match "20%"}}}
                   {:match {(-> t first work-fields)
                            (-> t second clean-query)}}))
            field-terms))))
    es-body))
