(ns cayenne.api.snapshots
  (:require [cayenne.conf :as conf]
            [clojure.string :as string]
            [clojure.core.memoize :as mem]
            [cayenne.ingest.prefix-storage :refer [files-by-prefix]]
            [amazonica.aws.s3 :as s3])
  (:import [org.joda.time DateTime]))

(defn- snapshots [year month]
  (files-by-prefix 
    (conf/get-param [:s3 :snapshot-storage-service]) 
    (conf/get-param [:s3 :snapshot-bucket])
    (if (and year month) 
      (str "monthly/" year "/" month)
      (when year
        (str "monthly/" year)))
    ".gz"
    nil))

(def snapshots-memo (mem/ttl snapshots :ttl/threshold (* 1000 60 (conf/get-param [:s3 :snapshot-bucket-ttl-mins]))))

(defn- key-part [f]
  #(f (string/split (:key %) #"/")))

(defn- years []
  (sort (map (key-part second) (snapshots-memo nil nil))))

(defn- months [year]
  (sort (map (key-part #(nth % 2)) (snapshots-memo year nil))))

(defn- get-latest []
  (let [year (last (years))]
    [year (last (months year))]))

(defn- files [year month]
  (map :key  
    (snapshots-memo year month)))

(defn snapshot-years []
  (->> (years)
       distinct
       (#(concat % ["latest"]))
       (reduce
         (fn [acc s]
           (str acc "<li><a href='/snapshots/monthly/" s "'>" s "</a></li>"))
         "")
       (str "<ol>")
       (#(str % "</ol>"))))

(defn snapshot-months [year]
  (->> (months year)
       distinct
       (reduce
         (fn [acc s]
           (str acc "<li><a href='/snapshots/monthly/" year "/" s "'>" s "</a></li>"))
         "")
       (str "<ol>")
       (#(str % "</ol>"))))

(defn snapshot-files [year month]
  (->> (files year month)
       distinct
       (reduce
         (fn [acc s]
           (str acc "<li><a href='/snapshots/" s "'>" ((key-part last) {:key s}) "</a></li>"))
         "")
       (str "<ol>")
       (#(str % "</ol>"))))

(defn latest-snapshot-files []
  (let [[year month] (get-latest)]
    (snapshot-files year month)))

(defn snapshot-url [year month file]
  (s3/generate-presigned-url
    {:client-config {:path-style-access-enabled true}}
    (conf/get-param [:s3 :snapshot-bucket]) 
    (str "monthly/" year "/" month "/" file) 
    (.plusMinutes (DateTime.) (conf/get-param [:s3 :snapshot-bucket-file-expiry-mins]))))

(defn latest-snapshot-url [file]
  (let [[year month] (get-latest)]
    (snapshot-url year month file)))
