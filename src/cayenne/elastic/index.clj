(ns cayenne.elastic.index
  (:require [cayenne.elastic.convert :as convert]
            [qbits.spandex :as elastic]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]))

(defn index-command [item]
  (let [es-doc (convert/item->es-doc item)]
    [{:index {:_id (:doi es-doc)}} es-doc]))

(defn index-item [item & {:keys [error-msg]
                          :or {error-msg "Elasticsearch work index failed"}}]
  (let [tries (atom 5)]
    (elastic-util/with-retry
      {:sleep 10000
       :decay :double
       :error-hook (partial elastic-util/retry-error-hook tries error-msg)}
      error-msg
      (elastic/request
        (conf/get-service :elastic)
        {:method :post
         :url (str (elastic-util/index-url-prefix :work) "_bulk")
         :body (elastic-util/raw-jsons (index-command item))}))))

(defn bulk-index-command [item]
  (let [es-doc (convert/item->es-doc item)]
    [{:index
      {:_index (conf/get-param [:service :elastic :index :work])
       :_type "work"
       :_id (:doi es-doc)}}
     es-doc]))

(defn bulk-index-items [index-commands & {:keys [error-msg]
                                          :or {error-msg "Elasticsearch work index failed"}}]
  (let [tries (atom 5)]
    (elastic-util/with-retry
      {:sleep 10000
       :decay :double
       :error-hook (partial elastic-util/retry-error-hook tries error-msg)}
      error-msg
      (elastic/request
        (conf/get-service :elastic)
        {:method :post
         :url (str (elastic-util/index-url-prefix :work) "_bulk")
         :body (elastic-util/raw-jsons index-commands)}))))

