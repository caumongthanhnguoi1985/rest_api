(ns cayenne.elastic.util
  (:require [qbits.spandex :as elastic]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [cayenne.conf :as conf]
            [robert.bruce :refer [try-try-again]])
  (:import com.amazonaws.auth.AWS4Signer
           com.amazonaws.auth.DefaultAWSCredentialsProviderChain
           com.amazonaws.http.AWSRequestSigningApacheInterceptor
           org.elasticsearch.client.RestClient
           org.elasticsearch.client.RestClientBuilder$HttpClientConfigCallback
           org.apache.http.HttpHost
           org.apache.http.client.config.RequestConfig))

(defn raw-jsons
  "Convert a sequence of objects into newline-delimited JSON objects."
  [jsons]
  (-> (apply str
             (->> jsons
                  (map json/write-str)
                  (interpose "\n")))
      (str "\n")
      elastic/raw))

(defn parse-open-contexts
  "From a _nodes/stats/indices response retrieve the numbers of open contexts across nodes."
  [input]
  (->> input
       :nodes
       (map #(-> % second :indices :search :open_contexts))))

(defn build-open-contexts-response
  "Return a sequence of numbers of open contexts."
  []
  (-> (elastic/request
       (conf/get-service :elastic)
       {:method :get
        :url "/_nodes/stats/indices?filter_path=**.open_contexts"})
      :body
      parse-open-contexts))

(defmacro with-exception [msg code]
  (let [r (gensym)
        e (gensym)]
    `(let [~r ~code
           ~e (->> ~r :body :items (filter (comp :error val first)))]
       (when (-> ~r :body :errors)
         (throw (Exception. (str ~msg " " (json/write-str ~e)))))
       ~r)))

(defmacro with-retry [retry-params msg code]
  `(try-try-again
     ~retry-params
     (fn [] (with-exception ~msg ~code))))

(defn retry-error-hook [tries msg ex]
  (swap! tries dec)
  (cond
    (str/starts-with? (.getMessage ex) msg) false
    (zero? @tries) false
    :else true))

(defn es-client []
  (if (conf/get-param [:service :elastic :no-auth])
    (elastic/client {:hosts (conf/get-param [:service :elastic :urls])})
    (let [credentials-provider (DefaultAWSCredentialsProviderChain.)
          signer (AWS4Signer.)]
      (.setServiceName signer "es")
      (.setRegionName signer (System/getenv "AWS_REGION"))
      (let [request-config (-> (RequestConfig/custom)
                               (.setConnectionRequestTimeout 60000)
                               (.setConnectTimeout 60000)
                               (.setSocketTimeout 120000)
                               (.build))
            interceptor (AWSRequestSigningApacheInterceptor. "es" signer credentials-provider)
            interceptor-callback (reify RestClientBuilder$HttpClientConfigCallback
                                   (customizeHttpClient [this builder]
                                     (-> builder
                                         (.addInterceptorLast interceptor)
                                         (.setMaxConnPerRoute 50)
                                         (.setMaxConnTotal 100)
                                         (.setDefaultRequestConfig request-config))))]
        (-> (RestClient/builder (into-array HttpHost
                                  (map #(HttpHost/create %) (conf/get-param [:service :elastic :urls]))))
            (.setHttpClientConfigCallback interceptor-callback)
            (.setMaxRetryTimeoutMillis 120000)
            (.build))))))

(defn index-url-prefix [index] 
  (str "/" (conf/get-param [:service :elastic :index index]) "/" (name index) "/"))

