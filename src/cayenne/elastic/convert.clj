(ns cayenne.elastic.convert
  (:require [cayenne.ids.doi :as doi-id]
            [clojure.set :as set]
            [cayenne.item-tree :as itree]
            [cayenne.util :as util]
            [clj-time.core :as t]
            [cayenne.ids.issn :as issn-id]
            [cayenne.ids.isbn :as isbn-id]
            [cayenne.ids.prefix :as prefix-id]
            [cayenne.ids.member :as member-id]
            [cayenne.ids.type :as type-id]
            [cayenne.ids.update-type :as update-id]
            [cayenne.ids :as ids]
            [clojure.string :as string]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]))

(def contributions [:author :chair :editor :translator :contributor
                    :investigator :lead-investigator :co-lead-investigator])

(defn particle->date-time [particle]
  (let [year (-> particle :year util/parse-int-safe)
        month (-> particle :month util/parse-int-safe)
        day (-> particle :day util/parse-int-safe)
        hour (-> particle :hour util/parse-int-safe)
        minute (-> particle :minute util/parse-int-safe)
        sec (-> particle :second util/parse-int-safe)]
    (cond (and year month day hour minute sec)
          (t/date-time year month day hour minute sec)
          (and year month day)
          (if (< (t/number-of-days-in-the-month year month) day)
            (t/date-time year month)
            (t/date-time year month day))
          (and year month)
          (t/date-time year month)
          year
          (t/date-time year))))

(defn particle->date-str [particle]
  (let [year (-> particle :year util/parse-int-safe)
        month (-> particle :month util/parse-int-safe)
        day (-> particle :day util/parse-int-safe)]
    (cond
      (and year month day)
      (if (< (t/number-of-days-in-the-month year month) day)
        (format "%d-%02d" year month)
        (format "%d-%02d-%02d" year month day))
      (and year month)
      (format "%d-%02d" year month)
      year
      year)))

(defn maybe-int [int-as-string]
  (try
    (Integer/parseInt int-as-string)
    (catch NumberFormatException _
      nil)))

(defn item-id [item id-type & {:keys [converter]
                               :or {converter identity}}]
  (when-let [id (-> item (itree/get-item-ids id-type) first)]
    (converter id)))

(defn item-doi [item]
  (item-id item :long-doi :converter doi-id/extract-long-doi))

(defn item-issn [item]
  (item-id item :issn :converter issn-id/extract-issn))

(defn item-isbn [item]
  (item-id item :isbn :converter isbn-id/extract-isbn))

(defn item-owner-prefix [item]
  (item-id item :owner-prefix :converter prefix-id/extract-prefix))

(defn item-member-id [item]
  (item-id item :member :converter member-id/extract-member-id))

(defn item-orcid [item]
  (item-id item :orcid))

(defn item-ror-id [item]
  (item-id item :ror))

(defn item-isni [item]
  (item-id item :isni))

(defn item-wikidata [item]
  (item-id item :wikidata))

(defn item-type [item]
  (when-let [i-type (-> item itree/get-item-subtype itree/subtype-labels type-id/->type-id)]
    (name i-type)))

(defn item-issued-date [item & {as-str :as-str}]
  (let [date (->> (concat
                    (itree/get-item-rel item :posted)
                    (itree/get-item-rel item :published-print)
                    (itree/get-item-rel item :published-online)
                    (itree/get-item-rel item :published-other)
                    (itree/get-item-rel item :published)
                    (itree/get-tree-rel item :content-created)
                    (-> item (itree/get-item-rel :awarded) first (itree/get-item-rel :started))
                    (mapcat #(-> % (itree/get-item-rel :awarded) first (itree/get-item-rel :started)) (itree/get-item-rel item :project))
                    (mapcat #(-> % (itree/get-item-rel :awarded) first (itree/get-item-rel :planned-started)) (itree/get-item-rel item :project)))
                  (sort-by particle->date-time)
                  first)]
    (if as-str
      (particle->date-str date)
      (particle->date-time date))))

(defn item-published-date-str [item]
  (->> (concat
         (itree/get-item-rel item :posted)
         (itree/get-item-rel item :published-print)
         (itree/get-item-rel item :published-online)
         (itree/get-item-rel item :published-other)
         (itree/get-item-rel item :published)
         (itree/get-tree-rel item :content-created))
       (sort-by particle->date-time)
       first
       particle->date-str))

(defn item-date [item date-rel]
  (when-let [first-date (-> item (itree/get-item-rel date-rel) first)]
    (particle->date-time first-date)))

(defn item-date-str [item date-rel]
  (when-let [first-date (-> item (itree/get-item-rel date-rel) first)]
    (particle->date-str first-date)))

(defn item-plain-abstract [item]
  (let [abstract (-> item (itree/get-item-rel :abstract) first :plain)]
    (when (not (string/blank? abstract))
      abstract)))

(defn item-xml-abstract [item]
  (let [abstract (-> item (itree/get-item-rel :abstract) first :xml)]
    (when (not (string/blank? abstract))
      abstract)))

(defn journal-volume [item]
  (-> item (itree/find-item-of-subtype :journal-volume) :volume))

(defn journal-issue [item]
  (-> item (itree/find-item-of-subtype :journal-issue) :issue))

(defn item-titles [item & {:keys [subtype]}]
  (let [titles (itree/get-item-rel item :title)]
    (if subtype
      (->> titles
           (filter #(= subtype (itree/get-item-subtype %)))
           (map :value))
      (map :value titles))))

(defn item-container-titles [item & {:keys [subtype]}]
  (let [titles (->> (itree/get-item-rel item :ancestor)
                    (mapcat #(itree/get-item-rel % :title)))]
    (if subtype
      (->> titles
           (filter #(= subtype (itree/get-item-subtype %)))
           (map :value))
      (map :value titles))))

(defn item-standards-body [item]
  (when-let [standards-body (-> item
                                (itree/get-tree-rel :standards-body)
                                first)]
    (select-keys standards-body [:name :acronym])))

(defn item-issns [item]
  (map
   #(hash-map :value (-> % :value issn-id/extract-issn)
              :type  (:kind %))
   (itree/get-tree-rel item :issn)))

(defn item-subjects [item]
  (if-let [journal (itree/find-item-of-subtype item :journal)]
    (or (:category journal) [])
    []))

(defn item-isbns [item]
  (cond->> (itree/get-item-rel item :isbn)
           
           (= :book-chapter (:subtype item))
           (concat (itree/get-item-rel (itree/find-item-of-subtype item :book) :isbn))
           
           true
           (map #(hash-map
                   :value (-> % :value isbn-id/extract-isbn)
                   :type  (:kind %)))))

(defn item-update-policy [item]
  (when-let [policy (-> item (itree/get-tree-rel :update-policy) first)]
    (:value policy)))

(defn item-domain-exclusive [item]
  (-> item
      (itree/get-item-rel :domain-exclusive)
      first
      (or false)))

(defn item-institution [institution]
  {:ror-id (item-ror-id institution)
   :isni (item-isni institution)
   :wikidata (item-wikidata institution)
   :name (:name institution)
   :country (:country institution)
   :acronym (map :value (itree/get-item-rel institution :acronym))
   :place (map :value (itree/get-item-rel institution :location))
   :department (map :name (itree/get-item-rel institution :component))})

(defn contributor-affiliations [contributor]
  (map item-institution (itree/get-item-rel contributor :affiliation)))

(defn item-contributors [item]
  (mapcat
   (fn [contributor-rel]
     (map
       #(hash-map
          :contribution        (name contributor-rel)
          :given-name          (:first-name %)
          :family-name         (:last-name %)
          :alternate-name      (:alternate-name %)
          :org-name            (:name %)
          :suffix              (:suffix %)
          :prefix              (:prefix %)
          :orcid               (item-orcid %)
          :authenticated-orcid (:orcid-authenticated %)
          :sequence            (:sequence %)
          :affiliation         (as-> % $
                                     (itree/get-item-rel $ :affiliation)
                                     (map :name $))
          :affiliation-struct  (contributor-affiliations %)
          :start-date          (item-date-str % :started)
          :end-date            (item-date-str % :ended))
      (itree/get-item-rel item contributor-rel)))
   contributions))

(defn item-funders [item]
  (map
    (fn [funder]
      (let [awards (->> (itree/get-tree-rel funder :awarded)
                        (map itree/get-item-ids)
                        (map first))]
        {:name            (:name funder)
         :doi             (item-doi (assoc funder :id (map doi-id/to-long-doi-uri (:id funder))))
         :doi-asserted-by (:doi-asserted-by funder)
         :award-original  awards
         :award-keyword   (map string/lower-case awards)}))
    (itree/get-item-rel item :funder)))

(defn item-clinical-trials [item]
  (let [clinical-trial (map
                         #(hash-map
                            :number   (:ctn %)
                            :registry (:registry %)
                            :type     (:ctn-type %))
                         (itree/get-tree-rel item :clinical-trial-number))]
    (map util/remove-nil-values clinical-trial)))

(defn item-events [item]
  (when-let [event (-> item (itree/get-tree-rel :about) first)]
    (let [wanted-keys [:name :theme :location
                       :acronym :number :sponsor]
          start (item-date-str event :start)
          end   (item-date-str event :end)
          event (->
                  (select-keys event wanted-keys)
                  (assoc :start start)
                  (assoc :end end))
          cleanedup-event (util/remove-nil-values event)]
       (util/remove-empty-collections cleanedup-event))))

(defn item-links [item]
  (map
   #(hash-map
     :content-type    (:content-type %)
     :url             (:value %)
     :version         (:content-version %)
     :application     (:intended-application %))
   (itree/get-item-rel item :resource-fulltext)))

(defn item-licenses [item]
  (letfn [(difference-in-days [a b]
            (if (or (nil? a) (nil? b) (t/after? a b))
              0
              (-> (t/interval a b)
                  (t/in-days))))]
    (map
     #(let [issued-date (item-issued-date item)
            start-date (or (item-date % :start) issued-date)]
        {:version        (:content-version %)
         :url            (:value %)
         :delay          (difference-in-days issued-date start-date)
         :start-extended start-date})
     (itree/get-tree-rel item :license))))

(defn item-assertions [item]
  (map
    #(-> %
         (select-keys  [:name :label :group-name
                        :group-label :url :explanation-url
                        :value :order])
         (util/assoc-int :order (:order %)))
    (itree/get-tree-rel item :assertion)))

(defn item-relations [item]
  (map
   #(hash-map
     :type        (-> % :subtype name)
     :object      (:object %)
     :object-type (:object-type %)
     :object-ns   (:object-namespace %)
     :claimed-by  (-> % :claimed-by name))
   (itree/get-tree-rel item :relation)))

(defn item-references [item]
  (let [convert-reference (fn [item-ref]
                            (as-> item-ref $
                                  (select-keys $ [:doi :doi-asserted-by :key :issn :issn-type
                                                  :isbn :isbn-type :author :volume :issue
                                                  :first-page :year :edition :component
                                                  :standard-designator :standards-body
                                                  :unstructured :article-title :series-title
                                                  :volume-title :journal-title])
                                  (if (and (:doi $) (not (string/starts-with? (:doi $) "10."))) (dissoc $ :doi) $)
                                  (if (:doi $) $ (dissoc $ :doi-asserted-by))
                                  (if (:issn $) $ (dissoc $ :issn-type))
                                  (if (:isbn $) $ (dissoc $ :isbn-type))))]
    (as-> item $
          (itree/get-tree-rel $ :citation)
          (map convert-reference $)
          (map util/remove-nil-values $))))

(defn item-update-tos [item]
  (map
    #(let [update-type (itree/get-item-subtype %)
           update-type-label (update-id/update-label update-type)
           label (cond
                   update-type-label update-type-label
                   (:label %) (:label %)
                   :else update-type)]
       {:doi           (:value %)
        :type          update-type
        :label         label
        :date-extended (item-date % :updated)})
    (itree/find-items-of-type item :update)))

(defn contributor-name [contributor]
  (or
    (:org-name contributor)
    (str (:given-name contributor) " " (:family-name contributor))))

(defn contributor-initials [contributor]
  (letfn [(initials [first-name]
            (when first-name
              (as-> first-name $
                (string/split $ #"[\s\-]+")
                (map first $)
                (string/join " " $))))]
    (or
     (:org-name contributor)
     (str
      (-> contributor :given-name initials)
      " "
      (:family-name contributor)))))

(defn item-base-content [item]
  (let [published-year (if-not (nil? (item-date item :published-print))
                         (t/year (item-date item :published-print))
                         nil)
        issued-year (if (item-issued-date item) (t/year (item-issued-date item)) nil)]
    (->>
     (vector
      issued-year
      published-year
      (journal-issue item)
      (journal-volume item)
      (:first-page item)
      (:last-page item))
     (concat (map :value (item-issns item)))
     (concat (map :value (item-isbns item)))
     (concat (item-titles item))
     (concat (item-container-titles item))
     (string/join " "))))

(defn item-contributor-names [item & {:keys [contribution]}]
  (let [contributors (concat
                       (item-contributors item)
                       (mapcat item-contributors (itree/get-item-rel item :project)))]
    (cond->> contributors
             (not (nil? contribution))
             (filter #(= (name contribution) (:contribution %)))
             :always
             (mapcat #(concat (:alternate-name %) (vector (:given-name %) (:family-name %) (:org-name %)))))))

(defn item-peer-review [item]
  (when-let [{:keys [running-number revision-round stage recommendation
                     competing-interest-statement review-type language]} (:review item)]
    {:running-number running-number
     :revision-round revision-round
     :stage stage
     :recommendation recommendation
     :competing-interest-statement competing-interest-statement
     :type review-type
     :language language}))

(defn item-institutions [item]
  (map item-institution (itree/get-tree-rel item :institution)))

(defn item-journal-issue [journal-issue]
  (when journal-issue
    {:published-print (-> journal-issue :published-print particle->date-str)
     :published-online (-> journal-issue :published-online particle->date-str)
     :issue (:issue journal-issue)}))

(defn item-article-number [item]
  (->> (itree/get-tree-rel item :number)
       (filter #(= "article-number" (:kind %)))
       (map :value)
       first))

(defn project-titles [project]
  (map
   #(hash-map
      :title    (:value %)
      :language (:language %))
   (itree/get-item-rel project :title)))

(defn project-descriptions [project]
  (->> (itree/get-item-rel project :description)
       (map #(hash-map
               :description (:value %)
               :language    (:language %)))
       (filter (comp not string/blank? :description))))

(defn project-fundings [project]
  (map
    #(hash-map
       :type                   (:subtype %)
       :scheme                 (:scheme %)
       :award-amount           (-> % (itree/get-item-rel :awarded) first :amount)
       :award-currency         (-> % (itree/get-item-rel :awarded) first :currency)
       :award-percentage       (-> % (itree/get-item-rel :awarded) first :percentage)
       :funder-name            (-> % (itree/get-item-rel :funder) first :name)
       :funder-doi             (-> % (itree/get-item-rel :funder) first item-doi)
       :funder-doi-asserted-by (-> % (itree/get-item-rel :funder) first :doi-asserted-by))
    (itree/get-item-rel project :funding)))

(defn item-projects [item]
  (map
   #(hash-map
      :title               (project-titles %)
      :description         (project-descriptions %)
      :contributor         (item-contributors %)
      :award-amount        (-> % (itree/get-item-rel :awarded) first :amount)
      :award-currency      (-> % (itree/get-item-rel :awarded) first :currency)
      :award-start         (-> % (itree/get-item-rel :awarded) first (item-date-str :started))
      :award-end           (-> % (itree/get-item-rel :awarded) first (item-date-str :ended))
      :award-planned-start (-> % (itree/get-item-rel :awarded) first (item-date-str :planned-started))
      :award-planned-end   (-> % (itree/get-item-rel :awarded) first (item-date-str :planned-ended))
      :funding             (project-fundings %))
    (itree/get-item-rel item :project)))

(defn grant-base-content [item & {:keys [:funders :initials] :or {:funders true :initials true}}]
  (let [projects (item-projects item)
        award (-> item (itree/get-item-rel :awarded) first)
        titles (->> projects (mapcat :title) (map :title))
        funders (if funders (->> projects (mapcat :funding) (map :funder-name)) [])
        contributors (->> projects
                          (mapcat :contributor)
                          (map (if initials contributor-initials contributor-name)))]
    (concat
      [(:number award)]
      (when-let [started (item-date award :started)] [(t/year started)])
      titles
      funders
      contributors)))

(defn item-bibliographic-content
  "Fields related to bibliographic citation look up"
  [item]
  (string/join
   " "
   (-> [(item-base-content item)]
       (concat (map contributor-initials (item-contributors item)))
       (concat (grant-base-content item)))))

(defn item-metadata-content
  "A default set of search fields"
  [item]
  (string/join
   " "
   (-> [(item-base-content item)]
       (conj (:description item))
       (concat (map ids/extract-supplementary-id (itree/get-tree-ids item :supplementary))) ; plain supp ids
       (concat (grant-base-content item :funders false :initials false))
       (concat (map contributor-name (item-contributors item))) ; full names
       (concat (mapcat itree/get-item-ids (itree/get-tree-rel item :awarded))) ; grant numbers
       (concat (map :name (itree/get-tree-rel item :funder)))))) ; funder names

(defn item-resource-urls [item subtype]
  (->> (itree/get-item-rel item :resource-resolution)
       (filter #(= :resolution (:type %)))
       (filter #(= subtype (:subtype %)))
       (mapcat #(itree/get-item-rel % :web-resource))
       (mapcat :id)))

(defn item->es-doc [item]
  (let [doi            (item-doi item)
        publisher      (-> item (itree/get-tree-rel :publisher) first)
        journal        (itree/find-item-of-subtype item :journal)
        journal-issue  (itree/find-item-of-subtype item :journal-issue)
        journal-volume (itree/find-item-of-subtype item :journal-volume)]
    
    {:doi              doi
     :source           "Crossref"
     :type             (item-type item)
     :prefix           (doi-id/extract-long-prefix doi)
     :owner-prefix     (item-owner-prefix publisher)
     :member-id        (maybe-int (item-member-id publisher))
     :journal-id       (maybe-int (:journal-id publisher))
     :citation-id      (maybe-int (:citation-id publisher))
     :book-id          (maybe-int (:book-id publisher))
     :supplementary-id (itree/get-tree-ids item :supplementary)

     :journal-issue    (item-journal-issue journal-issue)
     :issued           (item-issued-date item :as-str true)
     ; used for coverage breakdown by year
     :issued-year      (when (item-issued-date item) (t/year (item-issued-date item)))

     :published        (item-published-date-str item)
     :published-online (item-date-str item :published-online)
     :published-print  (or (item-date-str item :published-print) (item-date-str item :published))
     :published-other  (item-date-str item :published-other)

     :posted           (item-date-str item :posted)
     :accepted         (item-date-str item :accepted)
     :content-created  (-> item (itree/get-tree-rel :content-created) first particle->date-str)
     :content-updated  (-> item (itree/get-tree-rel :content-updated) first particle->date-str)
     :approved         (-> item (itree/get-tree-rel :approved) first particle->date-str)
     :deposited        (-> item (itree/get-tree-rel :deposited) first particle->date-time)
     :first-deposited  (or (-> item (itree/get-tree-rel :first-deposited) first particle->date-time)
                           (-> item (itree/get-tree-rel :deposited) first particle->date-time))
     :indexed          (t/now)

     :is-referenced-by-count (-> item (itree/get-tree-rel :cited-count) first)
     :references-count       (-> item (itree/get-tree-rel :citation) count)

     :publisher          (:name publisher)
     :publisher-location (:location publisher)

     :title                 (item-titles item :subtype :long)
     :short-title           (item-titles item :subtype :short)
     :original-title        (item-titles item :subtype :original)
     :group-title           (first (item-titles item :subtype :group))
     :subtitle              (item-titles item :subtype :secondary)
     :container-title       (item-container-titles item :subtype :long)
     :short-container-title (item-container-titles item :subtype :short)

     :first-page         (:first-page item)
     :last-page          (:last-page item)
     :issue              (:issue journal-issue)
     :volume             (:volume journal-volume)
     :description        (when (-> item :description string/blank? not) (:description item))
     :article-number     (item-article-number item)
     :degree             (map :value (itree/get-item-rel item :degree))
     :part-number        (:part-number (itree/find-item-of-subtype item :book-set))
     :edition-number     (:edition-number (itree/find-first-item-of-subtypes item [:edited-book :monograph :reference-book :book]))
     ;; :component-number
     :language           (:language journal)
     :free-to-read       {:start (-> item (itree/get-tree-rel :free-to-read-start) first particle->date-time)
                          :end (-> item (itree/get-tree-rel :free-to-read-end) first particle->date-time)}

     :resource-url       (item-resource-urls item :primary)
     :resource-multi-url (item-resource-urls item :multi)
     :update-policy      (item-update-policy item)
     :domain             (itree/get-item-rel item :domains)
     :domain-exclusive   (item-domain-exclusive item)
     :archive            (map :name (itree/get-tree-rel item :archived-with))

     :abstract              (item-plain-abstract item)
     :abstract-xml          (item-xml-abstract item)

     :metadata-content-text      (item-metadata-content item)
     :bibliographic-content-text (item-bibliographic-content item)
     :author-text                (item-contributor-names item :contribution :author)
     :editor-text                (item-contributor-names item :contribution :editor)
     :chair-text                 (item-contributor-names item :contribution :chair)
     :translator-text            (item-contributor-names item :contribution :translator)
     :contributor-text           (item-contributor-names item)

     :isbn             (item-isbns item)
     :issn             (item-issns item)
     :reference        (item-references item)
     :license          (item-licenses item)
     :link             (item-links item)
     :update-to        (item-update-tos item)
     :assertion        (item-assertions item)
     :relation         (item-relations item)
     :contributor      (item-contributors item)
     :funder           (item-funders item)
     :clinical-trial   (item-clinical-trials item)
     :event            (item-events item)
     :standards-body   (item-standards-body item)
     :peer-review      (item-peer-review item)
     :institution      (item-institutions item)
     :subject          (item-subjects item)
     :subtype          (:content-type item)
     :award-original   (-> item (itree/get-item-rel :awarded) first :number)
     :award-keyword    (-> item (itree/get-item-rel :awarded) first :number (#(if % (string/lower-case %) %)))
     :award-start      (-> item (itree/get-item-rel :awarded) first (item-date-str :started))
     :project          (item-projects item)}))

(defn citeproc-date [date-str]
  (when date-str
    (let [instant (tf/parse (tf/formatters :date-time) date-str)]
      {:date-parts [[(t/year instant) (t/month instant) (t/day instant)]]
       :date-time (tf/unparse (tf/formatters :date-time-no-ms) instant)
       :timestamp (tc/to-long instant)})))

(defn citeproc-date-parts [date-str]
  (when date-str
    (let [date-parts (-> date-str
                         str
                         (string/split #"-"))]
      {:date-parts [(into [] (map util/parse-int-safe date-parts))]})))

(defn citeproc-pages [{:keys [first-page last-page]}]
  (cond (and (not (string/blank? last-page))
             (not (string/blank? first-page)))
        (str first-page "-" last-page)
        (not (string/blank? first-page))
        first-page
        :else
        nil))

(defn citeproc-ids [item ids asserted-bys]
  (reduce
    (fn [ret-ids [id asserted-by]]
      (if (get item id)
        (conj ret-ids {:id (get item id)
                       :id-type (get {:ror-id "ROR" :funder-doi "DOI" :isni "ISNI" :wikidata "wikidata"} id)
                       :asserted-by (or (get item asserted-by) "publisher")})
        ret-ids))
    []
    (map vector ids asserted-bys)))

(defn citeproc-institution [institution]
  (-> {}
      (util/assoc-exists :id (citeproc-ids
                               institution
                               [:ror-id :isni :wikidata]
                               [:asserted-by :asserted-by :asserted-by]))
      (util/assoc-exists :name (:name institution))
      (util/assoc-exists :country (:country institution))
      (util/assoc-exists :acronym (:acronym institution))
      (util/assoc-exists :place (:place institution))
      (util/assoc-exists :department (:department institution))))

(defn citeproc-affiliations [contributor]
  (if (:affiliation-struct contributor)
    (map citeproc-institution (:affiliation-struct contributor))
    (map #(hash-map :name %) (:affiliation contributor))))

(defn citeproc-contributors [es-doc & {:keys [contribution]}]
  (cond->> (:contributor es-doc)
    contribution
    (filter #(= contribution (-> % :contribution keyword)))
    :always
    (map
     #(-> {}
          (util/assoc-exists :ORCID (:orcid %))
          (util/assoc-exists :authenticated-orcid (:authenticated-orcid %) (boolean (:authenticated-orcid %)))
          (util/assoc-exists :prefix (:prefix %))
          (util/assoc-exists :suffix (:suffix %))
          (util/assoc-exists :name (:org-name %))
          (util/assoc-exists :given (:given-name %))
          (util/assoc-exists :family (:family-name %))
          (util/assoc-exists :alternate-name (:alternate-name %))
          (util/assoc-exists :sequence (:sequence %))
          (assoc :affiliation (citeproc-affiliations %))
          (util/assoc-exists :role-start (citeproc-date-parts (:start-date %)))
          (util/assoc-exists :role-end (citeproc-date-parts (:end-date %)))))))

(defn citeproc-events [es-doc]
  (when (:event es-doc)
    (-> (:event es-doc)
        (update-in [:start] citeproc-date-parts)
        (update-in [:end] citeproc-date-parts)
        util/remove-nil-values
        util/remove-empty-strings)))

(defn citeproc-references [es-doc]
  (when (:reference es-doc)
   (let [citeproc-ref (map
                        #(-> %
                          (dissoc :doi)
                          (assoc :DOI (:doi %))
                          (dissoc :issn)
                          (assoc :ISSN (:issn %))
                          (dissoc :isbn)
                          (assoc :ISBN (:isbn %)))
                        (:reference es-doc))]
      (map util/remove-nil-values citeproc-ref))))

(defn citeproc-relations [es-doc]
  (->> (:relation es-doc)
       (map #(hash-map
              :id (:object %)
              :id-type (:object-type %)
              :asserted-by (:claimed-by %)
              :rel (:type %)))
       (group-by :rel)
       (map #(vector (first %) (map (fn [a] (dissoc a :rel)) (second %))))
       (into {})))

(defn citeproc-licenses [es-doc]
  (map #(-> %
            (assoc :start (or (-> % :start citeproc-date) (-> % :start-extended citeproc-date)))
            (dissoc :start-extended)
            (dissoc :version)
            (assoc :content-version (:version %))
            (dissoc :delay)
            (assoc :delay-in-days (:delay %))
            (dissoc :url)
            (assoc :URL (:url %))
            util/remove-nil-values)
       (:license es-doc)))

(defn citeproc-assertions [es-doc]
  (map #(-> {}
            (util/assoc-exists :value (:value %))
            (util/assoc-exists :URL (:url %))
            (util/assoc-exists :order (:order %))
            (util/assoc-exists :name (:name %))
            (util/assoc-exists :label (when (:label %) (clojure.string/trim (:label %))))
            (util/assoc-exists :explanation (:explanation-url %) {:URL (:explanation-url %)})
            (util/assoc-exists :group (:group-name %) (util/assoc-exists {:name (:group-name %)} :label (:group-label %))))
       (:assertion es-doc)))

(defn citeproc-links [es-doc]
  (map #(-> {}
            (util/assoc-exists :URL (:url %))
            (util/assoc-exists :content-type (:content-type %))
            (util/assoc-exists :content-version (:version %))
            (util/assoc-exists :intended-application (:application %)))
       (:link es-doc)))

(defn citeproc-clinical-trials [es-doc]
  (when (:clinical-trial es-doc)
    (let [dk {:number :clinical-trial-number, :registry :registry, :type :type}]
      (map #(set/rename-keys % dk) (:clinical-trial es-doc)))))

(defn citeproc-updates [updates]
  (map #(hash-map
         :DOI (doi-id/extract-long-doi (:doi %))
         :type (:type %)
         :label (:label %)
         :updated (or (-> % :date citeproc-date) (-> % :date-extended citeproc-date)))
       updates))

(defn citeproc-funders [es-doc]
  (when-let [funders (:funder es-doc)]
    (map
     #(-> {}
          (util/assoc-exists :DOI (:doi %))
          (util/assoc-exists :name (:name %))
          (util/assoc-exists :doi-asserted-by (:doi-asserted-by %))
          (util/assoc-exists :award (:award-original %))) funders)))

(defn citeproc-peer-review [es-doc]
  (when-let [{:keys [running-number revision-round stage
                     competing-interest-statement recommendation
                     language] :as peer-review} (:peer-review es-doc)]
    (-> {}
        (util/assoc-exists :type (:type peer-review))
        (util/assoc-exists :running-number running-number)
        (util/assoc-exists :revision-round revision-round)
        (util/assoc-exists :stage stage)
        (util/assoc-exists :competing-interest-statement competing-interest-statement)
        (util/assoc-exists :recommendation recommendation)
        (util/assoc-exists :language language))))

(defn citeproc-journal-issue [es-doc]
  (when-let [{:keys [issue published-online published-print]} (:journal-issue es-doc)]
    (when issue
      (-> {}
          (util/assoc-exists :issue issue)
          (util/assoc-exists :published-online published-online (citeproc-date-parts published-online))
          (util/assoc-exists :published-print published-print (citeproc-date-parts published-print))))))

(defn citeproc-free-to-read [es-doc]
  (when-let [{:keys [start end]} (:free-to-read es-doc)]
    (when (or start end)
      (-> {}
          (util/assoc-exists :start-date (citeproc-date-parts start))
          (util/assoc-exists :end-date (citeproc-date-parts end))))))

(defn citeproc-institutions [es-doc]
  (map citeproc-institution (:institution es-doc)))

(defn citeproc-content-domains [{:keys [crossmark-unaware?]} es-doc]
  (merge
   {:domain (or (get es-doc :domain) [])}
   (when-not crossmark-unaware?
     {:crossmark-restriction (:domain-exclusive es-doc)})))

(defn citeproc-project-titles [es-project]
  (map
    #(-> {:title (:title %)}
         (util/assoc-exists :language (:language %)))
    (:title es-project)))

(defn citeproc-project-descriptions [es-project]
  (map
    #(-> {:description (:description %)}
         (util/assoc-exists :language (:language %)))
    (:description es-project)))

(defn citeproc-project-fundings [es-project]
  (map
    #(-> {:type (:type %)}
         (util/assoc-exists :scheme (:scheme %))
         (util/assoc-exists :award-amount (:award-amount %) {:amount (:award-amount %)
                                                             :currency (:award-currency %)
                                                             :percentage (:award-percentage %)})
         (util/assoc-exists :funder (:funder-name %) {:name (:funder-name %)
                                                      :id   (citeproc-ids % [:funder-doi] [:funder-doi-asserted-by])}))
    (:funding es-project)))

(defn citeproc-projects [es-doc]
  (when-let [projects (:project es-doc)]
    (map #(-> {:project-title (citeproc-project-titles %)}
              (util/assoc-exists :project-description  (citeproc-project-descriptions %))
              (util/assoc-exists :investigator         (citeproc-contributors % :contribution :investigator))
              (util/assoc-exists :lead-investigator    (citeproc-contributors % :contribution :lead-investigator))
              (util/assoc-exists :co-lead-investigator (citeproc-contributors % :contribution :co-lead-investigator))
              (util/assoc-exists :award-amount         (:award-amount %) {:amount (:award-amount %)
                                                                          :currency (:award-currency %)})
              (util/assoc-exists :award-start          (-> % :award-start citeproc-date-parts))
              (util/assoc-exists :award-end            (-> % :award-end citeproc-date-parts))
              (util/assoc-exists :award-planned-start  (-> % :award-planned-start citeproc-date-parts))
              (util/assoc-exists :award-planned-end    (-> % :award-planned-end citeproc-date-parts))
              (util/assoc-exists :funding              (citeproc-project-fundings %)))
      projects)))

(defn citeproc-resource [es-doc]
  (when (:resource-url es-doc)
    (-> {}
        (util/assoc-exists :primary {:URL (-> es-doc :resource-url first)})
        (util/assoc-exists :secondary (map (partial hash-map :URL) (:resource-multi-url es-doc))))))

(defn clean [doc]
  (cond-> doc
          (= "grant" (:type doc))
          (dissoc :reference-count :references-count :is-referenced-by-count
                  :content-domain :short-container-title :title :container-title
                  :original-title :subtitle :short-title)))

(defn es-doc->citeproc [es-doc]
  (let [source-doc (:_source es-doc)
        type-key (keyword (:type source-doc))]
    (-> source-doc

        (select-keys [:source :group-title :issue :volume
                      :degree :update-policy :archive :prefix
                      :references-count :is-referenced-by-count :language
                      :publisher-location :article-number :edition-number
                      :part-number :component-number])

        (->> (reduce (fn [acc i] (util/assoc-exists acc (first i) (last i))) {}))

        (assoc :type                   (:type source-doc))
        (assoc :publisher              (:publisher source-doc))
        (assoc :title                  (set (:title source-doc)))
        (assoc :subtitle               (set (:subtitle source-doc)))
        (assoc :short-title            (set (:short-title source-doc)))
        (assoc :container-title        (set (get source-doc :container-title [])))
        (assoc :short-container-title  (set (get source-doc :short-container-title [])))
        (assoc :original-title         (set (:original-title source-doc)))
        (assoc :reference-count        (:references-count source-doc))
        (assoc :DOI                    (:doi source-doc))
        (assoc :URL                    (-> source-doc :doi doi-id/to-long-doi-uri))
        (assoc :issued                 (or (-> source-doc :issued citeproc-date-parts) {:date-parts [[nil]]}))
        (assoc :prefix                 (:owner-prefix source-doc))
        (assoc :member                 (when (:member-id source-doc) (str (:member-id source-doc))))
        (assoc :indexed                (-> source-doc :indexed citeproc-date))
        (assoc :relation               (citeproc-relations source-doc))
        (assoc :content-domain         (-> type-id/type-dictionary
                                           (get type-key)
                                           (citeproc-content-domains source-doc)))


        (util/assoc-exists :abstract               (:abstract-xml source-doc))
        (util/assoc-exists :alternative-id         (->> source-doc :supplementary-id (map ids/extract-supplementary-id)))
        (util/assoc-exists :ISSN                   (->> source-doc :issn (map :value)))
        (util/assoc-exists :ISBN                   (->> source-doc :isbn (map :value)))
        (util/assoc-exists :issn-type              (:issn source-doc))
        (util/assoc-exists :isbn-type              (:isbn source-doc))
        (util/assoc-exists :page                   (citeproc-pages source-doc))
        (util/assoc-exists :published              (-> source-doc :published citeproc-date-parts))
        (util/assoc-exists :published-print        (-> source-doc :published-print citeproc-date-parts))
        (util/assoc-exists :published-online       (-> source-doc :published-online citeproc-date-parts))
        (util/assoc-exists :published-other        (-> source-doc :published-other citeproc-date-parts))
        (util/assoc-exists :posted                 (-> source-doc :posted citeproc-date-parts))
        (util/assoc-exists :accepted               (-> source-doc :accepted citeproc-date-parts))
        (util/assoc-exists :approved               (-> source-doc :approved citeproc-date-parts))
        (util/assoc-exists :deposited              (-> source-doc :deposited citeproc-date))
        (util/assoc-exists :created                (-> source-doc :first-deposited citeproc-date))
        (util/assoc-exists :content-created        (-> source-doc :content-created citeproc-date-parts))
        (util/assoc-exists :content-updated        (-> source-doc :content-updated citeproc-date-parts))
        (util/assoc-exists :author                 (citeproc-contributors source-doc :contribution :author))
        (util/assoc-exists :editor                 (citeproc-contributors source-doc :contribution :editor))
        (util/assoc-exists :translator             (citeproc-contributors source-doc :contribution :translator))
        (util/assoc-exists :chair                  (citeproc-contributors source-doc :contribution :chair))
        (util/assoc-exists :standards-body         (:standards-body source-doc))
        (util/assoc-exists :reference              (citeproc-references source-doc))
        (util/assoc-exists :event                  (citeproc-events source-doc))
        (util/assoc-exists :clinical-trial-number  (citeproc-clinical-trials source-doc))
        (util/assoc-exists :assertion              (citeproc-assertions source-doc))
        (util/assoc-exists :link                   (citeproc-links source-doc))
        (util/assoc-exists :funder                 (citeproc-funders source-doc))
        (util/assoc-exists :license                (citeproc-licenses source-doc))
        (util/assoc-exists :updated-by             (citeproc-updates (:updated-by source-doc)))
        (util/assoc-exists :update-to              (citeproc-updates (:update-to source-doc)))
        (util/assoc-exists :description            (:description source-doc))
        (util/assoc-exists :resource               (citeproc-resource source-doc))

        (util/assoc-exists :review                 (citeproc-peer-review source-doc))
        (util/assoc-exists :journal-issue          (citeproc-journal-issue source-doc))
        (util/assoc-exists :free-to-read           (citeproc-free-to-read source-doc))
        (util/assoc-exists :institution            (citeproc-institutions source-doc))
        (util/assoc-exists :subject                (:subject source-doc))
        (util/assoc-exists :subtype                (:subtype source-doc))

        (util/assoc-exists :award                  (:award-original source-doc))
        (util/assoc-exists :award-start            (-> source-doc :award-start citeproc-date-parts))
        (util/assoc-exists :project                (citeproc-projects source-doc))

        (assoc :score (:_score es-doc))

        clean)))
