(ns cayenne.tasks.subject
  (:require [cayenne.conf :as conf]
            [cayenne.item-tree :as item-tree]
            [clojure.string :as string]
            [clojure.core.memoize :as memoize]
            [qbits.spandex :as elastic]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [taoensso.timbre :refer [info error]]
            [cayenne.ids.issn :as issn-id])
  (:import [com.monitorjbl.xlsx StreamingReader]))

;; ES functions

(defn es-index-subjects [subjects]
  (let [index-command (fn [{:keys [code name]}]
                        [{:index {:_id code}}
                         {:code      code
                          :high-code (-> code (/ 100) int (* 100))
                          :name      (string/trim name)}])
        bulk-body (->> subjects
                       (mapcat index-command)
                       elastic-util/raw-jsons)]
    (elastic-util/with-retry
      {:sleep 10000 :tries 5 :decay :double}
      "Elasticsearch subject index failed"
      (elastic/request
        (conf/get-service :elastic)
        {:method :post
         :url (str (elastic-util/index-url-prefix :subject) "_bulk")
         :body bulk-body}))))

(defn es-subject-name [code]
  (try
    (let [response (elastic-util/with-retry
                     {:sleep 10000 :tries 5 :decay :double}
                     (str "Elasticsearch getting subject name for code " code " failed")
                     (elastic/request
                       (conf/get-service :elastic)
                       {:method :get
                        :url (str (elastic-util/index-url-prefix :subject) code)}))]
      (get-in response [:body :_source :name]))
    (catch Exception _)))

(defn es-search-journals [issns]
  (let [response (elastic-util/with-retry
                   {:sleep 10000 :tries 5 :decay :double}
                   (str "Elasticsearch getting journal for ISSNs " issns " failed")
                   (elastic/request
                     (conf/get-service :elastic)
                     {:method :get
                      :url (str (elastic-util/index-url-prefix :journal) "_search")
                      :body {:query
                             {:bool
                              {:minimum_should_match 1
                               :should (map #(hash-map :term {:issn.value %}) issns)}}}}))]
    (get-in response [:body :hits :hits])))

(defn es-journal-id [issns]
  (let [journals (es-search-journals issns)]
    (get-in (first journals) [:_source :id])))

(defn es-journal-subjects [issns]
 (let [journals (es-search-journals issns)]
   (->> journals
        (mapcat (comp :subject :_source))
        (map :name))))

(def es-journal-subjects-memo (memoize/ttl es-journal-subjects :ttl/threshold (* 6 60 60 1000)))

(defn es-journal-subjects-memo-clear! []
  (memoize/memo-clear! es-journal-subjects-memo))

(defn es-update-journal-subjects [journal-id subjects]
  (elastic-util/with-retry
    {:sleep 10000 :tries 5 :decay :double}
    "Elasticsearch journal subjects update failed"
    (elastic/request
      (conf/get-service :elastic)
      {:method :post
       :url (str (elastic-util/index-url-prefix :journal) journal-id "/_update")
       :body {:doc {:subject subjects}}})))

;; spreadsheet functions

(defn workbook [path]
  (->> path
       (java.net.URL.)
       (.openStream)
       (.open (-> (StreamingReader/builder)
                  (.rowCacheSize 100)
                  (.bufferSize 4096)))))

(defn rows-seq [wb sheet-name-fun]
  (let [sheet (->> wb
                   (filter #(sheet-name-fun (.getSheetName %)))
                   first)]
    (seq sheet)))

(defn cell-value [row cell-number]
  (if (or (nil? row) (nil? (.getCell row cell-number)))
    nil
    (.getStringCellValue (.getCell row cell-number))))

(defn col-numbers [header-row]
  (let [headers (map
                  #(vector % (string/lower-case (cell-value header-row %)))
                  (range (.getLastCellNum header-row)))
        p-issn-num (->> headers
                        (filter #(and (string/includes? % "print") (string/includes? % "issn")))
                        first
                        first)
        e-issn-num (->> headers
                        (filter #(and (string/includes? % "e") (string/includes? % "issn")))
                        first
                        first)
        codes-num (->> headers
                       (filter #(string/includes? % "asjc"))
                       first
                       first)]
    [(or p-issn-num 2) (or e-issn-num 3) (or codes-num 24)]))

(defn ->subject [row]
  (try
    (let [subject-code (Integer/parseInt (cell-value row 0))
          subject-name (cell-value row 1)]
      (when (and subject-code subject-name)
        {:code subject-code
         :name subject-name}))
    (catch Exception _)))

(defn ->subjects [rows]
  (->> rows
       (reduce #(conj %1 (->subject %2)) [])
       (filter identity)))

(defn ->issns [row p-issn-num e-issn-num]
  (let [p-issn (-> row (cell-value p-issn-num) str issn-id/normalize-issn)
        e-issn (-> row (cell-value e-issn-num) str issn-id/normalize-issn)]
    (cond-> []
            (-> p-issn string/blank? not)
            (conj p-issn)
            (-> e-issn string/blank? not)
            (conj e-issn))))

(defn ->journal-subjects [row codes-num]
  (->> (cell-value row codes-num)
       (#(string/split % #"[;, ]+"))
       (map string/trim)
       (filter (complement string/blank?))
       (map #(hash-map
               :ASJC (-> % str Integer/parseInt)
               :name (es-subject-name %)))
       (filter :name)))

; main functions

(defn index-subjects [& {:keys [xls-loc sheet-name]
                         :or {xls-loc (conf/get-param [:location :scopus-title-list])
                              sheet-name "ASJC classification codes"}}]
  (es-journal-subjects-memo-clear!)
  (try
    (with-sentry-reporting
      {}
      (with-open [wb (workbook xls-loc)]
        (let [rows (rows-seq wb (partial = sheet-name))
              subjects (->subjects rows)]
          (es-index-subjects subjects))))
    (catch Exception e
      (error "Exception while ingesting subjects:" (.getMessage e))
      (throw e))))

(defn update-journal-subjects [& {:keys [xls-loc sheet-name-pref]
                                  :or {xls-loc (conf/get-param [:location :scopus-title-list])
                                       sheet-name-pref "Scopus Sources "}}]
  (es-journal-subjects-memo-clear!)
  (try
    (with-sentry-reporting
      {}
      (with-open [wb (workbook xls-loc)]
        (let [cnt (atom 0)
              all-rows (rows-seq wb #(string/starts-with? % sheet-name-pref))
              headers (first all-rows)
              [p-issn-num e-issn-num codes-num] (col-numbers headers)
              rows (drop 1 all-rows)]
          (doseq [row rows]
            (with-sentry-reporting
              {"row" row}
              (when row
                (let [issns (->issns row p-issn-num e-issn-num)]
                  (when-let [journal-id (es-journal-id issns)]
                    (es-update-journal-subjects journal-id (->journal-subjects row codes-num))))))
            (swap! cnt inc)
            (when (= 0 (mod @cnt 100))
              (info "Category updated for" @cnt "journals"))))))
    (catch Exception e
      (error "Exception while ingesting journal subjects:" (.getMessage e))
      (throw e))))

(defn apply-to
  "Merge subjects into an item if it is a journal item."
  ([item]
   (if (= :journal (item-tree/get-item-subtype item))
     (let [issns (map issn-id/normalize-issn (item-tree/get-item-ids item :issn))
           subjects (es-journal-subjects-memo issns)]
       (assoc item :category subjects))
     item))
  ([id item]
   [id (apply-to item)]))

