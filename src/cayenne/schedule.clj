(ns cayenne.schedule
  (:require [cayenne.api.v1.feed :refer [delete-processed-feed-files]]
            [cayenne.conf :as conf]
            [cayenne.tasks.funder :as funder]
            [cayenne.tasks.subject :as subject]
            [cayenne.tasks.member :as member]
            [cayenne.tasks.coverage :as coverage]
            [cayenne.tasks.journal :as journal]
            [clj-time.core :as time]
            [clj-time.format :as timef]
            [clj-time.coerce :as timec]
            [org.httpkit.client :as http]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as qt]
            [clojurewerkz.quartzite.jobs :as qj :refer [defjob]]
            [clojurewerkz.quartzite.schedule.cron :as cron]
            [taoensso.timbre :as timbre :refer [info error]]))

(def update-members-trigger
  (qt/build
   (qt/with-identity (qt/key "update-members"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule (conf/get-param [:update :schedule :members]))))))

(def update-journals-trigger
  (qt/build
   (qt/with-identity (qt/key "update-journals"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule (conf/get-param [:update :schedule :journals]))))))

(def update-funders-trigger
  (qt/build
   (qt/with-identity (qt/key "update-funders"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule (conf/get-param [:update :schedule :funders]))))))

(def update-subjects-trigger
  (qt/build
   (qt/with-identity (qt/key "update-subjects"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule (conf/get-param [:update :schedule :subjects]))))))

(def delete-old-feed-files-trigger
  (qt/build
   (qt/with-identity (qt/key "delete-old-feed-files"))
   (qt/with-schedule
     (cron/schedule
      (cron/cron-schedule "0 10 * * * ?")))))

(defjob update-members [ctx]
  (try
    (info "Updating members collection")
    (member/index-members)
    (info "Updating members collection done")
    (catch Exception e (error e "Failed to update members collection")))
  (Thread/sleep (* 5 60 1000))
  (try
    (info "Updating member flags and coverage values")
    (coverage/check-members)
    (info "Updating member flags and coverage values done")
    (catch Exception e (error e "Failed to update member flags and coverage values"))))

(defjob update-journals [ctx]
  (try
    (info "Updating journals collection")
    (journal/index-journals)
    (info "Updating journals collection done")
    (catch Exception e (error e "Failed to update journals collection")))
  (Thread/sleep (* 5 60 1000))
  (try
    (info "Updating journal flags and coverage values")
    (coverage/check-journals)
    (info "Updating journal flags and coverage values done")
    (catch Exception e (error e "Failed to update journal flags and coverage values"))))

(def last-modified-format (timef/formatter "EEE, dd MMM YYYY HH:mm:ss zz"))

(defn get-last-funder-update []
  (try
    (->> (conf/get-param [:res :funder-update])
         slurp
         (Long/parseLong)
         timec/from-long)
    (catch Exception _ (timec/from-long 0))))

(defn write-last-funder-update [dt]
  (-> (conf/get-param [:res :funder-update])
      (spit (timec/to-long dt))))

(defjob update-funders [ctx]
  (try
    (info "Updating funders from new RDF")
    (let [time-of-this-update (time/now)
          time-of-previous-update (get-last-funder-update)
          last-modified-header (-> @(http/head (conf/get-param [:location :cr-funder-registry]))
                                   :headers :last-modified)
          funders-last-modified (timef/parse last-modified-format last-modified-header)]
      (info "Registry funders last modified at" funders-last-modified)
      (info "API funders last modified at" time-of-previous-update)
      (if (time/after? funders-last-modified time-of-previous-update)
        (do
          (funder/index-funders)
          (write-last-funder-update time-of-this-update)
          (info "Updating funders done"))
        (info "Funders are up to date")))
    (catch Exception e (error e "Failed to update funders from RDF"))))

(defjob update-subjects [ctx]
  (try
    (info "Updating subjects collection")
    (subject/index-subjects)
    (info "Updating subjects collection done")
    (catch Exception e (error e "Failed to update subjects collection")))
  (try
    (info "Updating journal subjects")
    (subject/update-journal-subjects)
    (info "Updating journal subjects done")
    (catch Exception e (error e "Failed to update journal subjects"))))

(defjob delete-old-feed-files [ctx]
  (delete-processed-feed-files 120))

(defn start []
  (qs/initialize)
  (qs/start))

(defn start-members-updating []
  (qs/schedule
   (qj/build
    (qj/of-type update-members)
    (qj/with-identity (qj/key "update-members")))
   update-members-trigger))

(defn start-journals-updating []
  (qs/schedule
   (qj/build
    (qj/of-type update-journals)
    (qj/with-identity (qj/key "update-journals")))
   update-journals-trigger))

(defn start-funders-updating []
  (qs/schedule
   (qj/build
    (qj/of-type update-funders)
    (qj/with-identity (qj/key "update-funders")))
   update-funders-trigger))

(defn start-subjects-updating []
  (qs/schedule
   (qj/build
    (qj/of-type update-subjects)
    (qj/with-identity (qj/key "update-subjects")))
   update-subjects-trigger))

(defn start-delete-old-feed-files []
  (qs/schedule
   (qj/build
    (qj/of-type delete-old-feed-files)
    (qj/with-identity (qj/key "delete-old-feed-files")))
   delete-old-feed-files-trigger))

(conf/with-core :default
  (conf/add-startup-task
   :update-members
   (fn [_]
     (start-members-updating))))

(conf/with-core :default
  (conf/add-startup-task
   :update-journals
   (fn [_]
     (start-journals-updating))))

(conf/with-core :default
  (conf/add-startup-task
   :update-funders
   (fn [_]
     (start-funders-updating))))

(conf/with-core :default
  (conf/add-startup-task
   :update-subjects
   (fn [_]
     (start-subjects-updating))))

(conf/with-core :default
  (conf/add-startup-task
   :cleanup-feed-files
   (fn [_]
     (start-delete-old-feed-files))))
