(ns cayenne.identifier-defaults
  "Defaults that are intrinsic to the identifier data model. 
   These are split out from the :defaults namespace because they don't concern the operation of the API service,
   and can be used by external projects such as Manifold."
  (:require [cayenne.conf :as conf]))

(conf/with-core :identifier-defaults
    (conf/set-param! [:id :issn :path] "http://id.crossref.org/issn/")
    (conf/set-param! [:id :isbn :path] "http://id.crossref.org/isbn/")
    (conf/set-param! [:id :orcid :path] "http://orcid.org/")
    (conf/set-param! [:id :owner-prefix :path] "http://id.crossref.org/prefix/")
    (conf/set-param! [:id :long-doi :path] "http://dx.doi.org/")
    (conf/set-param! [:id :short-doi :path] "http://doi.org/")
    (conf/set-param! [:id :supplementary :path] "http://id.crossref.org/supp/")
    (conf/set-param! [:id :contributor :path] "http://id.crossref.org/contributor/")
    (conf/set-param! [:id :member :path] "http://id.crossref.org/member/")
    (conf/set-param! [:id :ror :path] "https://ror.org/")
    (conf/set-param! [:id :isni :path] "https://www.isni.org/")
    (conf/set-param! [:id :wikidata :path] "https://www.wikidata.org/entity/")

    (conf/set-param! [:id-generic :path] "http://id.crossref.org/")
    (conf/set-param! [:id-generic :data-path] "http://data.crossref.org/"))

(conf/set-core! :identifier-defaults)
