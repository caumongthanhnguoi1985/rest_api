(ns cayenne.ids
  (:require [cayenne.conf :as conf]
            [clojure.string :as string]))

(defn get-id-uri [id-type id-value]
  (if-let [prefix (conf/get-param [:id (keyword id-type) :path])]
    (str prefix id-value)
    (str (conf/get-param [:id-generic :path]) (name id-type) "/" id-value)))

(defn get-data-uri [id-type id-value]
  (if-let [prefix (conf/get-param [:id (keyword id-type) :data-path])]
    (str prefix id-value)
    (str (conf/get-param [:id-generic :data-path]) (name id-type) "/" id-value)))

(defn to-supplementary-id-uri [id-value]
  (str (conf/get-param [:id :supplementary :path]) id-value))

(defn extract-supplementary-id [id-uri]
  (when id-uri
    (string/replace id-uri #"\Ahttp:\/\/id\.crossref\.org\/supp\/" "")))

(defn id-uri-type [id-uri]
  (when id-uri
    (condp #(string/starts-with? %2 %1) id-uri
      (conf/get-param [:id :issn :path]) :issn
      (conf/get-param [:id :isbn :path]) :isbn
      (conf/get-param [:id :orcid :path]) :orcid
      (conf/get-param [:id :owner-prefix :path]) :owner-prefix
      (conf/get-param [:id :long-doi :path]) :long-doi
      (conf/get-param [:id :short-doi :path]) :short-doi
      (conf/get-param [:id :supplementary :path]) :supplementary
      (conf/get-param [:id :contributor :path]) :contributor
      (conf/get-param [:id :member :path]) :member
      (conf/get-param [:id :ror :path]) :ror
      (conf/get-param [:id :isni :path]) :isni
      (conf/get-param [:id :wikidata :path]) :wikidata
      :unknown)))
