(ns cayenne.xml
  (:require [clojure.string :as string]))

(def namespaces
  {"xml" "http://www.w3.org/XML/1998/namespace"})

(defn- root-element? [^nu.xom.Element element]
  (instance? nu.xom.Document (.getParent element)))

(defn- name-eq? [^nu.xom.Node node ^String name]
  (and (= (type node) nu.xom.Element) (= (.getLocalName node) name)))

(defn- nodes->str [nodes]
  (apply str (map #(.getValue %) nodes)))

(defn- nodes->xml [nodes]
  (apply str (map #(.toXML %) nodes)))

(defn- nodes->plain [nodes]
  (->> nodes
       (filter #(= (type %) nu.xom.Text))
       (map #(.getValue %))
       (apply str)
       (.trim)))
     
(defn- child-seq [^nu.xom.Node node]
  (if node
    (map #(.getChild node %) (range 0 (.getChildCount node)))
    []))

(defn- descendant-seq* [nodes where-fn]
  (let [children (flatten (map child-seq nodes))
        rest (filter #(not (where-fn %)) children)
        stoppers (filter where-fn children)]
    (if (seq rest)
      (cons stoppers (lazy-seq (descendant-seq* rest where-fn)))
      stoppers)))

(defn- descendant-seq [nodes where-fn]
  (flatten (descendant-seq* nodes where-fn)))

(defn- attribute->str [attribute]
  (when attribute
    (.getValue attribute)))

(defn process-xml
  "Parse an XML document and apply supplied function all instances of the named element.
   Also return a nu.xom.Document matching the given node."
  [^java.io.Reader rdr tag-name process-fn]
  (let [; Stateful event-based parser.
        ; When we see a matching node set flag.
        ; This mutable flag is a Java array rather than an atom so it plays nicely
        ; with external threading systems when used in extlib. See note at top
        ; of file.
        keep? (into-array [false])
        empty (nu.xom.Nodes.)
        factory (proxy [nu.xom.NodeFactory] []
                  (startMakingElement [^String name ^String ns]
                    (when (= (last (.split name ":")) tag-name)
                      (aset keep? 0 true))
                    (proxy-super startMakingElement name ns))
                  (finishMakingElement [^nu.xom.Element element]
                    ; Match when we've finished parsing an element with the given name.
                    (when (= (.getLocalName element) tag-name)
                      (process-fn element)
                      (aset keep? 0 false))
                    ; If this node matched, call superclass.
                    ; Or if root node, superclass to recurse down.
                    ; It's not expected to match the same node more than once in a document.
                    (if (or (aget keep? 0) (root-element? element))
                      (proxy-super finishMakingElement element)
                      empty)))]
    (.build (nu.xom.Builder. factory) rdr)))

(defn get-elements
  "Retrieve the elements with the given tag name."
  [^java.io.Reader rdr tag-name]
  ; result accumulated in a mutable array, for compatibility with non-Clojure
  ; concurrency system. 
  (let [result (new java.util.ArrayList)]
    (process-xml rdr tag-name (fn [r] (.add result r)))
    result))

(defn read-xml [^java.io.Reader rdr]
  (.getRootElement (.build (nu.xom.Builder.) rdr)))

(defrecord SelectorContext [nodes descending?])

(defn- xselect-result [out-val]
  (if (= SelectorContext (type out-val))
    (or (:nodes out-val) [])
    out-val))

(defn- get-attr [node attr-name]
  (let [parts (string/split attr-name #":")]
    (cond
      (= 1 (count parts))
      (.getAttribute node attr-name)
      (= 2 (count parts))
      (.getAttribute node (second parts) (namespaces (first parts)))
      :else
      nil)))

(defn- xselect* [^SelectorContext context selector]
  (let [nodes (:nodes context)
        descending? (:descending? context)]
    (cond
      (vector? selector)
      (let [f (first selector)]
        (cond
          (= :has f)
          (->SelectorContext
            (filter #(not= (get-attr % (second selector)) nil) nodes)
            false)
          (= := f)
          (->SelectorContext
            (filter
              #(= (-> % (get-attr (second selector)) (attribute->str)) (nth selector 2)) nodes)
            false)
          (= :has-not f)
          (->SelectorContext
            (filter #(= (get-attr % (second selector)) nil) nodes)
            false)
          :else
          (map #(-> % (get-attr f) (attribute->str)) nodes)))

      (= :> selector)
      (->SelectorContext nodes true)

      (= :text selector)
      (map (comp nodes->str child-seq) nodes)

      (= :plain selector)
      (map (comp nodes->plain child-seq) nodes)

      (= :xml selector)
      (map (comp nodes->xml child-seq) nodes)

      (and descending? (= java.lang.String (type selector)))
      (->SelectorContext
        (descendant-seq nodes #(name-eq? % selector))
        false)

      (= java.lang.String (type selector))
      (->SelectorContext
        (filter #(name-eq? % selector)
          (flatten (map child-seq nodes)))
        false))))

(defn xselect [nodes & path]
  (if-not nodes
    []
    (let [node-seq (if (seq? nodes) nodes (cons nodes nil))
          initial (->SelectorContext node-seq false)
          result (reduce xselect* initial path)]
      (xselect-result result))))

(defn xselect1 [& args]
  (let [res (first (apply xselect args))]
    (if (string? res)
      (string/trim res)
      res)))

