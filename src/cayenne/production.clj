(ns cayenne.production
  (:gen-class :main true)
  (:require [cayenne.conf :as conf]
            [cayenne.defaults]
            [cayenne.schedule :as schedule]
            [cayenne.startup-tasks]
            [cayenne.version :refer [version]]
            [clojure.data.json :as json]
            [clojure.pprint :refer [pprint]]
            [org.httpkit.client :as http]
            [taoensso.timbre :as timbre :refer [error]]
            [sentry-clj.core :as sentry]
            [environ.core :refer [env]]))

(defn slack-format
  [{:keys [level ns throwable message]}]
  (format "_%s_ `%s` %s%s"
          (-> level name)
          ns
          (if message
            (with-out-str (pprint message))
            "")
          (if throwable
            (str "\n" (.toString throwable))
            "")))

(defn send-to-slack [log-event]
  (let [payload {"username" "cayennebot"
                 "icon_emoji" ":ghost:"
                 "text" (slack-format log-event)}]
    (http/post (conf/get-param [:upstream :slack-logging])
               {:form-params {:payload (json/write-str payload)}})))

(def timbre-slack
  {:doc "Spits to #cayenne slack channel."
   :enabled? true
   :async? true
   :fn send-to-slack})

(defn apply-env-overrides
  "Take some config values from environment variables. Overrides
   some parameter values in the given core."
  [core-name]
  (conf/with-core core-name
    (when (env :references)
      (if (some #{(env :references)} ["open" "limited" "closed"])
        (conf/set-param! [:service :api :references] (env :references))
        (error (str "Unknown references setting " (env :references)))))))

(def termination (promise))

(defn -main [& args]
  ; Drop leading colon from profile names.
  (let [profiles (map #(->> % (drop 1) (apply str) keyword) args)]

    (sentry/init!
      (conf/get-param [:sentry :dsn])
      {:environment (conf/get-param [:sentry :env]) :release version})
    
    (timbre/set-level! (conf/get-param [:log :level]))
    (timbre/set-config! [:appenders :standard-out :enabled?] true)

    (when (conf/get-param [:upstream :slack-logging])
      (timbre/set-config! [:appenders :slack] timbre-slack))

    (schedule/start)

    (conf/create-core-from! :production :default)
    (conf/set-core! :production)

    (apply-env-overrides :production)

    (apply conf/start-core! :production profiles)

    @termination))

(defn stop []
  (deliver termination true))
