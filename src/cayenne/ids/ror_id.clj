(ns cayenne.ids.ror-id
  (:require [clojure.string :as string])
  (:require [cayenne.ids :as ids]))

(defn normalize-ror-id
  [s]
  (when s
    (let [id (or (re-find #"\/0[A-Za-z0-9]{8}" s) (re-find #"^0[A-Za-z0-9]{8}" s))]
      (cond
        (string/blank? id)
        nil

        (string/starts-with? id "/")
        (subs id 1)

        :else
        id))))

(defn is-ror-id? [s]
  (when s
    (not (nil? (re-find #"^0[A-Za-z0-9]{8}$" s)))))

(defn to-ror-id-uri
  [s]
  (when-let [normalized-s (normalize-ror-id s)]
    (ids/get-id-uri :ror normalized-s)))

