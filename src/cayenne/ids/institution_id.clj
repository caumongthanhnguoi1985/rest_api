(ns cayenne.ids.institution-id
  (:require [clojure.string :as string])
  (:require [cayenne.ids :as ids]))

(defn normalize-institution-id [s]
  (when s
    (last (string/split s #"\/"))))

(defn to-institution-id-uri [id-type s]
  (when-let [normalized-s (normalize-institution-id s)]
    (ids/get-id-uri (keyword id-type) normalized-s)))

