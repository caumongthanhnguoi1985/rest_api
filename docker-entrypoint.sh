case $APPLICATION in
  "api")
    echo "running $APPLICATION"
    lein with-profiles prod run :api
    ;;
  "indexer")
    echo "running $APPLICATION"
    lein with-profiles prod run :create-mappings :update-index-settings :sqs-ingest
    ;;
  "scheduled-tasks")
    echo "running $APPLICATION"
    lein with-profiles prod run :create-mappings :update-index-settings :update-members :update-journals :update-funders :update-subjects
    ;;
  "scanner")
    echo "running $APPLICATION"
    lein with-profiles prod run :s3-sqs-produce-all
    ;;
  *)
    echo "APPLICATION $APPLICATION unknown";;
esac
