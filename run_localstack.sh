case $1 in
test)
  docker-compose -f docker-compose-localstack.yml run --service-ports api lein run :nrepl
  ;;
run)
  docker-compose -f docker-compose-localstack.yml kill 
  docker-compose -f docker-compose-localstack.yml up -d
  ;;
rm)
  docker-compose -f docker-compose-localstack.yml kill 
  docker-compose -f docker-compose-localstack.yml rm --force
  ;;
kill)
  docker-compose -f docker-compose-localstack.yml kill
  ;;
init)
  docker-compose -f docker-compose-localstack.yml kill 
  docker-compose -f docker-compose-localstack.yml rm --force
  docker-compose -f docker-compose-localstack.yml up -d
  ;;
stop)
  docker-compose -f docker-compose-localstack.yml stop
  ;;
start)
  docker-compose -f docker-compose-localstack.yml start
  ;;
esac
