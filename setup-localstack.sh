alias awslocal="aws --region us-east-1 --endpoint-url=http://${LOCALSTACK_HOST:-127.0.0.1}:4566" &&\
QURL=`awslocal sqs create-queue --queue-name mdqueue | egrep -o "http.*mdqueue"` &&\
echo QueueUrl: $QURL &&\
QARN=`awslocal sqs get-queue-attributes --queue-url $QURL --attribute-names QueueArn | egrep -o "arn:.*mdqueue"` &&\
echo QueueArn: $QARN &&\
TARN=`awslocal sns create-topic --name mdtopic | egrep -o "arn:.*mdtopic"` &&\
echo TopicArn: $TARN &&\

awslocal s3 mb s3://md-bucket &&\
awslocal s3 mb s3://tk-bucket &&\
awslocal s3 mb s3://sn-bucket &&\

#awslocal sqs set-queue-attributes --queue-url $QURL --attributes '{"Policy":"{\"Version\":\"v1\",\"Id\":\"allow-sns-sending-message-to-sqs\",\"Statement\":[{\"Sid\":\"sqs-sid\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"*\"},\"Action\":\"SQS:SendMessage\",\"Resource\":\"'$QARN'\",\"Condition\":{\"ArnEquals\":{\"aws:SourceArn\":\"'$TARN'\"}}}]}"}'

awslocal sns subscribe --topic-arn $TARN --protocol "sqs" --notification-endpoint $QARN &&\


#awslocal sns set-topic-attributes --topic-arn $TARN --attribute-name Policy --attribute-value '{"Version":"v1","Id":"allow-s3-publishing-to-sns","Statement":[{"Sid":"s3-sid","Effect":"Allow","Principal":{"AWS":"*"},"Action":"SNS:Publish","Resource":"'$QARN'","Condition":{"ArnEquals":{"aws:SourceArn":"arn:aws:s3:::md-bucket"}}}]}' 

awslocal s3api put-bucket-notification-configuration --bucket md-bucket --notification-configuration '{ "TopicConfigurations": [ { "Id": "s3-upload-notification-to-sns", "TopicArn": "'$TARN'", "Events": [ "s3:ObjectCreated:*" ] } ] }'

if [ "0" -eq $? ] 
then
    cat << EOF
    ##############################################
    ########## LOCALSTACK SETUP ##################
    ##############################################
    #
    # remember to use awslocal to use it
    # alias awslocal="aws --region us-east-1 --endpoint-url=http://${LOCALSTACK_HOST:-127.0.0.1}:4566"
    #
    ##############################################
EOF
else
    cat << EOF
    ##############################################
    ########## LOCALSTACK SETUP ##################
    ##############################################
    #
    # FAILED SETTING UP LOCALSTACK!!!
    #
    ##############################################
EOF
	exit 1
fi
