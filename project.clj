(defproject crossref/cayenne "2.0.1"
  :description "Index and serve CrossRef metadata"
  :url "http://github.com/CrossRef/cayenne"
  :repl-options {:port 9494 :init-ns cayenne.user :timeout 120000}
  :main cayenne.production
  :jvm-opts ["-XX:+UseG1GC"]
  :resource-paths ["csl/styles" "csl/locales" "resources"]
  :daemon {:cayenne {:ns cayenne.production
                     :pidfile "cayenne.pid"}}
  :profiles {:uberjar {:aot [cayenne.xml
                             cayenne.extlib.core]
                       :main cayenne.extlib.core}
             :dev {:source-paths ["dev"]
                   :resource-paths ["dev-resources"]
                   :dependencies [[marge "0.11.0"]]
                   :eastwood {:config-files ["eastwood-disabled-warnings.clj"] :exclude-namespaces [cayenne.data.deposit cayenne.api.deposits-tests]}}
             :prod {}}
  :test-selectors {:default (constantly true)
                   :unit :unit
                   :component :component
                   :integration :integration
                   :all (constantly true)
                   :manual :manual}
  :dependencies [[amazonica "0.3.157"]
                 [org.clojure/google-closure-library "0.0-2029-2"]
                 [org.apache.ant/ant "1.8.2"]
                 [org.clojure/clojurescript "0.0-1586"]
                 [org.clojure/clojure "1.8.0"]
                 [compliment "0.3.12"]
                 [org.clojure/core.async "0.2.395"]
                 [nrepl "0.9.0"]
                 [org.clojure/tools.trace "0.7.8"]
                 [org.mozilla/rhino "1.7.7.1"]
                 [de.undercouch/citeproc-java "2.0.0"]
                 [org.jbibtex/jbibtex "1.0.14"]
                 [info.hoetzel/clj-nio2 "0.1.1"]
                 [xml-apis "1.4.01"]
                 [me.raynes/fs "1.4.6"]
                 [com.taoensso/timbre "3.4.0"]
                 [com.cemerick/url "0.1.1"]
                 [irclj "0.5.0-alpha2"]
                 [clojurewerkz/quartzite "1.0.1"]
                 [congomongo "2.2.1"]
                 [enlive "1.1.1"]
                 [org.apache.jena/jena-core "4.3.1"]
                 [org.apache.jena/jena-arq "4.3.1"]
                 [xom "1.2.5"]
                 [clj-time "0.14.0"]
                 [clj-http "3.7.0"]
                 [org.clojure/core.incubator "0.1.2"]
                 [org.clojure/data.json "0.2.0"]
                 [org.clojure/data.xml "0.2.0-alpha6"]
                 [org.clojure/data.zip "0.1.1"]
                 [org.clojure/data.csv "0.1.2"]
                 [org.clojure/core.memoize "0.5.8"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [com.novemberain/langohr "1.4.1"]
                 [liberator "0.15.2"]
                 [compojure "1.6.0"]
                 [ring "1.9.3"]
                 [ring/ring-mock "0.3.2"]
                 
                 ; originally we were using [ring-logger "1.0.1"]
                 ; this fork adds support for async handlers, see https://github.com/nberger/ring-logger/pull/49
                 [com.github.Kauko/ring-logger "5ef5c33135"]
                 [ring/ring-codec "1.1.3"]
                 [bk/ring-gzip "0.3.0"]
                 [metosin/ring-swagger "0.26.0"]
                 [metosin/ring-swagger-ui "3.9.0"]
                 [ring-basic-authentication "1.0.5"]
                 [http-kit "2.2.0"]
                 [instaparse "1.4.1"]
                 [com.github.kyleburton/clj-xpath "1.4.3"]
                 [kjw/ring-logstash "0.1.3"]
                 [crossref/heartbeat "0.1.4"]
                 [robert/bruce "0.8.0"]
                 [bigml/sampling "3.0"]
                 [digest "1.4.4"]
                 [cc.qbits/spandex "0.5.2"]
                 [environ "1.0.3"]
                 [javax.xml.bind/jaxb-api "2.3.1"]
                 [slingshot "0.12.2"]
                 [com.monitorjbl/xlsx-streamer "2.1.0" :exclusions [com.rackspace.apache/xerces2-xsd11]]
                 [org.apache.commons/commons-compress "1.19"]
                 [io.sentry/sentry-clj "3.1.138"]
                 [com.climate/claypoole "1.1.4"]
                 [com.github.awslabs/aws-request-signing-apache-interceptor "f39dfaf" :exclusions [com.amazonaws/aws-java-sdk-core]]

                 ; Only for tests.
                 [cheshire "5.8.1"]]

  :plugins [[jonase/eastwood "0.3.5"]
            [cider/cider-nrepl "0.28.1"]
            [mx.cider/enrich-classpath "1.6.2"] ]

  :repositories [["jitpack.io" "https://jitpack.io"]])

