
The number of items returned in a single response is controlled by `rows` parameter (default is 20, and maximum is 1,000). To limit results to 5, for example, you could do the following:

##

```
/works?query=allen+renear&rows=5
```

##

`offset` parameter can be used to retrieve items starting from a specific index of the result list. For example, to select the second set of 5 results (i.e. results 6 through 10), you would do the following:

##

```
/works?query=allen+renear&rows=5&offset=5
```

##
