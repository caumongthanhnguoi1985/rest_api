
## Filters

Filters allow you to select items based on specific criteria. All filter results are lists.

##

Example:

##
```
/funders?filter=location:Spain
```
##

This endpoint supports the following filters:

##
