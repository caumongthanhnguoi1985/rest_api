
## Sort

Results can be sorted by applying the `sort` and `order` parameters. `sort` sets the field by which results will be sorted. `order` sets the result ordering, either `asc` or `desc` (default is `desc`).

An example that sorts results in order of publication, beginning with the least recent:

##

```
/works?query=josiah+carberry&sort=published&order=asc
```

##

This endpoint supports sorting by the following elements:

##
