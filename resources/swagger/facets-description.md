
## Facets

Facet counts can be retrieved by enabling faceting. Facets are enabled by providing facet field names along with a maximum number of returned term values. The larger the number of returned values, the longer the query will take. Some facet fields can accept a `*` as their maximum, which indicates that all values should be returned.

##

For example, to get facet counts for all work types:

##
```
/works?facet=type-name:*
```

##

This endpoint supports the following facets:

##
