
## Queries

Free form search queries can be made, for example, funders that include `association` and `library`:

##

```
/members?query=association+library
```

