
## Querying

This endpoint accepts `query` parameter, which allows for free text querying. The result contains aggregated licenses from the works that match given query.

##

For example, this request:

##

```
/licenses?query=richard+feynman
```

##

will first select works matching `richard+feynman`, and aggregate their licenses.
