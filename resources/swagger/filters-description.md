
## Filters

Filters allow you to select items based on specific criteria. All filter results are lists.

##

For example:

##
```
/works?filter=type:dataset
```

### Multiple filters

Multiple filters can be specified in a single query. In such a case, different filters will be applied with AND semantics, while specifying the same filter multiple times will result in OR semantics - that is, specifying the filters:

- `is-update:true`
- `from-pub-date:2014-03-03`
- `funder:10.13039/100000001`
- `funder:10.13039/100000050`

would locate documents that are updates, were published on or after 3rd March 2014 and were funded by either the National Science Foundation (`10.13039/100000001`) or the National Heart, Lung, and Blood Institute (`10.13039/100000050`). These filters would be specified by joining each filter together with a comma:

##
```
/works?filter=is-update:true,from-pub-date:2014-03-03,funder:10.13039/100000001,funder:10.13039/100000050
```

### Dot filters

A filter with a dot in its name is special. The dot signifies that the filter will be applied to some other record type that is related to primary resource record type. For example, with work queries, one can filter on works that have an award, where the same award has a particular award number and award-giving funding agency:

##
```
/works?filter=award.number:CBET-0756451,award.funder:10.13039/100000001
```
##

Here we filter on works that have an award by the National Science Foundation that also has the award number `CBET-0756451`.

### Note on dates

The dates in filters should always be of the form YYYY-MM-DD, YYYY-MM or YYYY. The date filters are inclusive. For example:

* `from-pub-date:2018-09-18` filters works published on or after 18th September 2018
* `from-created-date:2016-02-29,until-created-date:2016-02-29` filters works first deposited on 29th February 2016
* `until-created-date:2010-06` filters works first deposited in or before June 2010
* `from-update-date:2017,until-update-date:2017` filters works with metadata updated in 2017

Also note that date information in Crossref metadata can often be incomplete. So, for example, a publisher may only include the year and month of publication for a journal article. For a monograph they might just include the year. In these cases the API selects the earliest possible date given the information provided. So, for instance, if the publisher only provided 2013-02 as the published date, then the date would be treated as 2013-02-01. Similarly, if the publisher only provided the year 2013 as the date, it would be treated at 2013-01-01.

### Note on owner prefixes

The prefix of a Crossref DOI does **NOT** indicate who currently owns the DOI. It only reflects who originally registered the DOI. Crossref metadata has an **prefix** element that records the current owner of the Crossref DOI in question.

##

Crossref also has member IDs for depositing organisations. A single member may control multiple owner prefixes, which in turn may control a number of DOIs. When looking at works published by a certain organisaton, member IDs and the member routes should be used.

### Notes on incremental metadata updates

When using time filters to retrieve periodic, incremental metadata updates, the `from-index-date` filter should be used over `from-update-date`, `from-deposit-date`, `from-created-date` and `from-pub-date`. The timestamp that `from-index-date` filters on is guaranteed to be updated every time there is a change to metadata requiring a reindex.

##

This endpoint supports the following filters:

##
