
## Filters

Filters allow you to select deposits based on specific criteria. All filter results are lists.

##

Examples:

##
```
/deposits?filter=from-submission-time:2020-06-01
```
##

This endpoint supports the following filters:

##
