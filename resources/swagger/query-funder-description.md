
## Queries

Free form search queries can be made, for example, funders that include `research` and `foundation`:

##

```
/funders?query=research+foundation
```

