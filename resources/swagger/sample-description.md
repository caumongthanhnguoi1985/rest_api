
## Sample

Being able to select random results is useful for both testing and sampling. You can use the `sample` parameter to retrieve random results. So, for example, the following selects 10 random works:

##
```
/works?sample=10
```
##

Note that when you use the `sample` parameter, the `rows` and `offset` parameters are ignored.

